import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.pool.ConnectionPool;
import edu.epam.task6.service.AccountService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UserLoginTest {
    private String login;
    private String password;
    private AccountService service;
    private ConnectionPool pool = ConnectionPool.getInstance();

    @Before
    public void setParameters() {
        login = "nikolay";
        password = "1234";
        service = AccountService.getInstance();
        pool.openConnection();
    }

    @Test
    public void validUserTest() throws ServiceException {
        boolean result = false;
        Account account = new Account(login, password);
        Account dbAccount = service.retrieveAccount(account);
        if (dbAccount != null) {
            result = true;
        }
        Assert.assertTrue(result);
    }

    @After
    public void tearDown() {
        pool.closePool();
    }
}
