import edu.epam.task6.helper.EncryptDecrypt;
import org.junit.Assert;
import org.junit.Test;

public class EncryptTest {

    public static final String DB_PROPERTIES = "db.properties";

    @Test
    public void encryptDecryptTest() throws Exception {
        String propertyFileName = DB_PROPERTIES;
        EncryptDecrypt app = new EncryptDecrypt(propertyFileName);
        app.encryptPropertyValue();
        String result = app.decryptPropertyValue();
        Assert.assertTrue("Decrypted password should be \"root\"", result.equalsIgnoreCase("root"));
    }
}
