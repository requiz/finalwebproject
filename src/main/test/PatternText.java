import edu.epam.task6.validation.FieldsValidator;
import org.junit.Assert;
import org.junit.Test;

public class PatternText {
    private String validSum = "1234";
    private String invalidSum = "12345678901234";

    @Test
    public void validateBalanceTest() {
        boolean validResult = FieldsValidator.validateMoney(validSum);
        Assert.assertTrue(validResult);
    }

    @Test
    public void invalidBaslanceTest() {
        boolean invalidRest = FieldsValidator.validateMoney(invalidSum);
        Assert.assertFalse(invalidRest);
    }
}
