import edu.epam.task6.entity.impl.Card;
import edu.epam.task6.entity.impl.Operation;
import edu.epam.task6.entity.impl.Transaction;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.exception.ValidationException;
import edu.epam.task6.pool.ConnectionPool;
import edu.epam.task6.service.CardService;
import edu.epam.task6.service.OperationService;
import edu.epam.task6.service.TransactionService;
import edu.epam.task6.validation.TransactionValidator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TransactionTest {
    private String number;
    private String operation;
    private String sum;
    private CardService cardService;
    private OperationService operationService;
    private TransactionService transactionService;
    private static final double DELTA = 1e-15;
    private ConnectionPool pool = ConnectionPool.getInstance();
    private Lock lock = new ReentrantLock();

    @Before
    public void setParameters() {
        number = "14665264";
        operation = "payment for mts";
        sum = "20000";
        cardService = CardService.getInstance();
        operationService = OperationService.getInstance();
        transactionService = TransactionService.getInstance();
        pool.openConnection();
    }

    @Test
    public void paymentTest() throws ServiceException, ValidationException {
        double endBalance;
        double payment = Double.parseDouble(sum);
        long cardNumber = Long.parseLong(number);
        Card card = cardService.getByNumber(cardNumber);
        Operation selectOperation = operationService.getOperByName(operation);
        endBalance = cardService.decreaseAmmount(card, cardService.validateCurrency(card), payment);
        Transaction transaction = TransactionValidator.validateTransaction(number, selectOperation, sum);
        lock.lock();
        try {
            double decreaseSum = cardService.withdrawMoney(transaction);
            transactionService.insertTransferPayment(transaction, decreaseSum);
        } finally {
            lock.unlock();
        }
        Card cardAfterPayment = cardService.getByNumber(Long.parseLong(number));
        Assert.assertEquals(endBalance, cardAfterPayment.getBalance(), DELTA);
    }

    @After
    public void tearDown() {
        pool.closePool();
    }
}
