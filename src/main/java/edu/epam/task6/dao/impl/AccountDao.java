package edu.epam.task6.dao.impl;

import edu.epam.task6.dao.IDAO;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.entity.impl.status.AccountStatus;
import edu.epam.task6.dao.exception.DaoException;
import edu.epam.task6.pool.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>This class works with the entity of account from database,
 * implementation of {@link edu.epam.task6.dao.IDAO}</p>
 *
 * @author Vladislav Gryadovkin
 */
public class AccountDao implements IDAO<Account, String> {
    private final Logger logger = Logger.getLogger(AccountDao.class.getName());
    private final String ACC_GET_ALL = "select * from account where login=?";
    private final String ACC_GET_BY_ID = "select *from account where acc_id=?";
    private final String ACC_INSERT_SQL = "insert into account"
            + "(login,password,first_name,last_name,sex,email,contact_number,role) values"
            + "(?,?,?,?,?,?,?,?)";
    private final String ACC_GET_LOGINS = "select login from account where role<>?";
    private final String ACC_UPDATE_SQL = "update account set login=?,first_name=?,last_name=?," +
            "sex=?,email=?,contact_number=? where acc_id=?";
    private final String ACC_GET_USERS = "select SQL_NO_CACHE * from account where role<>? limit %d,%d";
    private final String ACC_CHANGE_PASSWORD = "update account set password=? where acc_id=?";
    private final String ACC_CASHED_SQL = "SELECT SQL_NO_CACHE COUNT(*) from account where role<>?";
    public static final String ACC_ID = "acc_id";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String EMAIL = "email";
    public static final String SEX = "sex";
    public static final String CONTACT_NUMBER = "contact_number";
    public static final String ROLE = "role";
    private static final AccountDao instance = new AccountDao();

    private AccountDao() {

    }

    public static AccountDao getAccountDao() {
        return instance;
    }

    @Override
    public Account getByParam(String login) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Account account = null;
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(ACC_GET_ALL)) {
            statement.setString(1, login);
            try (ResultSet rs = statement.executeQuery()) {
                if (rs.next()) {
                    account = setAccountInfo(rs);
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return account;
    }

    @Override
    public List<Account> getAll() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    private Account setAccountInfo(ResultSet rs) throws SQLException {
        Account account = new Account();
        account.setId(rs.getInt(ACC_ID));
        account.setLogin(rs.getString(LOGIN));
        account.setPassword(rs.getString(PASSWORD));
        account.setFirstName(rs.getString(FIRST_NAME));
        account.setLastName(rs.getString(LAST_NAME));
        account.setSex(rs.getString(SEX));
        account.setEmail(rs.getString(EMAIL));
        account.setContactNumber(rs.getString(CONTACT_NUMBER));
        account.setStatus(AccountStatus.valueOf(rs.getString(ROLE)));
        return account;
    }

    private void setStatement(PreparedStatement statement, Account account) throws SQLException {
        statement.setString(1, account.getLogin());
        statement.setString(2, account.getPassword());
        statement.setString(3, account.getFirstName());
        statement.setString(4, account.getLastName());
        statement.setString(5, account.getSex());
        statement.setString(6, account.getEmail());
        statement.setString(7, account.getContactNumber());

    }

    @Override
    public void insert(Account account) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(ACC_INSERT_SQL)) {
            setStatement(statement, account);
            statement.setString(8, account.getStatus().name());
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
    }

    @Override
    public void update(Account account) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(ACC_UPDATE_SQL)) {
            statement.setString(1, account.getLogin());
            statement.setString(2, account.getFirstName());
            statement.setString(3, account.getLastName());
            statement.setString(4, account.getSex());
            statement.setString(5, account.getEmail());
            statement.setString(6, account.getContactNumber());
            statement.setLong(7, account.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
    }

    public Account getById(Integer id) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(ACC_GET_BY_ID)) {
            statement.setLong(1, id);
            try (ResultSet rs = statement.executeQuery()) {
                if (rs.next()) {
                    return setAccountInfo(rs);
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return null;
    }

    public List<Account> getAll(int page, int recordsPerPage) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<Account> accounts = new ArrayList<>();
        String sql = String.format(ACC_GET_USERS, page, recordsPerPage);
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, AccountStatus.ADMIN.name());
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Account account = new Account();
                    account.setLogin(rs.getString(LOGIN));
                    account.setEmail(rs.getString(EMAIL));
                    account.setContactNumber(rs.getString(CONTACT_NUMBER));
                    accounts.add(account);
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return accounts;
    }

    public void changePassword(Account account) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(ACC_CHANGE_PASSWORD)) {
            statement.setString(1, account.getPassword());
            statement.setInt(2, account.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
    }

    public List<String> getUserLogins() {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<String> logins = new ArrayList<>();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(ACC_GET_LOGINS)) {
            statement.setString(1, AccountStatus.ADMIN.name());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                logins.add(rs.getString(LOGIN));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return logins;
    }

    public int getNoOfRecords() {
        int noOfRecords = 0;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(ACC_CASHED_SQL)) {
            statement.setString(1, AccountStatus.ADMIN.name());
            try (ResultSet rs = statement.executeQuery()) {
                if (rs.next()) {
                    noOfRecords = rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return noOfRecords;
    }
}
