package edu.epam.task6.dao.impl;

import edu.epam.task6.dao.IDAO;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.entity.impl.Card;
import edu.epam.task6.entity.impl.status.CardStatus;
import edu.epam.task6.dao.exception.DaoException;
import edu.epam.task6.pool.ConnectionPool;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>This class works with the entity of card from database,
 * implementation of {@link edu.epam.task6.dao.IDAO}</p>
 *
 * @author Vladislav Gryadovkin
 */
public class CardDao implements IDAO<Card, Long> {
    private static final Logger logger = Logger.getLogger(CardDao.class.getName());
    private final String CARD_INSERT_SQL = "insert into card"
            + "(number,balance,currency,account_acc_id,date_open,type_card_type,status) values"
            + "(?,?,?,?,?,?,?)";
    private final String CARD_VIEW_SQL = "select *from card where account_acc_id=?";
    private final String CARD_VIEW_WORKING = "select *from card where account_acc_id=? and status=?";
    private final String CARD_UPDATE_STATUS = "update card set status=? where number=?";
    private final String CARD_DROP = "delete from card where number=?";
    private final String CARD_VIEW_NUNBER = "select *from card where number=?";
    private final String CARD_VIEW_ALLEN = "select *from card where account_acc_id<>? " +
            "and status= ?";
    private static final String CARD_NEW_STATUS = "select NUMBER,balance,currency,login from card" +
            " INNER JOIN account on account_acc_id = acc_id where status= ?";
    public static final String NUMBER = "number";
    public static final String BALANCE = "balance";
    public static final String CURRENCY = "currency";
    public static final String DATE_OPEN = "date_open";
    public static final String TYPE_CARD_TYPE = "type_card_type";
    public static final String STATUS = "status";
    public static final String LOGIN = "login";

    private static final CardDao dao = new CardDao();

    public static CardDao getInstance() {
        return dao;
    }

    @Override
    public void insert(Card card) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
        dateFormat.format(card.getDate());
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(CARD_INSERT_SQL)) {
            setStatement(statement, card);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
    }

    private static void setStatement(PreparedStatement statement, Card card) throws SQLException {
        BigDecimal decimal = new BigDecimal(card.getBalance());
        statement.setLong(1, card.getNumber());
        statement.setBigDecimal(2, decimal);
        statement.setString(3, card.getCurrency());
        statement.setLong(4, card.getAccountId());
        statement.setDate(5, new Date(card.getDate().getTime()));
        statement.setString(6, card.getType());
        statement.setString(7, card.getStatus().name());
    }

    private static Card getCard(ResultSet rs) throws SQLException {
        Card card = new Card();
        card.setNumber(rs.getLong(NUMBER));
        card.setBalance(rs.getDouble(BALANCE));
        card.setCurrency(rs.getString(CURRENCY));
        card.setDate(rs.getDate(DATE_OPEN));
        card.setType(rs.getString(TYPE_CARD_TYPE));
        card.setStatus(CardStatus.valueOf(rs.getString(STATUS)));
        return card;
    }

    public List<Card> getUserCards(Account account) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<Card> cards = new ArrayList<>();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(CARD_VIEW_SQL)) {
            statement.setInt(1, account.getId());
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    cards.add(getCard(rs));
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return cards;
    }

    public List<Card> getCardWithStatus(Account account, CardStatus status) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<Card> cards = new ArrayList<>();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(CARD_VIEW_WORKING)) {
            statement.setInt(1, account.getId());
            statement.setString(2, status.name());
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    cards.add(getCard(rs));
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return cards;
    }

    @Override
    public void update(Card card) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(CARD_UPDATE_STATUS)) {
            statement.setString(1, card.getStatus().name());
            statement.setLong(2, card.getNumber());
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
    }

    public void delete(Card card) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement statement = connection.prepareStatement(CARD_DROP)) {
                statement.setLong(1, card.getNumber());
                statement.executeUpdate();
            }
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
    }

    @Override
    public Card getByParam(Long number) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Card card = null;
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(CARD_VIEW_NUNBER)) {
            statement.setLong(1, number);
            try (ResultSet res = statement.executeQuery()) {
                if (res.next()) {
                    card = getCard(res);
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return card;
    }

    @Override
    public List<Card> getAll() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    public List<Card> getAllenCards(int id) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<Card> cards = new ArrayList<>();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(CARD_VIEW_ALLEN)) {
            statement.setInt(1, id);
            statement.setString(2, CardStatus.WORKING.name());
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    cards.add(getCard(rs));
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return cards;
    }

    public Map<Card, String> selectNewStatus() {
        Map<Card, String> cards = new HashMap<>();
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(CARD_NEW_STATUS)) {
            statement.setString(1, CardStatus.NEW.name());
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Card card = new Card();
                    card.setNumber(rs.getLong(NUMBER));
                    card.setBalance(rs.getDouble(BALANCE));
                    card.setCurrency(rs.getString(CURRENCY));
                    cards.put(card, rs.getString(LOGIN));
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return cards;
    }
}


