package edu.epam.task6.dao.impl;

import edu.epam.task6.dao.IDAO;
import edu.epam.task6.entity.impl.Operation;
import edu.epam.task6.entity.impl.status.OperationStatus;
import edu.epam.task6.dao.exception.DaoException;
import edu.epam.task6.pool.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>This class works with the entity of operation from database,
 * implementation of {@link edu.epam.task6.dao.IDAO}</p>
 *
 * @author Vladislav Gryadovkin
 */
public class OperationDao implements IDAO<Operation, Integer> {
    public static final String NAME = "name";
    public static final String OPER_ID = "oper_id";
    public static final String DESCRIPTION = "description";
    public static final String STATUS = "status";
    private static Logger logger = Logger.getLogger(OperationDao.class.getName());

    private static final OperationDao dao = new OperationDao();
    private final String OPERATION_GET_ALL = "select *from operation";
    private final String OPERATION_GET_ACTIVE = "select *from operation where status=? and oper_id<>?";
    private final String OPERATION_INSERT = "insert into operation(name,description) values(?,?)";
    private final String OPERATION_GET_ID = "select *from operation where name=?";
    private final String OPERATION_FIND = "select *from operation where oper_id=?";
    private final String OPERATION_UPDATE = "update operation set name=?, description=?  where oper_id=?";
    private final String CHANGE_STATUS = "update operation set status =? where oper_id=?";

    public static OperationDao getInstance() {
        return dao;
    }

    private OperationDao() {

    }

    @Override
    public List<Operation> getAll() {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<Operation> operations = new ArrayList<>();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(OPERATION_GET_ALL);
             ResultSet rs = statement.executeQuery()) {
            while (rs.next()) {
                operations.add(getOperation(rs));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return operations;
    }

    public Operation getByName(String name) {
        Operation operation = null;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(OPERATION_GET_ID)) {
            statement.setString(1, name);
            try (ResultSet rs = statement.executeQuery()) {
                if (rs.next()) {
                    operation = getOperation(rs);
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return operation;
    }

    @Override
    public void insert(Operation operation) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(OPERATION_INSERT)) {
            statement.setString(1, operation.getName());
            statement.setString(2, operation.getDescription());
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
    }

    @Override
    public Operation getByParam(Integer id) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(OPERATION_FIND)) {
            statement.setInt(1, id);
            try (ResultSet rs = statement.executeQuery()) {
                if (rs.next()) {
                    return getOperation(rs);
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return null;
    }

    private Operation getOperation(ResultSet rs) throws SQLException {
        Operation operation = new Operation();
        operation.setId(rs.getInt(OPER_ID));
        operation.setName(rs.getString(NAME));
        operation.setDescription(rs.getString(DESCRIPTION));
        operation.setStatus(OperationStatus.valueOf(rs.getString(STATUS)));
        return operation;
    }

    @Override
    public void update(Operation operation) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(OPERATION_UPDATE)) {
            statement.setString(1, operation.getName());
            statement.setString(2, operation.getDescription());
            statement.setInt(3, operation.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
    }

    public void changeStatus(Operation operation) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(CHANGE_STATUS)) {
            statement.setString(1, OperationStatus.ARCHIVE.name());
            statement.setInt(2, operation.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
    }

    public List<Operation> getActive() {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<Operation> operations = new ArrayList<>();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(OPERATION_GET_ACTIVE)) {
            statement.setString(1, OperationStatus.ACTIVE.name());
            statement.setInt(2, 3);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    operations.add(getOperation(rs));
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return operations;
    }
}


