package edu.epam.task6.dao.impl;

import edu.epam.task6.dao.IDAO;
import edu.epam.task6.entity.impl.Currency;
import edu.epam.task6.dao.exception.DaoException;
import edu.epam.task6.pool.ConnectionPool;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>This class works with the entity of currency from database,
 * implementation of {@link edu.epam.task6.dao.IDAO}</p>
 *
 * @author Vladislav Gryadovkin
 */
public class CurrencyDao implements IDAO<Currency, String> {
    public static final String COURSE = "course";
    public static final String NAME = "name";
    public static final String MAX_SUM = "max_sum";
    private static Logger logger = Logger.getLogger(CurrencyDao.class.getName());
    private static final CurrencyDao dao = new CurrencyDao();

    private CurrencyDao() {

    }

    private final String CURRENCY_INSERT = "insert into currency"
            + "(course,max_sum,name) values"
            + "(?,?,?)";
    private final String CURRENCY_GET_ALL = "select * from currency";
    private final String CURRENCY_UPDATE = "update currency set course=?,max_sum=? where name=?";
    private final String CURRENCY_GET_BY_NAME = "select * from currency where name=?";

    public static CurrencyDao getInstance() {
        return dao;
    }

    @Override
    public void insert(Currency currency) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(CURRENCY_INSERT)) {
            setStatement(statement, currency);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
    }

    @Override
    public void update(Currency currency) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(CURRENCY_UPDATE)) {
            setStatement(statement, currency);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
    }

    @Override
    public List<Currency> getAll() {
        List<Currency> currencies = new ArrayList<>();
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(CURRENCY_GET_ALL);
             ResultSet res = statement.executeQuery()) {
            while (res.next()) {
                currencies.add(getCurrency(res));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return currencies;
    }

    @Override
    public Currency getByParam(String name) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Currency currency = null;
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(CURRENCY_GET_BY_NAME)) {
            statement.setString(1, name);
            try (ResultSet res = statement.executeQuery()) {
                if (res.next()) {
                    currency = getCurrency(res);
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return currency;
    }

    private Currency getCurrency(ResultSet rs) throws SQLException {
        Currency currency = new Currency();
        currency.setCourse(rs.getDouble(COURSE));
        currency.setName(rs.getString(NAME));
        currency.setMax_sum(rs.getDouble(MAX_SUM));
        return currency;
    }

    private void setStatement(PreparedStatement statement, Currency currency) throws SQLException {
        BigDecimal decimal = new BigDecimal(currency.getMax_sum());
        BigDecimal course = new BigDecimal(currency.getCourse());
        statement.setBigDecimal(1, course);
        statement.setBigDecimal(2, decimal);
        statement.setString(3, currency.getName());
    }
}

