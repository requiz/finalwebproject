package edu.epam.task6.dao.impl;

import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.entity.impl.Operation;
import edu.epam.task6.entity.impl.Transaction;
import edu.epam.task6.entity.impl.status.OperationStatus;
import edu.epam.task6.entity.impl.status.TransactionStatus;
import edu.epam.task6.dao.exception.DaoException;
import edu.epam.task6.pool.ConnectionPool;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>This class works with the entity of transaction from database, it's also contains jdbc transactions
 * </p>
 *
 * @author Vladislav Gryadovkin
 */
public class TransactionDao {
    public static final String TR_ID = "tr_id";
    public static final String AMMOUNT = "ammount";
    public static final String CARD_NUMBER = "card_number";
    public static final String DATETIME = "datetime";
    public static final String NAME = "name";
    public static final String OPERATION_OPER_ID = "operation_oper_id";
    public static final String CARD_NUMBER_1 = "card_number1";
    public static final String DAY = "day";
    public static final String WEEK = "week";
    public static final String MONTH = "month";
    public static final String STATUS = "status";
    public static final String OPER_ID = "oper_id";
    public static final String OPERATION_STATUS = "operation.status";
    public static final String TRANSACTION_STATUS = "transaction.status";

    private final String CARD_UPDATE_BALANCE = "update card set balance=? where number=?";
    private final String TRANSACTION_GET_REPORT = "select tr_id,card_number,ammount,datetime,transaction.status, name," +
            "oper_id, operation.status " +
            "from transaction inner join operation on operation_oper_id = oper_id";
    private final String TRANSACTION_INSERT_PAY = "insert into TRANSACTION "
            + "(ammount,datetime,card_number,operation_oper_id) values"
            + "(?,?,?,?)";
    private final String TRANSACTION_INSERT_TRANSFER = "insert into TRANSACTION "
            + "(ammount,datetime,card_number,card_number1) values"
            + "(?,?,?,?)";
    private final String TRANSACTION_VIEW_ADMIN = " where card_number in " +
            " (select distinct card_number from transaction inner join card on  card_number = number " +
            "where account_acc_id =?)";
    private final String TRANSACTION_VIEW_CLIENT = "select *from TRANSACTION where tr_id=? and status=?";
    private final String TRANSACTION_GET = " where card_number in " +
            " (select distinct card_number from transaction inner join card on  card_number = number " +
            "where account_acc_id =?) and transaction.status = ?";
    private final String TRANSACTION_UPDATE_STATUS = "update TRANSACTION set status=? where tr_id=?";

    private final String TRANSACTION_GET_DAY = "  WHERE datetime >= CURDATE()";
    private final String TRANSACTION_GET_WEEK = " WHERE datetime >= DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY)";
    private final String TRANSACTION_MONTH = " WHERE datetime >= DATE_SUB(CURRENT_DATE, INTERVAL 1 month)";
    private static Logger logger = Logger.getLogger(TransactionDao.class.getName());
    private static final TransactionDao dao = new TransactionDao();

    public static TransactionDao getInstance() {
        return dao;
    }

    private TransactionDao() {

    }

    public boolean insertServiceTransaction(Transaction transaction, double balance) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement statement = connection.prepareStatement(TRANSACTION_INSERT_PAY)) {
                setStatement(statement, transaction);
                statement.setInt(4, transaction.getOperation());
                statement.executeUpdate();
            }
            try (PreparedStatement statement = connection.prepareStatement(CARD_UPDATE_BALANCE)) {
                statement.setDouble(1, balance);
                statement.setLong(2, transaction.getNumber());
                statement.executeUpdate();
            }
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return true;
    }

    private void setStatement(PreparedStatement statement, Transaction transaction) throws SQLException {
        Timestamp timestamp = new Timestamp(transaction.getDatetime().getTime());
        BigDecimal decimal = new BigDecimal(transaction.getAmmount());
        statement.setBigDecimal(1, decimal);
        statement.setTimestamp(2, timestamp);
        statement.setLong(3, transaction.getNumber());
    }

    public boolean insertTransferTransaction(Transaction transaction, double sumFrom, double sumTo) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement statement = connection.prepareStatement(TRANSACTION_INSERT_TRANSFER)) {
                setStatement(statement, transaction);
                statement.setLong(4, transaction.getNumberTo());
                statement.executeUpdate();
            }
            try (PreparedStatement statement = connection.prepareStatement(CARD_UPDATE_BALANCE)) {
                statement.setDouble(1, sumFrom);
                statement.setLong(2, transaction.getNumber());
                statement.executeUpdate();
            }
            try (PreparedStatement statement = connection.prepareStatement(CARD_UPDATE_BALANCE)) {
                statement.setDouble(1, sumTo);
                statement.setLong(2, transaction.getNumberTo());
                statement.executeUpdate();
            }
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return true;
    }

    private Transaction setTransaction(ResultSet rs) throws SQLException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String ts = rs.getTimestamp(DATETIME).toString();
        ts = ts.substring(0, ts.indexOf('.'));
        Transaction transaction = new Transaction();
        transaction.setId(rs.getInt(TR_ID));
        transaction.setAmmount(rs.getDouble(AMMOUNT));
        transaction.setNumber(rs.getLong(CARD_NUMBER));
        try {
            transaction.setDatetime(format.parse(ts));
        } catch (ParseException e) {
            logger.error(e.getMessage());
        }
        return transaction;
    }


    public Map<Transaction, Operation> getSelectedTransactions(Account account) {
        String query = TRANSACTION_GET_REPORT + TRANSACTION_GET;
        Map<Transaction, Operation> map = new HashMap<>();
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, account.getId());
            statement.setString(2, TransactionStatus.SAVE.name());
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Transaction transaction = setTransaction(rs);
                    Operation operation = new Operation();
                    operation.setId(rs.getInt(OPER_ID));
                    operation.setName(rs.getString(NAME));
                    operation.setStatus(OperationStatus.valueOf(rs.getString(OPERATION_STATUS)));
                    map.put(transaction, operation);
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return map;
    }

    public boolean cancelPaymentTransaction(Transaction transaction, double balanceFrom) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement statement = connection.prepareStatement(TRANSACTION_UPDATE_STATUS)) {
                statement.setString(1, transaction.getStatus().name());
                statement.setInt(2, transaction.getId());
                statement.executeUpdate();
            }
            try (PreparedStatement statement = connection.prepareStatement(CARD_UPDATE_BALANCE)) {
                statement.setDouble(1, balanceFrom);
                statement.setLong(2, transaction.getNumber());
                statement.executeUpdate();
            }
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return true;
    }

    public boolean cancelTransferTransaction(Transaction transaction, double balanceFrom, double balanceTo) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement statement = connection.prepareStatement(TRANSACTION_UPDATE_STATUS)) {
                statement.setString(1, transaction.getStatus().name());
                statement.setInt(2, transaction.getId());
                statement.executeUpdate();
            }
            try (PreparedStatement statement = connection.prepareStatement(CARD_UPDATE_BALANCE)) {
                statement.setDouble(1, balanceFrom);
                statement.setLong(2, transaction.getNumber());
                statement.executeUpdate();
            }
            try (PreparedStatement statement = connection.prepareStatement(CARD_UPDATE_BALANCE)) {
                statement.setDouble(1, balanceTo);
                statement.setLong(2, transaction.getNumberTo());
                statement.executeUpdate();
            }
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return true;
    }

    public Transaction getByParam(Integer number) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Transaction transaction = null;
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(TRANSACTION_VIEW_CLIENT)) {
            statement.setLong(1, number);
            statement.setString(2, TransactionStatus.SAVE.name());
            try (ResultSet res = statement.executeQuery()) {
                if (res.next()) {
                    transaction = setTransaction(res);
                    transaction.setOperation(res.getInt(OPERATION_OPER_ID));
                    transaction.setNumberTo(res.getLong(CARD_NUMBER_1));
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return transaction;
    }


    public Map<Transaction, Operation> getReport(String request) {
        Map<Transaction, Operation> map = new HashMap<>();
        String sqlcommand = null;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection()) {
            switch (request) {
                case DAY:
                    sqlcommand = TRANSACTION_GET_REPORT + TRANSACTION_GET_DAY;
                    break;
                case WEEK:
                    sqlcommand = TRANSACTION_GET_REPORT + TRANSACTION_GET_WEEK;
                    break;
                case MONTH:
                    sqlcommand = TRANSACTION_GET_REPORT + TRANSACTION_MONTH;
                    break;
            }
            try (PreparedStatement statement = connection.prepareStatement(sqlcommand);
                 ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Transaction transaction = setTransaction(rs);
                    transaction.setStatus(TransactionStatus.valueOf(rs.getString(TRANSACTION_STATUS)));
                    Operation operation = new Operation();
                    operation.setName(rs.getString(NAME));
                    map.put(transaction, operation);
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return map;
    }

    public Map<Transaction, Operation> getTransactionToAdmin(Account account) {
        String query = TRANSACTION_GET_REPORT + TRANSACTION_VIEW_ADMIN;
        Map<Transaction, Operation> map = new HashMap<>();
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, account.getId());
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    Transaction transaction = setTransaction(rs);
                    transaction.setStatus(TransactionStatus.valueOf(rs.getString(STATUS)));
                    Operation operation = new Operation();
                    operation.setName(rs.getString(NAME));
                    map.put(transaction, operation);
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return map;
    }
}



