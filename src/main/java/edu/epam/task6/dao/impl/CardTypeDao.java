package edu.epam.task6.dao.impl;

import edu.epam.task6.dao.IDAO;
import edu.epam.task6.entity.impl.CardType;
import edu.epam.task6.dao.exception.DaoException;
import edu.epam.task6.pool.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>This class works with the entity of cardType from database,
 * implementation of {@link edu.epam.task6.dao.IDAO}</p>
 *
 * @author Vladislav Gryadovkin
 */
public class CardTypeDao implements IDAO<CardType, Integer> {
    private final Logger logger = Logger.getLogger(CardTypeDao.class.getName());
    private final String CARD_TYPE = "card_type";
    private final String CARD_TYPE_SQL = "select card_type from type";
    private final String CARD_TYPE_INSERT = "insert into type"
            + "(card_type,description,rate) values"
            + "(?,?,?)";
    private static final CardTypeDao type = new CardTypeDao();

    public static CardTypeDao getInstance() {
        return type;
    }

    private CardTypeDao() {

    }

    public List<String> getCardTypes() {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        List<String> cardTypes = new ArrayList<>();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(CARD_TYPE_SQL);
             ResultSet rs = statement.executeQuery()) {
            while (rs.next()) {
                String type = rs.getString(CARD_TYPE);
                cardTypes.add(type);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
        return cardTypes;
    }

    @Override
    public void insert(CardType type) {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(CARD_TYPE_INSERT)) {
            statement.setString(1, type.getType());
            statement.setString(2, type.getDescription());
            statement.setDouble(3, type.getRate());
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new DaoException(e);
        }
    }

    @Override
    public void update(CardType obj) {
        throw new UnsupportedOperationException();
    }


    @Override
    public CardType getByParam(Integer param) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<CardType> getAll() {
        throw new UnsupportedOperationException();
    }
}

