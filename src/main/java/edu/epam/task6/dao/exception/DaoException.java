package edu.epam.task6.dao.exception;

/**
 * <p>This exception occurred when something wrong in the dao classes</p>
 *
 * @author Vladislav Gryadovkin
 */
public class DaoException extends RuntimeException {
    public DaoException() {
        super();
    }

    public DaoException(String message) {
        super(message);
    }

    public DaoException(Throwable throwable) {
        super(throwable);
    }

    public DaoException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
