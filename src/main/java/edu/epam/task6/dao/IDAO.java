package edu.epam.task6.dao;

import edu.epam.task6.entity.AbstractEntity;

import java.util.List;

/**
 * <p>This interface includes simple operations with which DAO is works</p>
 *
 * @param <S> class that is subclass of {@link edu.epam.task6.entity.AbstractEntity}
 * @param <T> main parameter of corresponding entity
 * @author Vladislav Gryadovkin
 */
public interface IDAO<S extends AbstractEntity, T> {
    /**
     * <p>This method is used for adding new entity to the database</p>
     *
     * @param obj entity which is inserted
     * @throws UnsupportedOperationException this error occurred when dao doesn't support method
     */
    void insert(S obj) throws UnsupportedOperationException;

    /**
     * <p>This method is used for updating entity in the database</p>
     *
     * @param obj entity which is updating
     * @throws UnsupportedOperationException this error occurred when dao doesn't support method
     */
    void update(S obj) throws UnsupportedOperationException;

    /**
     * <p>This method is used for getting entity from the database</p>
     *
     * @param param this is main parameter of corresponding entity
     * @return entity
     * @throws UnsupportedOperationException this error occurred when dao doesn't support method
     */
    S getByParam(T param) throws UnsupportedOperationException;

    /**
     * <p>This method is used for getting list of entity from the database</p>
     *
     * @return a list of entity
     * @throws UnsupportedOperationException this error occurred when dao doesn't support method
     */
    List<S> getAll() throws UnsupportedOperationException;

}
