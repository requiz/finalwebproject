package edu.epam.task6.controller;

import edu.epam.task6.command.Command;
import edu.epam.task6.manager.ConfigManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>Class is used to get requests from client, process them
 * by appropriate command and then generate http response.
 * @author Vladislav Gryadovkin
 * </p>
 */
public class MainController extends HttpServlet {
    public static final String PAGE_INDEX = "page.index";
    public static final String ERROR_POST = "error in POST ";
    public static final String ERROR_GET = "error in GET ";
    private Logger logger = Logger.getLogger(MainController.class.getName());

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        try {
            processRequest(request, response);
        } catch (ServletException | IOException e) {
            logger.error(ERROR_POST + e);
        }
    }


    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            processRequest(request, response);
        } catch (ServletException | IOException e) {
            logger.error(ERROR_GET + e);
        }
    }

    /**
     * <p>Method is used to process doGet and doPost </p>
     * @param request is http request from client
     * @param response is http response to client
     * @throws ServletException
     * @throws IOException
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page;
        CommandFactory action = CommandFactory.getInstance();
        Command command = action.defineCommand(request);
        page = command.execute(request);
        if (page != null) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            page = ConfigManager.getProperty(PAGE_INDEX);
            response.sendRedirect(request.getContextPath() + page);
        }
    }
}

