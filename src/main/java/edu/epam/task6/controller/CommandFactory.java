package edu.epam.task6.controller;

import edu.epam.task6.command.admin.ApproveCardCommand;
import edu.epam.task6.command.admin.SelectUserCommand;
import edu.epam.task6.command.admin.ViewAccountsCommand;
import edu.epam.task6.command.admin.ViewUserCardsCommand;
import edu.epam.task6.command.admin.insert.CurrencyCommand;
import edu.epam.task6.command.admin.insert.OperationCommand;
import edu.epam.task6.command.admin.transaction.ViewAllCommand;
import edu.epam.task6.command.admin.transaction.ViewToAdminCommand;
import edu.epam.task6.command.client.*;
import edu.epam.task6.command.client.card.AddCardCommand;
import edu.epam.task6.command.client.card.DropCardCommand;
import edu.epam.task6.command.client.card.ViewCardsCommand;
import edu.epam.task6.command.common.*;
import edu.epam.task6.command.common.profile.EditProfileCommand;
import edu.epam.task6.command.Command;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>This class is used to define commands that come from client request
 *
 * @author Vladislav Gryadovkin
 *         </p>
 */
public class CommandFactory {
    public static final String COMMAND = "command";
    private static Map<CommandEnum, Command> commandMap;

    private CommandFactory() {
        commandMap = new HashMap<>();
        commandMap.put(CommandEnum.LOGIN, new LoginCommand());
        commandMap.put(CommandEnum.LOGOUT, new LogoutCommand());
        commandMap.put(CommandEnum.REGISTRATION, new RegistrationCommand());
        commandMap.put(CommandEnum.ADDCARD, new AddCardCommand());
        commandMap.put(CommandEnum.VIEWCARDS, new ViewCardsCommand());
        commandMap.put(CommandEnum.CARDSTATUS, new CardStatusCommand());
        commandMap.put(CommandEnum.DROP, new DropCardCommand());
        commandMap.put(CommandEnum.CHANGE, new EditProfileCommand());
        commandMap.put(CommandEnum.PAY, new PayMoneyCommand());
        commandMap.put(CommandEnum.TRANSFER, new TransferMoneyCommand());
        commandMap.put(CommandEnum.VIEWALLTRANSACTIONS, new ViewAllCommand());
        commandMap.put(CommandEnum.VIEWTOADMIN, new ViewToAdminCommand());
        commandMap.put(CommandEnum.VIEWTOUSER, new ViewToUserCommand());
        commandMap.put(CommandEnum.CANCEL, new CancelTransactionCommand());
        commandMap.put(CommandEnum.VIEWUSERS, new ViewAccountsCommand());
        commandMap.put(CommandEnum.CURRENCY, new CurrencyCommand());
        commandMap.put(CommandEnum.APPROVECARD, new ApproveCardCommand());
        commandMap.put(CommandEnum.OPER, new OperationCommand());
        commandMap.put(CommandEnum.SELECTUSER, new SelectUserCommand());
        commandMap.put(CommandEnum.CHANGELANGUAGE, new ChangeLanguageCommand());
        commandMap.put(CommandEnum.MAIN, new MainCommand());
        commandMap.put(CommandEnum.VIEWUSERCARDS, new ViewUserCardsCommand());
        commandMap.put(CommandEnum.RECOVERY, new PasswordRecoveryCommand());
    }

    public static class ActionHolder {
        public static final CommandFactory factory = new CommandFactory();
    }

    public static CommandFactory getInstance() {
        return ActionHolder.factory;
    }

    /**
     * <p>This method is used to get request from client and find corresponding value in map</p>
     *
     * @param request - http request from client
     * @return appropriate command from map
     */
    public Command defineCommand(HttpServletRequest request) {
        Command current = commandMap.get(CommandEnum.MAIN);
        String command = request.getParameter(COMMAND);
        if (command == null || command.isEmpty()) {
            return current;
        } else
            current = commandMap.get(CommandEnum.valueOf(command.toUpperCase()));
        return current;
    }

    /**
     * <p> This method is used to get command by key</p>
     *
     * @param key - value from {@link edu.epam.task6.controller.CommandEnum}
     * @return appropriate command
     */
    public static Command getCommand(CommandEnum key) {

        return commandMap.get(key);
    }

}
