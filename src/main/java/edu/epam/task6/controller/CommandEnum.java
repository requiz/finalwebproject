package edu.epam.task6.controller;

/**
 * <p> This enum contains names of all commands, that is used in application
 * </p>
 *
 * @author Vladislav Gryadovkin
 */
public enum CommandEnum {
    VIEWUSERS, CURRENCY, APPROVECARD, REGISTRATION, LOGIN, LOGOUT, MAIN, ADDCARD, PAY, TRANSFER, CANCEL,
    VIEWCARDS, CARDSTATUS, DROP, CHANGE, VIEWALLTRANSACTIONS, VIEWTOADMIN, VIEWTOUSER, OPER, ACCSTATUS, SELECTUSER,
    CHANGELANGUAGE, VIEWUSERCARDS,RECOVERY;
}
