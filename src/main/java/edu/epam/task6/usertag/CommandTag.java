package edu.epam.task6.usertag;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
/**
 * <p>This class is used for extraction of command from url</p>
 *
 * @author Vladislav Gryadovkin
 */
public class CommandTag extends TagSupport {
    private static final Logger logger = Logger.getLogger(PageTag.class.getName());

    @Override
    public int doStartTag() {
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        String uri = request.getRequestURI();
        String parsedUri = uri.substring(uri.lastIndexOf("/") + 1, uri.lastIndexOf("."));
        try {
            JspWriter out = pageContext.getOut();
            out.write(parsedUri);
        } catch (IOException e) {
            logger.error(e);
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() {
        return EVAL_PAGE;
    }
}
