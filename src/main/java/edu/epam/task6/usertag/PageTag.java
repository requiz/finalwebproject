package edu.epam.task6.usertag;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * <p>This class is used for getting full url of jsp page</p>
 *
 * @author Vladislav Gryadovkin
 */
public class PageTag extends TagSupport {
    private static final Logger logger = Logger.getLogger(PageTag.class.getName());
    private String pattern = "finalproject";

    @Override
    public int doStartTag() {
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
        String uri = request.getRequestURI();
        int position = uri.indexOf(pattern) + pattern.length();
        String parsed = uri.substring(position);
        try {
            JspWriter out = pageContext.getOut();
            out.write(parsed);
        } catch (IOException e) {
            logger.error(e);
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() {
        return EVAL_PAGE;
    }
}
