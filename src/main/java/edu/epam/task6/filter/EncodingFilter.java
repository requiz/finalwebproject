package edu.epam.task6.filter;


import javax.servlet.*;
import java.io.IOException;
/**
 * <p>This class is used for setting UTF-8 encoding to http request and response </p>
 *
 * @author Vladislav Gryadovkin
 */
public class EncodingFilter implements Filter {
    public static final String ENCODING = "encoding";
    private String code;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        code = filterConfig.getInitParameter(ENCODING);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        String codeRequest = servletRequest.getCharacterEncoding();
        if (code != null && !code.equalsIgnoreCase(codeRequest)) {
            servletRequest.setCharacterEncoding(code);
            servletResponse.setCharacterEncoding(code);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }


    @Override
    public void destroy() {
        code = null;
    }
}
