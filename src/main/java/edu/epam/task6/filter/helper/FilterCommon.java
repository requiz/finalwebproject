package edu.epam.task6.filter.helper;

import edu.epam.task6.controller.CommandEnum;
import edu.epam.task6.controller.CommandFactory;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.entity.impl.status.AccountStatus;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>This class is acts as a helper for setting role if it's not defined </p>
 *
 * @author Vladislav Gryadovkin
 */
public abstract class FilterCommon {
    public static final String ACCOUNT = "account";

    public static Account getAccountFromSession(HttpServletRequest request) {
        if (request.getSession().getAttribute(ACCOUNT) != null) {
            return (Account) request.getSession().getAttribute(ACCOUNT);
        } else {
            Account unauthorized = new Account();
            unauthorized.setStatus(AccountStatus.GUEST);
            return unauthorized;
        }
    }

    public static String validateMainPage(Account account, HttpServletRequest request) {
        String page = null;
        switch (account.getStatus()) {
            case ADMIN:
                page = CommandFactory.getCommand(CommandEnum.APPROVECARD).execute(request);
                break;
            case USER:
                page = CommandFactory.getCommand(CommandEnum.VIEWCARDS).execute(request);
                break;
        }
        return page;
    }
}
