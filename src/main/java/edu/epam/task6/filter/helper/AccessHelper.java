package edu.epam.task6.filter.helper;

import edu.epam.task6.controller.CommandEnum;
import edu.epam.task6.entity.impl.status.AccountStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * <p>This class is describes commands that is defined by role of the user </p>
 *
 * @author Vladislav Gryadovkin
 */
public class AccessHelper {
    private static AccessHelper helper = new AccessHelper();
    private HashMap<AccountStatus, List<CommandEnum>> map = new HashMap<>();

    public static AccessHelper getInstance() {
        return helper;
    }

    private AccessHelper() {
        map.put(AccountStatus.USER, new ArrayList<>(Arrays.asList(CommandEnum.CANCEL,
                CommandEnum.LOGIN, CommandEnum.PAY, CommandEnum.TRANSFER, CommandEnum.ADDCARD, CommandEnum.CHANGELANGUAGE,
                CommandEnum.CHANGE, CommandEnum.LOGOUT, CommandEnum.MAIN, CommandEnum.VIEWCARDS,
                CommandEnum.VIEWTOUSER, CommandEnum.DROP, CommandEnum.CARDSTATUS)));

        map.put(AccountStatus.ADMIN, new ArrayList<>(Arrays.asList(CommandEnum.MAIN, CommandEnum.APPROVECARD,
                CommandEnum.VIEWUSERS, CommandEnum.CHANGE, CommandEnum.CHANGELANGUAGE,
                CommandEnum.VIEWUSERCARDS, CommandEnum.VIEWALLTRANSACTIONS, CommandEnum.CURRENCY,
                CommandEnum.CARDSTATUS, CommandEnum.LOGOUT, CommandEnum.ACCSTATUS, CommandEnum.OPER,
                CommandEnum.SELECTUSER, CommandEnum.DROP, CommandEnum.LOGIN)));

        map.put(AccountStatus.GUEST, new ArrayList<>(Arrays.asList(CommandEnum.MAIN, CommandEnum.CHANGELANGUAGE,
                CommandEnum.REGISTRATION, CommandEnum.LOGIN, CommandEnum.RECOVERY)));

    }

    public Boolean validateAccess(AccountStatus status, CommandEnum commandEnum) {
        Boolean result = false;
        if (map.get(status) != null && map.get(status).contains(commandEnum)) {
            result = true;
        }
        return result;
    }

    public CommandEnum validateCommand(String command) {
        if (command == null || command.isEmpty()) {
            return CommandEnum.MAIN;
        } else return CommandEnum.valueOf(command.toUpperCase());
    }
}
