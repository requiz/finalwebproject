package edu.epam.task6.filter;

import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.filter.helper.FilterCommon;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>This class is used for filtering main page for client and admin </p>
 *
 * @author Vladislav Gryadovkin
 */
public class RoleDefineFilter implements Filter {
    private FilterConfig config;
    private ServletContext context;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.config = filterConfig;
        context = config.getServletContext();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        Account account = FilterCommon.getAccountFromSession(request);
        String mainPage = FilterCommon.validateMainPage(account, request);
        if (mainPage != null) {
            RequestDispatcher dispatcher = request.getRequestDispatcher(mainPage);
            dispatcher.forward(request, response);
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}

