package edu.epam.task6.filter;

import edu.epam.task6.controller.CommandEnum;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.filter.helper.AccessHelper;
import edu.epam.task6.filter.helper.FilterCommon;
import edu.epam.task6.manager.ConfigManager;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * <p>This class is used for filtering all commands by role </p>
 *
 * @author Vladislav Gryadovkin
 */
public class AccessFilter implements Filter {
    public static final String ERROR = "page.error403";
    public static final String COMMAND = "command";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        AccessHelper helper = AccessHelper.getInstance();
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        Account account = FilterCommon.getAccountFromSession(request);
        String command = request.getParameter(COMMAND);
        CommandEnum value = helper.validateCommand(command);
        if (!helper.validateAccess(account.getStatus(), value)) {
            RequestDispatcher dispatcher = request.getRequestDispatcher(ConfigManager.getProperty(ERROR));
            dispatcher.forward(request, response);
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
