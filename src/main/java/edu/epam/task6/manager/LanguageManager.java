package edu.epam.task6.manager;

/**
 * <p>This class is used for defining main languages of the application</p>
 *
 * @author Vladislav Gryadovkin
 */
public enum LanguageManager {
    RU("ru_Ru"), EN("en_US");
    private final String language;

    @Override
    public String toString() {
        return this.language;
    }

    LanguageManager(String language) {
        this.language = language;
    }
}
