package edu.epam.task6.manager;

import java.util.Locale;
import java.util.ResourceBundle;
/**
 * <p>This class is used for getting corresponding message from property file</p>
 *
 * @author Vladislav Gryadovkin
 */
public class MessageManager {
    private final static ResourceBundle RU_BUNDLE = ResourceBundle.getBundle("properties.messages",
            new Locale("ru", "RU"));
    private final static ResourceBundle EN_BUNDLE = ResourceBundle.getBundle("properties.messages",
            new Locale("en", "US"));

    private MessageManager() {

    }

    public static String getProperty(String key, String language) {
        String message;
        if (language != null && language.equals(LanguageManager.EN.toString())) {
            message = EN_BUNDLE.getString(key);
        } else message = RU_BUNDLE.getString(key);

        return message;
    }
}
