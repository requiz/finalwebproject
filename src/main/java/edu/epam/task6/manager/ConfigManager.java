package edu.epam.task6.manager;

import java.util.ResourceBundle;
/**
 * <p>This class is used for retrieving information about pages from appropriate property file</p>
 *
 * @author Vladislav Gryadovkin
 */
public class ConfigManager {
    private static final ResourceBundle bundle = ResourceBundle.getBundle("properties.pages");

    private ConfigManager() {

    }

    public static String getProperty(String key) {

        return bundle.getString(key);
    }
}
