package edu.epam.task6.entity.impl.status;

/**
 * <p>This enum is used to describe roles of account entity</p>
 *
 * @author Vladislav Gryadovkin
 */
public enum AccountStatus {
    USER, ADMIN, GUEST;
}
