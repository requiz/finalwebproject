package edu.epam.task6.entity.impl;

import edu.epam.task6.entity.AbstractEntity;
import edu.epam.task6.entity.impl.status.TransactionStatus;

import java.util.Date;

/**
 * <p>This class is describes transaction entity from database</p>
 *
 * @author Vladislav Gryadovkin
 * @see edu.epam.task6.entity.AbstractEntity
 */
public class Transaction implements AbstractEntity {
    private int id;
    private Double ammount;
    private Date datetime;
    private long numberFrom;
    private int operation;
    private long numberTo;

    public TransactionStatus getStatus() {

        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    private TransactionStatus status;

    public long getNumberTo() {
        return numberTo;
    }

    public void setNumberTo(long numberTo) {
        this.numberTo = numberTo;
    }


    public Transaction(Double ammount, Date datetime, long number, int operation) {
        this.ammount = ammount;
        this.datetime = datetime;
        this.numberFrom = number;
        this.operation = operation;
        status = TransactionStatus.SAVE;
    }

    public Transaction(Double ammount, Date datetime, long number, long numberTo) {
        this.ammount = ammount;
        this.datetime = datetime;
        this.numberFrom = number;
        this.numberTo = numberTo;
        status = TransactionStatus.SAVE;
    }

    public Transaction() {

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getAmmount() {
        return ammount;
    }

    public void setAmmount(Double ammount) {
        this.ammount = ammount;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public long getNumber() {
        return numberFrom;
    }

    public void setNumber(long number) {
        this.numberFrom = number;
    }

    public int getOperation() {
        return operation;
    }

    public void setOperation(int operation) {
        this.operation = operation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transaction)) return false;

        Transaction that = (Transaction) o;

        if (id != that.id) return false;
        if (numberFrom != that.numberFrom) return false;
        if (operation != that.operation) return false;
        if (numberTo != that.numberTo) return false;
        if (ammount != null ? !ammount.equals(that.ammount) : that.ammount != null) return false;
        if (datetime != null ? !datetime.equals(that.datetime) : that.datetime != null) return false;
        return status == that.status;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (ammount != null ? ammount.hashCode() : 0);
        result = 31 * result + (datetime != null ? datetime.hashCode() : 0);
        result = 31 * result + (int) (numberFrom ^ (numberFrom >>> 32));
        result = 31 * result + operation;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (int) (numberTo ^ (numberTo >>> 32));
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" id ").append(id);
        sb.append(" ammount ").append(ammount);
        sb.append(" datetime ").append(datetime);
        sb.append(" numberFrom ").append(numberFrom);
        sb.append(" operation ").append(operation);
        sb.append(" status ").append(status);
        sb.append(" numberTo ").append(numberTo);
        return sb.toString();
    }
}
