package edu.epam.task6.entity.impl;

import edu.epam.task6.entity.AbstractEntity;
import edu.epam.task6.entity.impl.status.OperationStatus;

/**
 * <p>This class is describes operation entity from database</p>
 *
 * @author Vladislav Gryadovkin
 * @see edu.epam.task6.entity.AbstractEntity
 */
public class Operation implements AbstractEntity {
    private int id;
    private String name;
    private String description;
    private OperationStatus status;

    public Operation(String name, String description) {
        this.name = name;
        this.description = description;
        status = OperationStatus.ACTIVE;
    }

    public Operation(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
        status = OperationStatus.ACTIVE;
    }

    public Operation() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Operation)) return false;

        Operation operation = (Operation) o;

        if (id != operation.id) return false;
        if (name != null ? !name.equals(operation.name) : operation.name != null) return false;
        return !(description != null ? !description.equals(operation.description) : operation.description != null);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    public OperationStatus getStatus() {
        return status;
    }

    public void setStatus(OperationStatus status) {
        this.status = status;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" id ").append(id);
        sb.append(" name ").append(name);
        sb.append(" description ").append(description);
        return sb.toString();
    }
}
