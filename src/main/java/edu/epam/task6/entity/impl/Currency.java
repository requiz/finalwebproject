package edu.epam.task6.entity.impl;

import edu.epam.task6.entity.AbstractEntity;

/**
 * <p>This class is describes currency entity from database</p>
 *
 * @author Vladislav Gryadovkin
 * @see edu.epam.task6.entity.AbstractEntity
 */
public class Currency implements AbstractEntity {
    private String name;
    private Double course;
    private Double max_sum;

    public Currency(String name, Double course, Double max_sum) {
        this.name = name;
        this.course = course;
        this.max_sum = max_sum;
    }

    public Currency() {

    }

    public Double getMax_sum() {
        return max_sum;
    }

    public void setMax_sum(double max_sum) {
        this.max_sum = max_sum;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (!(o instanceof Currency)) return false;

        Currency currency = (Currency) o;

        if (Double.compare(currency.course, course) != 0) return false;
        if (Double.compare(currency.max_sum, max_sum) != 0) return false;
        return !(name != null ? !name.equals(currency.name) : currency.name != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = name != null ? name.hashCode() : 0;
        temp = Double.doubleToLongBits(course);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(max_sum);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCourse() {
        return course;
    }

    public void setCourse(double course) {
        this.course = course;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" name ").append(name);
        sb.append(" course ").append(course);
        sb.append(" max_ sum ").append(max_sum);
        return sb.toString();
    }


}
