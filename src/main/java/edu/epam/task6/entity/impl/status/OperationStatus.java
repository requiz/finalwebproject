package edu.epam.task6.entity.impl.status;
/**
 * <p>This enum is used to describe status of operation entity</p>
 *
 * @author Vladislav Gryadovkin
 */
public enum OperationStatus {
    ACTIVE,ARCHIVE;
}
