package edu.epam.task6.entity.impl.status;
/**
 * <p>This enum is used to describe status of card entity</p>
 *
 * @author Vladislav Gryadovkin
 */
public enum CardStatus {
    DISAPPROVED, ADMIN_BLOCKED, NEW, BLOCKED, WORKING;
}

