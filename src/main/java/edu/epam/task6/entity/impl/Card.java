package edu.epam.task6.entity.impl;

import edu.epam.task6.entity.AbstractEntity;
import edu.epam.task6.entity.impl.status.CardStatus;

import java.util.Date;
/**
 * <p>This class is describes card entity from database</p>
 *
 * @author Vladislav Gryadovkin
 * @see edu.epam.task6.entity.AbstractEntity
 */
public class Card implements AbstractEntity {
    private long number;
    private Double balance;
    private String type;
    private String currency;
    private int accountId;
    private CardStatus status;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    private Date date;

    public Card(long number, Double balance, String type,
                String currency, int accountId, Date date, CardStatus status) {
        this.number = number;
        this.balance = balance;
        this.type = type;
        this.currency = currency;
        this.accountId = accountId;
        this.date = date;
        this.type = type;
        this.status = status;
    }

    public Card() {

    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Card)) return false;

        Card card = (Card) o;

        if (number != card.number) return false;
        if (Double.compare(card.balance, balance) != 0) return false;
        if (accountId != card.accountId) return false;
        if (type != null ? !type.equals(card.type) : card.type != null) return false;
        if (currency != null ? !currency.equals(card.currency) : card.currency != null) return false;
        if (status != card.status) return false;
        return !(date != null ? !date.equals(card.date) : card.date != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (number ^ (number >>> 32));
        temp = Double.doubleToLongBits(balance);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        result = 31 * result + accountId;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" number ").append(number);
        sb.append(" balance ").append(balance);
        sb.append(" type ").append(type);
        sb.append(" currency ").append(currency);
        sb.append(" accountId ").append(accountId);
        sb.append(" status ").append(status);
        return number + " " + balance + " " + type + " " + currency + " " + accountId + " " + date + " " + status;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public CardStatus getStatus() {
        return status;
    }

    public void setStatus(CardStatus status) {
        this.status = status;
    }
}
