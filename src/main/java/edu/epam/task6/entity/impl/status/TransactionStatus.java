package edu.epam.task6.entity.impl.status;
/**
 * <p>This enum is used to describe status of transaction entity</p>
 *
 * @author Vladislav Gryadovkin
 */
public enum TransactionStatus {
    SAVE, CANCEL;
}
