package edu.epam.task6.entity.impl;

import edu.epam.task6.entity.AbstractEntity;
import edu.epam.task6.entity.impl.status.AccountStatus;

/**
 * <p>This class is describes account entity from database</p>
 *
 * @author Vladislav Gryadovkin
 * @see edu.epam.task6.entity.AbstractEntity
 */
public class Account implements AbstractEntity {
    private int id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String sex;
    private String contactNumber;
    private String email;
    private AccountStatus status;


    public Account(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public Account() {

    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Account)) return false;

        Account account = (Account) o;

        if (id != account.id) return false;
        if (login != null ? !login.equals(account.login) : account.login != null) return false;
        if (password != null ? !password.equals(account.password) : account.password != null) return false;
        if (firstName != null ? !firstName.equals(account.firstName) : account.firstName != null) return false;
        if (lastName != null ? !lastName.equals(account.lastName) : account.lastName != null) return false;
        if (sex != null ? !sex.equals(account.sex) : account.sex != null) return false;
        if (contactNumber != null ? !contactNumber.equals(account.contactNumber) : account.contactNumber != null)
            return false;
        if (email != null ? !email.equals(account.email) : account.email != null) return false;
        return status == account.status;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (sex != null ? sex.hashCode() : 0);
        result = 31 * result + (contactNumber != null ? contactNumber.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" id ").append(id);
        sb.append(" login  ").append(login);
        sb.append(" firstName ").append(firstName);
        sb.append(" lastName ").append(lastName);
        sb.append(" sex ").append(sex);
        sb.append(" contactNumber ").append(contactNumber);
        sb.append(" email").append(email);
        sb.append(" status ").append(status);
        return sb.toString();
    }
}
