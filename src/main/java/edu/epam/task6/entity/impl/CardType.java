package edu.epam.task6.entity.impl;

import edu.epam.task6.entity.AbstractEntity;

/**
 * <p>This class is describes cardType entity from database</p>
 *
 * @author Vladislav Gryadovkin
 * @see edu.epam.task6.entity.AbstractEntity
 */
public class CardType implements AbstractEntity {
    private String type;
    private String description;
    private double rate;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CardType)) return false;

        CardType cardType = (CardType) o;

        if (Double.compare(cardType.rate, rate) != 0) return false;
        if (type != null ? !type.equals(cardType.type) : cardType.type != null) return false;
        return !(description != null ? !description.equals(cardType.description) : cardType.description != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = type != null ? type.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        temp = Double.doubleToLongBits(rate);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" type ").append(type);
        sb.append(" description ").append(description);
        sb.append(" rate ").append(rate);
        return sb.toString();
    }
}
