package edu.epam.task6.entity;

/**
 * <p>This interface is used as a marker to describe entities from database</p>
 * @author Vladislav Gryadovkin
 */
public interface AbstractEntity {
}
