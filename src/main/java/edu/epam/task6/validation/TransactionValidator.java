package edu.epam.task6.validation;

import edu.epam.task6.entity.impl.Operation;
import edu.epam.task6.entity.impl.Transaction;
import edu.epam.task6.entity.impl.status.OperationStatus;
import edu.epam.task6.validation.exception.ValidationException;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>This class is used for validating fields of transaction entity</p>
 *
 * @author Vladislav Gryadovkin
 */
public abstract class TransactionValidator {
    public static final String MESSAGE_CANCEL = "message.cancelTransaction";
    public static final String MESSAGE_BALANCE_ERROR = "message.payError";

    public static Transaction validateTransaction(String cardNumber, Operation operation, String sum)
            throws ValidationException {
        double payment;
        long number = FieldsValidator.validateNumber(cardNumber);
        if (FieldsValidator.validateMoney(sum)) {
            payment = FieldsValidator.doubleValidator(sum);
        } else throw new ValidationException(MESSAGE_BALANCE_ERROR);
        Date existTransaction = new Date();
        Transaction transaction = new Transaction(payment, existTransaction, number, operation.getId());
        return transaction;
    }

    public static Transaction validateTransfer(String ownCard, String allenCard, String sum) throws ValidationException {
        Transaction transaction;
        double transferSum;
        long ownNumber = FieldsValidator.validateNumber(ownCard);
        long allenNumber = FieldsValidator.validateNumber(allenCard);
        if (FieldsValidator.validateMoney(sum)) {
            transferSum = FieldsValidator.doubleValidator(sum);
        } else throw new ValidationException(MESSAGE_BALANCE_ERROR);
        Date existTransaction = new Date();
        transaction = new Transaction(transferSum, existTransaction, ownNumber, allenNumber);
        return transaction;
    }

    public static HashMap<Transaction, Long> validateForCancel(Map<Transaction, Operation> transactions) {
        HashMap<Transaction, Long> positions = new HashMap<>();
        for (Transaction transaction : transactions.keySet()) {
            Timestamp nowTimeStamp = new Timestamp(new Date().getTime());
            Timestamp timestamp = new Timestamp(transaction.getDatetime().getTime());
            long seconds = (nowTimeStamp.getTime() - timestamp.getTime()) / 1000;
            if (seconds < 60) {
                positions.put(transaction, transaction.getNumber());
            }
        }
        return positions;
    }

    public static HashMap<Integer, String> validateArchiveOper(Map<Transaction, Operation> transactions) {
        HashMap<Integer, String> operations = new HashMap<>();
        for (Map.Entry<Transaction, Operation> operationEntry : transactions.entrySet()) {
            Operation operation = operationEntry.getValue();
            if (operation.getStatus().equals(OperationStatus.ARCHIVE)) {
                operations.put(operation.getId(), operation.getName());
            }
        }
        return operations;
    }

    public static void checkTimeForCancel(Transaction transaction) throws ValidationException {
        Timestamp nowTimeStamp = new Timestamp(new Date().getTime());
        Timestamp timestamp = new Timestamp(transaction.getDatetime().getTime());
        long seconds = (nowTimeStamp.getTime() - timestamp.getTime()) / 1000;
        if (seconds > 60) throw new ValidationException(MESSAGE_CANCEL);
    }
}

