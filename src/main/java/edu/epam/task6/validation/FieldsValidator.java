package edu.epam.task6.validation;

import edu.epam.task6.validation.exception.ValidationException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>This class is used for validating fields by patterns</p>
 *
 * @author Vladislav Gryadovkin
 */
public abstract class FieldsValidator {
    private static final String LOGIN_MATCHER = "^[a-zA-Z0-9]{3,16}$";
    private static final String PASSWORD_MATCHER = "^[a-z0-9]{3,16}$";
    private static final String FIO_MATCHER = "([a-zA-Z]{3,30})";
    private static final String NUMBER_MATCHER = "\\d{2}[\\-]\\d{3}[\\-]\\d{4}";
    private static final String EMAIL_MATCHER = "^(.+)@(.+)$";
    private static final String NUMBER_REGEX = "\\d+";
    private static final String TEXT_REGEX = "^[^0-9].*";
    private static final String URI_REGEX = "^([\\w\\-]+)";
    private static final String MONEY_REGEX = "\\d{1,12}";
    private static final String PAY_BALANCE = "pay.balance";
    private static final String INTEGER_REGEX = "^\\d+$";
    public static final String MESSAGE_NUMBER_ERROR = "message.numberError";

    public static boolean validateLogin(String login) {
        Boolean result = false;
        if (validateField(login)) {
            result = login.matches(LOGIN_MATCHER);
        }
        return result;
    }

    public static boolean validatePassword(String password) {
        Boolean result = false;
        if (validateField(password)) {
            result = password.matches(PASSWORD_MATCHER);
        }
        return result;
    }

    public static boolean validateConfirm(String password, String repPass) {
        Boolean result = false;
        if (validatePassword(password) && password.equals(repPass)) {
            result = true;
        }
        return result;
    }

    public static boolean validateEmail(String email) {
        Boolean result = false;
        if (validateField(email)) {
            result = email.matches(EMAIL_MATCHER);
        }
        return result;
    }

    public static boolean validateFio(String firstName, String lastName) {
        Boolean result = false;
        if (validateField(firstName) && validateField(lastName)) {
            result = (firstName.matches(FIO_MATCHER) && lastName.matches(FIO_MATCHER));
        }
        return result;
    }

    public static boolean validateField(String field) {
        return field != null && !field.isEmpty();
    }

    public static boolean validatePhoneNumber(String contactNumber) {
        Boolean result = false;
        if (validateField(contactNumber)) {
            result = contactNumber.matches(NUMBER_MATCHER);
        }
        return result;
    }

    public static long validateNumber(String number) {
        long card = 0;
        Pattern pattern = Pattern.compile(NUMBER_REGEX);
        Matcher matcher = pattern.matcher(number);
        if (matcher.find()) {
            card = Long.parseLong(matcher.group());
        }
        return card;
    }

    public static boolean validateText(String text) {
        boolean result = false;
        if (!FieldsValidator.validateField(text)) {
            return false;
        } else {
            Pattern pattern = Pattern.compile(TEXT_REGEX);
            Matcher matcher = pattern.matcher(text);
            if (matcher.find()) {
                result = true;
            }
        }
        return result;
    }

    public static StringBuilder validateUri(StringBuilder text) {
        StringBuilder command = new StringBuilder();
        Pattern pattern = Pattern.compile(URI_REGEX);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            command.append(matcher.group());
        }
        return command;
    }

    public static double doubleValidator(String sum) throws ValidationException {
        double value;
        try {
            value = Double.parseDouble(sum);
        } catch (NumberFormatException e) {
            throw new ValidationException(PAY_BALANCE);
        }
        return value;
    }

    public static boolean validateMoney(String money) {
        Boolean result = false;
        Pattern pattern = Pattern.compile(MONEY_REGEX);
        Matcher matcher = pattern.matcher(money);
        if (matcher.matches()) {
            result = true;
        }
        return result;
    }

    public static boolean validateInteger(String value) {
        boolean result = false;
        if (!FieldsValidator.validateField(value)) {
            return false;
        } else {
            Pattern pattern = Pattern.compile(INTEGER_REGEX);
            Matcher matcher = pattern.matcher(value);
            if (matcher.matches()) {
                result = true;
            }
        }
        return result;
    }

    public static int validateId(String id) throws ValidationException {
        int validId;
        if (FieldsValidator.validateInteger(id)) {
            validId = Integer.parseInt(id);
        } else throw new ValidationException(MESSAGE_NUMBER_ERROR);
        return validId;
    }
}
