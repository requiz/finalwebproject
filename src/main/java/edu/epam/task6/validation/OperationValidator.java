package edu.epam.task6.validation;

import edu.epam.task6.entity.impl.Operation;
import edu.epam.task6.validation.exception.ValidationException;

/**
 * <p>This class is used for validating fields of operation entity</p>
 *
 * @author Vladislav Gryadovkin
 */
public abstract class OperationValidator {
    public static final String OPERATION_ERROR = "operation.error";

    public static void validateNewOperation(Operation operation) throws ValidationException {
        if (!FieldsValidator.validateText(operation.getName()) ||
                !FieldsValidator.validateText(operation.getDescription())) {
            throw new ValidationException(OPERATION_ERROR);
        }
    }

    public static int validateId(String id) {
        if (FieldsValidator.validateInteger(id)) {
            return Integer.parseInt(id);
        } else return 0;
    }
}
