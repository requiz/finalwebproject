package edu.epam.task6.validation;

import org.apache.log4j.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
/**
 * <p>This class is used for ciphering password to MD5</p>
 *
 * @author Vladislav Gryadovkin
 */
public class Cipher {

    public static final String MD_5 = "MD5";
    private static final Logger logger = Logger.getLogger(Cipher.class.getName());

    public static String getHex(String password) {
        MessageDigest md;
        StringBuffer sb = null;
        try {
            md = MessageDigest.getInstance(MD_5);
            md.update(password.getBytes());

            byte byteData[] = md.digest();
            sb = new StringBuffer();
            for (byte aByteData : byteData) {
                sb.append(Integer.toString((aByteData & 0xff) + 0x100, 16).substring(1));
            }
        } catch (NoSuchAlgorithmException e) {
            logger.error(e);
        }
        return sb != null ? sb.toString() : null;
    }
}

