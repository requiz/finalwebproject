package edu.epam.task6.validation;

import edu.epam.task6.entity.impl.Currency;
import edu.epam.task6.validation.exception.ValidationException;

/**
 * <p>This class is used for validating fields of currency entity</p>
 *
 * @author Vladislav Gryadovkin
 */
public abstract class CurrencyValidator {

    public static final String CURRENCY_INPUT_ERROR = "currency.empty";
    public static final String CURRENCY_EMPTY = "currency.error";

    public static Currency validateCurrency(String name, String course, String max_sum) throws ValidationException {
        double courseBY;
        double sum;
        Currency currency;
        if (!FieldsValidator.validateField(name)) {
            throw new ValidationException(CURRENCY_INPUT_ERROR);
        } else if (FieldsValidator.validateMoney(course) && FieldsValidator.validateMoney(max_sum)) {
            courseBY = FieldsValidator.doubleValidator(course);
            sum = FieldsValidator.doubleValidator(max_sum);
        } else throw new ValidationException(CURRENCY_EMPTY);
        currency = new Currency(name, courseBY, sum);
        return currency;
    }
}
