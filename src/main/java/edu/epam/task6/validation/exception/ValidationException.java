package edu.epam.task6.validation.exception;

/**
 * <p>This class is used for description of errors which occurred when something wrong
 * during validating fields</p>
 *
 * @author Vladislav Gryadovkin
 */
public class ValidationException extends Exception {

    public ValidationException() {
        super();
    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(Throwable throwable) {
        super(throwable);
    }

    public ValidationException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
