package edu.epam.task6.validation;

import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.entity.impl.Card;
import edu.epam.task6.entity.impl.status.CardStatus;
import edu.epam.task6.validation.exception.ValidationException;

import java.util.*;

/**
 * <p>This class is used for validating fields of card entity</p>
 *
 * @author Vladislav Gryadovkin
 */
public abstract class CardValidator {
    private static final long START = 10000000L;
    private static final long FINISH = 99999999L;
    public static final String MESSAGE_CARD_CREATE_ERROR = "message.cardCreateError";
    public static final String MESSAGE_CREATION_ERROR = "message.creationError";
    public static final String MESSAGE_FIELDSERROR = "message.fieldserror";
    public static final String MESSAGE_NUMBER_ERROR = "message.numberError";

    public static Card validateNewCard(String type, String balance, String currency, Account account)
            throws ValidationException {
        double sum;
        if (!FieldsValidator.validateField(type) || !FieldsValidator.validateField(currency)) {
            throw new ValidationException(MESSAGE_CREATION_ERROR);
        } else if (FieldsValidator.validateMoney(balance)) {
            sum = FieldsValidator.doubleValidator(balance);
        } else {
            throw new ValidationException(MESSAGE_CARD_CREATE_ERROR);
        }
        Random random = new Random();
        long number = START + ((long) (random.nextDouble() * (FINISH - START)));
        Date openDate = new Date();
        Card card = new Card(number, sum, type, currency, account.getId(), openDate, CardStatus.NEW);
        return card;
    }

    public static Long validateNumber(String parameter) throws ValidationException {
        long number;
        if (!FieldsValidator.validateField(parameter)) {
            throw new ValidationException(MESSAGE_FIELDSERROR);
        }
        number = FieldsValidator.validateNumber(parameter);
        if (number == 0) {
            throw new ValidationException(MESSAGE_NUMBER_ERROR);
        }
        return number;
    }

}
