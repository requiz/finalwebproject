package edu.epam.task6.validation;

import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.validation.exception.ValidationException;

import java.util.Date;

/**
 * <p>This class is used for validating fields of account entity</p>
 *
 * @author Vladislav Gryadovkin
 */
public abstract class AccountValidator {
    public static final String MESSAGE_PASSWORD = "message.passwordError";
    public static final String MESSAGE_CONFIRM_PASSWORD = "message.confirmPassword";
    public static final String MESSAGE_LOGINERROR = "message.loginerror";
    public static final String MESSAGE_EMAILERROR = "message.emailerror";
    public static final String MESSAGE_FIOERROR = "message.fioerror";
    public static final String MESSAGE_PHONE_NUMBERERROR = "message.phoneNumbererror";
    public static final String MESSAGE_PASSWORD_ERROR = "message.passwordError";
    public static final String EMPTY = "empty";

    public static void validateAccount(Account account) throws ValidationException {
        if (!FieldsValidator.validateLogin(account.getLogin())) {
            throw new ValidationException(MESSAGE_LOGINERROR);
        }
        if (!FieldsValidator.validatePassword(account.getPassword())) {
            throw new ValidationException(MESSAGE_PASSWORD_ERROR);
        }
    }

    public static void validateNewAccount(Account account, String repPassword)
            throws ValidationException {
        validatePersonalInfo(account);
        passConfirm(account.getPassword(), repPassword);
    }

    public static void validatePasswordRecovery(Account account) throws ValidationException {
        if (!FieldsValidator.validateLogin(account.getLogin())) {
            throw new ValidationException(MESSAGE_LOGINERROR);
        }
        if (!FieldsValidator.validateEmail(account.getEmail())) {
            throw new ValidationException(MESSAGE_EMAILERROR);
        }
    }

    public static void validatePersonalInfo(Account accountConfirm) throws ValidationException {
        if (!FieldsValidator.validateLogin(accountConfirm.getLogin())) {
            throw new ValidationException(MESSAGE_LOGINERROR);
        }
        if (!FieldsValidator.validateEmail(accountConfirm.getEmail())) {
            throw new ValidationException(MESSAGE_EMAILERROR);
        }
        if (!FieldsValidator.validateFio(accountConfirm.getFirstName(), accountConfirm.getLastName())) {
            throw new ValidationException(MESSAGE_FIOERROR);
        }
        if (!FieldsValidator.validatePhoneNumber(accountConfirm.getContactNumber())) {
            throw new ValidationException(MESSAGE_PHONE_NUMBERERROR);
        }
    }

    public static void validatePassword(Account account, String prevPassword, String newPassword, String confirmPassword)
            throws ValidationException {
        passHexConfirm(account, Cipher.getHex(prevPassword));
        passConfirm(newPassword, confirmPassword);
    }

    private static void passConfirm(String pass, String confirmPass) throws ValidationException {
        if (!FieldsValidator.validateConfirm(pass, confirmPass)) {
            throw new ValidationException(MESSAGE_CONFIRM_PASSWORD);
        }
    }

    private static void passHexConfirm(Account account, String password) throws ValidationException {
        if (!account.getPassword().equals(password)) {
            throw new ValidationException(MESSAGE_PASSWORD);
        }
    }

    public static String validateNewPassword() throws ValidationException {
        String password;
        Date date = new Date();
        int passHashcode = Math.abs(date.hashCode());
        password = Integer.toString(passHashcode);
        if (password.isEmpty()) {
            throw new ValidationException(EMPTY);
        }
        return password;
    }
}