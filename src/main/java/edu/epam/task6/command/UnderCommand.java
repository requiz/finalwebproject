package edu.epam.task6.command;

/**
 * <p>This interface is used as a marker for commands that is not defined in
 * {@link edu.epam.task6.controller.CommandEnum} but they also play very important role in the application </p>
 *
 * @author Vladislav Gryadovkin
 */
public interface UnderCommand {
    /**
     * <p>This method is used to return url of default page
     * that is bound to corresponding undercommand</p>
     *
     * @return url of default page
     */
    String getDefaultPage();
}
