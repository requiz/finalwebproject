package edu.epam.task6.command;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>This is the main interface in application. It is used for processing
 * corresponding command from client request</p>
 * @author Vladislav Gryadovkin
 */
public interface Command {
    /**
     *<p>This method is used to process http request</p>
     * @param request is http request that is come from client
     * @return corresponding url of page for redirecting or forwarding
     */
    String execute(HttpServletRequest request);

}
