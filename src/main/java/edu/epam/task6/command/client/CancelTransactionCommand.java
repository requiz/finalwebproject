package edu.epam.task6.command.client;

import edu.epam.task6.command.Command;
import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.controller.CommandEnum;
import edu.epam.task6.controller.CommandFactory;
import edu.epam.task6.entity.impl.Transaction;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import edu.epam.task6.service.CardService;
import edu.epam.task6.service.TransactionService;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.FieldsValidator;
import edu.epam.task6.validation.TransactionValidator;
import edu.epam.task6.validation.exception.ValidationException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>This class is used for returning money back if time of permits doesn't pass</p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class CancelTransactionCommand implements Command {
    public static final String ID = "id";
    public static final String MSG = "transaction.success";
    private Logger logger = Logger.getLogger(CancelTransactionCommand.class.getName());
    private Lock lock = new ReentrantLock();

    @Override
    public String execute(HttpServletRequest request) {
        MessageHandler handler = MessageHandler.getInstance();
        String id = request.getParameter(ID);
        try {
            Integer validId = FieldsValidator.validateId(id);
            TransactionService service = TransactionService.getInstance();
            CardService cardService = CardService.getInstance();
            Transaction transaction = service.getSelectedTransaction(validId);
            TransactionValidator.checkTimeForCancel(transaction);
            lock.lock();
            try {
                double cardFrom = cardService.getMoneyBack(transaction);
                double cardTo = cardService.validateCardTo(transaction);
                service.cancelTransaction(transaction, cardFrom, cardTo);
                handler.writeSuccess(request, MSG);
            } finally {
                lock.unlock();
            }
        } catch (ValidationException | ServiceException e) {
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
            handler.writeError(request, e);
        }
        return CommandFactory.getCommand(CommandEnum.VIEWTOUSER).execute(request);

    }
}
