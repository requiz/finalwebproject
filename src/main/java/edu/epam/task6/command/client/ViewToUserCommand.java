package edu.epam.task6.command.client;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.Command;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.entity.impl.Operation;
import edu.epam.task6.entity.impl.Transaction;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import edu.epam.task6.service.TransactionService;
import edu.epam.task6.validation.TransactionValidator;
import edu.epam.task6.helper.ViewHelper;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>This class is used to passing information about user transactions</p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class ViewToUserCommand implements Command {
    public static final String PAGE_VIEW_TRANSACTIONS = "page.viewTransactions";
    public static final String POSITION = "position";
    public static final String TRANSACTIONS = "transactions";
    public static final String ACCOUNT = "account";
    public static final String SORT = "sort";
    public static final String ARCHIVED = "archived";
    private Logger logger = Logger.getLogger(ViewToUserCommand.class.getName());

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigManager.getProperty(PAGE_VIEW_TRANSACTIONS);
        MessageHandler handler = MessageHandler.getInstance();
        HashMap<Transaction, Long> positions;
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute(ACCOUNT);
        TransactionService service = TransactionService.getInstance();
        Map<Transaction, Operation> transactions;
        Map<Integer, String> archivedOperations;
        try {
            transactions = service.getToClient(account);
            positions = TransactionValidator.validateForCancel(transactions);
            archivedOperations = TransactionValidator.validateArchiveOper(transactions);
            request.setAttribute(ARCHIVED, archivedOperations);
            String parameterSort = request.getParameter(SORT);
            if (parameterSort != null) {
                transactions = ViewHelper.sortTransaction(parameterSort, transactions);
                request.setAttribute(SORT, parameterSort);
            }
            request.setAttribute(TRANSACTIONS, transactions);
            request.setAttribute(POSITION, positions);
        } catch (ServiceException e) {
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
            handler.writeError(request, e);
        }
        return page;
    }

}
