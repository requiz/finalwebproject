package edu.epam.task6.command.client;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.Command;
import edu.epam.task6.controller.CommandEnum;
import edu.epam.task6.controller.CommandFactory;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.entity.impl.Card;
import edu.epam.task6.entity.impl.Operation;
import edu.epam.task6.entity.impl.Transaction;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import edu.epam.task6.service.CardService;
import edu.epam.task6.service.OperationService;
import edu.epam.task6.service.TransactionService;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.TransactionValidator;
import edu.epam.task6.validation.exception.ValidationException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>This class is used to pay money foe services that exists in the system</p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class PayMoneyCommand implements Command {
    public static final String CARDS = "cards";
    public static final String OPERATIONS = "operations";
    public static final String OPERATION = "operation";
    public static final String SUM = "sum";
    public static final String ACCOUNT = "account";
    public static final String ENTER = "enter";
    public static final String LIST = "list";
    public static final String MSG = "message.payment";
    private Logger logger = Logger.getLogger(PayMoneyCommand.class.getName());
    private Lock lock = new ReentrantLock();
    public static final String PAY = "page.pay";

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigManager.getProperty(PAY);
        String button = request.getParameter(ENTER);
        CardService service = CardService.getInstance();
        MessageHandler handler = MessageHandler.getInstance();
        OperationService operationService = OperationService.getInstance();
        try {
            HttpSession session = request.getSession(true);
            Account account = (Account) session.getAttribute(ACCOUNT);
            List<Card> cards = service.getWorkingCards(account);
            List<Operation> operations = operationService.getActiveOperations();
            request.setAttribute(CARDS, cards);
            request.setAttribute(OPERATIONS, operations);
            if (button != null) {
                page = payHandler(request, service, operationService);
                handler.writeSuccess(request, MSG);
            }
        } catch (ValidationException | ServiceException e) {
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
            handler.writeError(request, e);
            page = ConfigManager.getProperty(PAY);
        }
        return page;
    }

    /**
     * <p>This method is used for processing form when button was submitted </p>
     *
     * @param request          is http request from the client
     * @param service          is service that contains card operations
     * @param operationService is service that contains all operations
     * @return url of corresponding page
     * @throws ValidationException is exception of validating fields
     * @throws ServiceException    is exception that occurred in the corresponding service
     */
    private String payHandler(HttpServletRequest request, CardService service, OperationService operationService) throws ValidationException, ServiceException {
        String page;
        String card = request.getParameter(CARDS);
        String operation = request.getParameter(OPERATION);
        String sum = request.getParameter(SUM);
        Operation selectOperation = operationService.getOperByName(operation);
        Transaction transaction = TransactionValidator.validateTransaction(card, selectOperation, sum);
        TransactionService transactionService = TransactionService.getInstance();
        lock.lock();
        try {
            double decreaseSum = service.withdrawMoney(transaction);
            transactionService.insertTransferPayment(transaction, decreaseSum);
        } finally {
            lock.unlock();
        }
        page = CommandFactory.getCommand(CommandEnum.VIEWTOUSER).execute(request);
        return page;
    }
}
