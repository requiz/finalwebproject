package edu.epam.task6.command.client.card;

import edu.epam.task6.command.Command;
import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.controller.CommandEnum;
import edu.epam.task6.controller.CommandFactory;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import edu.epam.task6.service.CardService;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.CardValidator;
import edu.epam.task6.validation.exception.ValidationException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>This class is used to drop card from the system by the client</p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class DropCardCommand implements Command {
    public static final String NUMBER = "number";
    private Logger logger = Logger.getLogger(DropCardCommand.class.getName());
    public static final String CARD_DROP = "card.drop";

    @Override
    public String execute(HttpServletRequest request) {
        MessageHandler handler = MessageHandler.getInstance();
        String page = CommandFactory.getCommand(CommandEnum.MAIN).execute(request);
        String number = request.getParameter(NUMBER);
        try {
            Long parsedNumber = CardValidator.validateNumber(number);
            CardService service = CardService.getInstance();
            service.validateBeforeDel(parsedNumber);
            handler.writeSuccess(request, CARD_DROP);
        } catch (ValidationException | ServiceException e) {
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
            handler.writeError(request, e);
        }
        return page;
    }
}
