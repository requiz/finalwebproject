package edu.epam.task6.command.client.card;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.Command;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.entity.impl.Card;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import edu.epam.task6.service.CardService;
import edu.epam.task6.helper.ViewHelper;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>This class is used to passing information about
 * user cards,it's also acts as a main page</p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class ViewCardsCommand implements Command {

    public static final String PAGE_VIEW = "page.view";
    public static final String ACCOUNT = "account";
    public static final String CARDS = "cards";
    public static final String NOTCHANGED = "notchanged";
    private Logger logger = Logger.getLogger(ViewCardsCommand.class.getName());

    @Override
    public String execute(HttpServletRequest request) {
        MessageHandler handler = MessageHandler.getInstance();
        HttpSession session = request.getSession(true);
        String page = ConfigManager.getProperty(PAGE_VIEW);
        CardService service = CardService.getInstance();
        List<Card> cards;
        try {
            cards = service.getAll((Account) session.getAttribute(ACCOUNT));
            request.setAttribute(CARDS, ViewHelper.sortCards(cards));
            request.setAttribute(NOTCHANGED, ViewHelper.setNotChangedStatus());
        } catch (ServiceException e) {
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
            handler.writeError(request, e);
        }
        return page;
    }
}
