package edu.epam.task6.command.client.card;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.Command;
import edu.epam.task6.controller.CommandEnum;
import edu.epam.task6.controller.CommandFactory;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.entity.impl.Card;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.exception.ValidationException;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import edu.epam.task6.service.CardService;
import edu.epam.task6.service.CardTypeService;
import edu.epam.task6.service.CurrencyService;
import edu.epam.task6.validation.CardValidator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>This class is used to adding card to the system by the client</p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class AddCardCommand implements Command {
    public static final String PAGE_NEWCARD = "page.newcard";
    public static final String TYPES = "types";
    public static final String TYPE = "type";
    public static final String ENTER = "addcard";
    public static final String CURRENCY = "currency";
    public static final String BALANCE = "balance";
    public static final String ACCOUNT = "account";
    public static final String MSG = "message.cardSuccess";

    private Logger logger = Logger.getLogger(AddCardCommand.class.getName());

    @Override
    public String execute(HttpServletRequest request) {
        CardTypeService service = CardTypeService.getInstance();
        MessageHandler handler = MessageHandler.getInstance();
        CurrencyService currencyService = CurrencyService.getInstance();
        String page = ConfigManager.getProperty(PAGE_NEWCARD);
        String button = request.getParameter(ENTER);
        try {
            request.setAttribute(CURRENCY, currencyService.getAllCurrencies());
            request.setAttribute(TYPES, service.getAllTypes());
            if (button != null) {
                HttpSession session = request.getSession(true);
                String type = request.getParameter(TYPE);
                String currency = request.getParameter(CURRENCY);
                String balance = request.getParameter(BALANCE);
                Account account = (Account) session.getAttribute(ACCOUNT);
                Card card = CardValidator.validateNewCard(type, balance, currency, account);
                currencyService.validateCurrency(card);
                CardService cardService = CardService.getInstance();
                cardService.insert(card);
                page = CommandFactory.getCommand(CommandEnum.MAIN).execute(request);
                handler.writeSuccess(request, MSG);
            }
        } catch (ValidationException | ServiceException e) {
            handler.writeError(request, e);
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
        }
        return page;
    }
}
