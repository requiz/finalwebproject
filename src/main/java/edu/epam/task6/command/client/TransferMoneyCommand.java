package edu.epam.task6.command.client;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.Command;
import edu.epam.task6.controller.CommandEnum;
import edu.epam.task6.controller.CommandFactory;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.entity.impl.Card;
import edu.epam.task6.entity.impl.Transaction;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.exception.ValidationException;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import edu.epam.task6.service.CardService;
import edu.epam.task6.service.TransactionService;
import edu.epam.task6.validation.TransactionValidator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>This class is used to transfer money between different cards</p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class TransferMoneyCommand implements Command {

    public static final String PAGE_TRANSFER = "page.transfer";
    public static final String ACCOUNT = "account";
    public static final String ENTER = "enter";
    public static final String OWN_CARDS = "ownCards";
    public static final String ALLEN_CARDS = "allenCards";
    public static final String OWN = "own";
    public static final String ALLEN = "allen";
    public static final String SUM = "sum";
    public static final String MSG = "message.transfer";
    private Logger logger = Logger.getLogger(TransferMoneyCommand.class.getName());
    private Lock lock = new ReentrantLock();

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigManager.getProperty(PAGE_TRANSFER);
        String button = request.getParameter(ENTER);
        MessageHandler handler = MessageHandler.getInstance();
        CardService service = CardService.getInstance();
        try {
            HttpSession session = request.getSession(true);
            Account account = (Account) session.getAttribute(ACCOUNT);
            List<Card> ownCards = service.getWorkingCards(account);
            List<Card> allenCards = service.getAllenCards(account.getId());
            request.setAttribute(OWN_CARDS, ownCards);
            request.setAttribute(ALLEN_CARDS, allenCards);
            if (button != null) {
                page = transferHandler(request, service);
                handler.writeSuccess(request, MSG);
            }
        } catch (ValidationException | ServiceException e) {
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
            handler.writeError(request, e);
            page = ConfigManager.getProperty(PAGE_TRANSFER);
        }
        return page;
    }

    /**
     * <p>This method is used for processing form when button was submitted </p>
     *
     * @param request is http request from the client
     * @param service is service that contains card operations
     * @return url of corresponding page
     * @throws ValidationException is exception of validating fields
     * @throws ServiceException    is exception that occurred in the corresponding service
     */
    private String transferHandler(HttpServletRequest request, CardService service) throws ServiceException, ValidationException {
        String page;
        String ownCard = request.getParameter(OWN);
        String allenCard = request.getParameter(ALLEN);
        String sum = request.getParameter(SUM);
        Transaction transaction = TransactionValidator.validateTransfer(ownCard, allenCard, sum);
        TransactionService transactionService = TransactionService.getInstance();
        lock.lock();
        try {
            double sumFromCard = service.withdrawMoney(transaction);
            double sumToCard = service.putMoney(transaction);
            transactionService.insertTransferToCard(transaction, sumFromCard, sumToCard);
        } finally {
            lock.unlock();
        }
        page = CommandFactory.getCommand(CommandEnum.VIEWTOUSER).execute(request);
        return page;
    }
}
