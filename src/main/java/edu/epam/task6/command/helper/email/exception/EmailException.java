package edu.epam.task6.command.helper.email.exception;

public class EmailException extends RuntimeException {
    public EmailException() {
        super();
    }

    public EmailException(String message) {
        super(message);
    }

    public EmailException(Throwable throwable) {
        super(throwable);
    }

    public EmailException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
