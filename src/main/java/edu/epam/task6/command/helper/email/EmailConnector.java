package edu.epam.task6.command.helper.email;

import edu.epam.task6.command.helper.email.exception.EmailException;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.helper.EncryptDecrypt;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class EmailConnector {
    public static final String LOGIN = "login";
    public static final String EMAIL_PROPERTIES = "email.properties";
    public static final String BODY = "body";
    public static final String SUBJECT = "subject";
    public static final String TRUE = "true";
    public static final String SMTP = "smtp";
    public static final String PORT = "port";
    public static final String MAIL_PORT = "mail.port";
    public static final String AUTH = "auth";
    public static final String STARTTLS = "starttls";
    public static final String GMAIL = "gmail";
    Properties emailProperties;
    Session mailSession;
    MimeMessage emailMessage;
    private static ResourceBundle bundle = ResourceBundle.getBundle("properties.email");
    private Logger logger = Logger.getLogger(EmailConnector.class.getName());
    private Lock lock = new ReentrantLock();

    private final static EmailConnector connector = new EmailConnector();

    private EmailConnector() {

    }

    public static EmailConnector getInstance() {
        return connector;
    }

    public boolean sendNewPassword(Account account) {
        boolean result = false;
        lock.lock();
        try {
            setMailServerProperties();
            createEmailMessage(account.getEmail(), account.getPassword());
            sendEmail();
            result = true;
        } catch (MessagingException e) {
            logger.error(e.getMessage());
            throw new EmailException(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    private void setMailServerProperties() {
        String emailPort = bundle.getString(PORT);
        emailProperties = System.getProperties();
        emailProperties.put(bundle.getString(MAIL_PORT), emailPort);
        emailProperties.put(bundle.getString(AUTH), TRUE);
        emailProperties.put(bundle.getString(STARTTLS), TRUE);

    }

    private void createEmailMessage(String email, String password) throws MessagingException {
        String fromUser = bundle.getString(LOGIN);
        String emailSubject = bundle.getString(SUBJECT);
        String emailBody = bundle.getString(BODY) + password;
        mailSession = Session.getDefaultInstance(emailProperties, null);
        emailMessage = new MimeMessage(mailSession);
        emailMessage.setFrom(new InternetAddress(fromUser));
        emailMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
        emailMessage.setSubject(emailSubject);
        emailMessage.setContent(emailBody, "text/html");

    }

    private void sendEmail() throws MessagingException {
        String fromUser = bundle.getString(LOGIN);
        String emailHost = bundle.getString(GMAIL);
        EncryptDecrypt encryptDecrypt = new EncryptDecrypt(EMAIL_PROPERTIES);
        try {
            encryptDecrypt.encryptPropertyValue();
            String password = encryptDecrypt.decryptPropertyValue();
            Transport transport = mailSession.getTransport(SMTP);
            transport.connect(emailHost, fromUser, password);
            transport.sendMessage(emailMessage, emailMessage.getAllRecipients());
            transport.close();
        } catch (ConfigurationException e) {
            logger.error(e);
        }
    }
}






