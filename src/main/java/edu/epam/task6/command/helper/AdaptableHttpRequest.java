package edu.epam.task6.command.helper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * <p>This class is used as a wrapper of http requests</p>
 *
 * @author Vladislav Gryadovkin
 */
public class AdaptableHttpRequest extends HttpServletRequestWrapper {
    private static ConcurrentMap<String, String> params = new ConcurrentHashMap<>();

    public AdaptableHttpRequest(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String getParameter(String name) {
        HttpServletRequest req = (HttpServletRequest) super.getRequest();
        String requestParameter = req.getParameter(name);
        if (requestParameter != null && !requestParameter.isEmpty()) {
            return requestParameter;
        }
        return params.get(name);

    }

    /**
     * <p>This method is used for checking value from map before from request</p>
     *
     * @param name - is parameter from request
     * @return - string value of parameter
     */
    public String getParameterFromOneRequest(String name) {
        HttpServletRequest req = (HttpServletRequest) super.getRequest();
        String key = params.get(name);
        if (key != null && !key.isEmpty()) {
            return params.get(name);
        } else
            return req.getParameter(name);
    }

    /**
     * <p>This method is adding parameter to hashmap</p>
     *
     * @param name  - key to hashmap
     * @param value - parameter for value in hashmap
     */
    public void addParameter(String name, String value) {
        params.put(name, value);
    }

    /**
     * <p> This method is used for removing parameter from hashmap
     * </p>
     *
     * @param key - parameter by which removing is occured
     */
    public void removeParameter(String key) {
        boolean result = params.containsKey(key);
        if (result) {
            params.remove(key);
        }
    }
}
