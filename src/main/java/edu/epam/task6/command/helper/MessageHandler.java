package edu.epam.task6.command.helper;

import edu.epam.task6.manager.MessageManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>This class is used to setting error or success message to the client</p>
 *
 * @author Vladislav Gryadovkin
 */
public class MessageHandler {
    public static final String LANGUAGE = "language";
    public static final String ERROR_MESSAGE = "errorMessage";
    public static final String SUCCESS = "success";
    private static MessageHandler handler = new MessageHandler();

    private MessageHandler() {

    }

    public static MessageHandler getInstance() {
        return handler;
    }

    public void writeError(HttpServletRequest request, Throwable e) {
        HttpSession session = request.getSession();
        String language = (String) session.getAttribute(LANGUAGE);
        request.setAttribute(ERROR_MESSAGE, MessageManager.getProperty(e.getMessage(), language));
    }

    public void writeSuccess(HttpServletRequest request, String msg) {
        HttpSession session = request.getSession();
        String language = (String) session.getAttribute(LANGUAGE);
        request.setAttribute(SUCCESS, MessageManager.getProperty(msg, language));
    }
}
