package edu.epam.task6.command.helper;

import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.entity.impl.Operation;
import edu.epam.task6.entity.impl.status.AccountStatus;
import edu.epam.task6.validation.FieldsValidator;
import edu.epam.task6.validation.OperationValidator;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>This class is used to process parameters that comes from request</p>
 *
 * @author Vladislav Gryadovkin
 */
public class RequestHelper {
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
    public static final String SEX = "sex";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String NUMBER = "number";
    public static final String ROLE = "role";
    public static final String CONTROLLER = "/controller";
    public static final String PAGE_LOGIN = "main";
    public static final String REFERER = "Referer";
    private final String pattern = "command";
    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    private static final RequestHelper helper = new RequestHelper();

    private RequestHelper() {

    }

    public static RequestHelper getInstance() {
        return helper;
    }

    public Account getAccountFromRequest(HttpServletRequest request) {
        String login = request.getParameter(LOGIN);
        String email = request.getParameter(EMAIL);
        String sex = request.getParameter(SEX);
        String password = request.getParameter(PASSWORD);
        String firstName = request.getParameter(FIRST_NAME);
        String lastName = request.getParameter(LAST_NAME);
        String contactNumber = request.getParameter(NUMBER);
        String role = request.getParameter(ROLE);

        Account account = new Account(login, password);
        account.setContactNumber(contactNumber);
        account.setEmail(email);
        account.setSex(sex);
        account.setFirstName(firstName);
        account.setLastName(lastName);
        if (role == null) {
            account.setStatus(AccountStatus.USER);
        } else account.setStatus(AccountStatus.ADMIN);
        return account;
    }

    public Operation getOperationFromRequest(HttpServletRequest request) {
        String name = request.getParameter(TITLE);
        String id = request.getParameter(ID);
        String description = request.getParameter(DESCRIPTION);
        int validId = OperationValidator.validateId(id);
        Operation operation;
        if (validId != 0) {
            operation = new Operation(validId, name, description);
        } else operation = new Operation(name, description);
        return operation;
    }

    public String validateUri(HttpServletRequest request) {
        Boolean result = false;
        String requestCommand = null;
        StringBuilder searchLine = new StringBuilder();
        StringBuilder command;
        String uri = request.getHeader(REFERER);
        if (uri != null) {
            result = uri.contains(pattern);
            if (uri.endsWith(CONTROLLER)) {
                requestCommand = PAGE_LOGIN;
            }
        }
        if (result) {
            searchLine.append(uri.substring(uri.indexOf(pattern) + pattern.length() + 1));
            command = FieldsValidator.validateUri(searchLine);
            requestCommand = command.toString();
            int position = searchLine.indexOf(requestCommand) + requestCommand.length();
            if (position != searchLine.length()) {
                String parsedString = searchLine.substring(position + 1);
                setParamToRequest(parsedString, request);
            }
        }
        return requestCommand;
    }

    public void setParamToRequest(String string, HttpServletRequest request) {
        AdaptableHttpRequest adaptableHttpRequest = new AdaptableHttpRequest(request);
        List<String> list = new ArrayList<>();
        String[] s = string.split("=|&");
        Collections.addAll(list, s);
        for (int i = 0; i < list.size() - 1; ) {
            adaptableHttpRequest.addParameter(list.get(i), list.get(i + 1));
            i += 2;
        }
    }
}
