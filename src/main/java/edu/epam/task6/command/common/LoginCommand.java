package edu.epam.task6.command.common;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.Command;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.exception.ValidationException;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import edu.epam.task6.service.AccountService;
import edu.epam.task6.validation.AccountValidator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>This class is used to perform task of authorization</p>
 * @see Command
 * @author Vladislav Gryadovkin
 */
public class LoginCommand implements Command {
    public static final String PAGE_LOGIN = "page.login";
    public static final String ACCOUNT = "account";
    private Logger logger = Logger.getLogger(LoginCommand.class.getName());
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";

    @Override
    public String execute(HttpServletRequest request) {
        MessageHandler handler = MessageHandler.getInstance();
        AccountService service = AccountService.getInstance();
        String page = ConfigManager.getProperty(PAGE_LOGIN);
        HttpSession session = request.getSession();
        String login = request.getParameter(LOGIN);
        String password = request.getParameter(PASSWORD);
        try {
            Account account = new Account(login, password);
            AccountValidator.validateAccount(account);
            Account dbAccount = service.retrieveAccount(account);
            if (dbAccount != null) {
                session.setAttribute(ACCOUNT, dbAccount);
            }
        } catch (ValidationException | ServiceException e) {
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
            handler.writeError(request, e);
        }
        return page;
    }
}
