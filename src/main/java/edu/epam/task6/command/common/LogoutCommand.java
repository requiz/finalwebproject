package edu.epam.task6.command.common;

import edu.epam.task6.command.Command;
import edu.epam.task6.manager.ConfigManager;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>This class is used to kill all session parameters when user leaves application</p>
 * @see Command
 * @author Vladislav Gryadovkin
 */
public class LogoutCommand implements Command {

    public static final String PAGE_LOGIN = "page.login";

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().invalidate();
        return ConfigManager.getProperty(PAGE_LOGIN);
    }
}
