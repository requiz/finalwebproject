package edu.epam.task6.command.common.profile.impl;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.command.UnderCommand;
import edu.epam.task6.command.common.profile.SubCommand;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.exception.ValidationException;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.service.AccountService;
import edu.epam.task6.validation.AccountValidator;

/**
 * <p>This class is used to change personal details of account</p>
 *
 * @author Vladislav Gryadovkin
 * @see UnderCommand ,edu.epam.task6.command.common.profile.SubCommand
 */
public class EditInfoCommand implements SubCommand, UnderCommand {
    public static final String PAGE_CHANGE = "page.change";
    public static final String LOGIN = "login";
    public static final String EMAIL = "email";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String NUMBER = "number";
    private static final EditInfoCommand editInfoCommand = new EditInfoCommand();
    public static final String CHANGE = "change";
    public static final String MSG = "change.details";
    public static final String PAGE_LOGIN = "page.login";

    private EditInfoCommand() {

    }

    public static EditInfoCommand getEditInfoCommand() {
        return editInfoCommand;
    }

    public String execute(AdaptableHttpRequest adaptableHttpRequest, Account account) throws ValidationException, ServiceException {
        String page = getDefaultPage();
        String button = adaptableHttpRequest.getParameter(CHANGE);
        MessageHandler handler = MessageHandler.getInstance();
        if (button != null) {
            AccountService service = AccountService.getInstance();
            account.setFirstName(adaptableHttpRequest.getParameter(FIRST_NAME));
            account.setLastName(adaptableHttpRequest.getParameter(LAST_NAME));
            account.setContactNumber(adaptableHttpRequest.getParameter(NUMBER));
            account.setLogin(adaptableHttpRequest.getParameter(LOGIN));
            account.setEmail(adaptableHttpRequest.getParameter(EMAIL));
            AccountValidator.validatePersonalInfo(account);
            if (service.updateInfoAccount(account)) {
                handler.writeSuccess(adaptableHttpRequest, MSG);
                page = ConfigManager.getProperty(PAGE_LOGIN);
            }
        }
        return page;
    }

    @Override
    public String getDefaultPage() {
        return ConfigManager.getProperty(PAGE_CHANGE);
    }
}

