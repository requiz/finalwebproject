package edu.epam.task6.command.common.profile;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.command.Command;
import edu.epam.task6.command.common.profile.factory.ProfileFactory;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.exception.ValidationException;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>This class is used to determine corresponding undercommand by parameter
 * and send processing to that command
 * </p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class EditProfileCommand implements Command {
    public static final String NAME = "name";
    public static final String ACCOUNT = "account";
    private ProfileFactory profileFactory;
    private Logger logger = Logger.getLogger(EditProfileCommand.class.getName());

    public EditProfileCommand() {
        profileFactory = new ProfileFactory();
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        AdaptableHttpRequest adaptableHttpRequest = new AdaptableHttpRequest(request);
        String parameter = adaptableHttpRequest.getParameter(NAME);
        MessageHandler handler = MessageHandler.getInstance();
        SubCommand underCommand = null;
        HttpSession session = request.getSession(true);
        Account account = (Account) session.getAttribute(ACCOUNT);
        try {
            adaptableHttpRequest.removeParameter(NAME);
            underCommand = profileFactory.getUnderCommand(parameter);
            page = underCommand.execute(adaptableHttpRequest, account);
        } catch (ValidationException | ServiceException e) {
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
            handler.writeError(request, e);
            page = underCommand.getDefaultPage();

        }
        return page;
    }
}
