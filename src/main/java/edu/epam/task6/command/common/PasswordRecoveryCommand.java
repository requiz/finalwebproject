package edu.epam.task6.command.common;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.Command;
import edu.epam.task6.command.helper.email.EmailConnector;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.exception.ValidationException;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import edu.epam.task6.service.AccountService;
import edu.epam.task6.validation.AccountValidator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>This class is used for changing  password by email
 * </p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class PasswordRecoveryCommand implements Command {

    public static final String PAGE_RECOVERY = "page.recovery";
    public static final String BUTTON = "button";
    public static final String LOGIN = "login";
    public static final String EMAIL = "email";
    public static final String PAGE_LOGIN = "page.login";
    public static final String MSG = "message.recover";
    private Logger logger = Logger.getLogger(PasswordRecoveryCommand.class.getName());

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigManager.getProperty(PAGE_RECOVERY);
        String button = request.getParameter(BUTTON);
        MessageHandler handler = MessageHandler.getInstance();
        if (button != null) {
            try {
                String login = request.getParameter(LOGIN);
                String email = request.getParameter(EMAIL);
                Account account = new Account();
                account.setLogin(login);
                account.setEmail(email);
                AccountValidator.validatePasswordRecovery(account);
                AccountService service = AccountService.getInstance();
                EmailConnector connector = EmailConnector.getInstance();
                if (service.validateDataForRecovery(account)) {
                    String password = AccountValidator.validateNewPassword();
                    service.updatePassword(account, password);
                    account.setPassword(password);
                    connector.sendNewPassword(account);
                }
                handler.writeSuccess(request, MSG);
                page = ConfigManager.getProperty(PAGE_LOGIN);
            } catch (ValidationException | ServiceException e) {
                logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
                handler.writeError(request, e);
            }
        }
        return page;
    }
}
