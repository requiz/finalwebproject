package edu.epam.task6.command.common;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.Command;
import edu.epam.task6.command.helper.RequestHelper;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.exception.ValidationException;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import edu.epam.task6.service.AccountService;
import edu.epam.task6.validation.AccountValidator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
/**
 * <p>This class is used to register a new user to the system</p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class RegistrationCommand implements Command {

    public static final String REGISTRATION = "page.registration";
    public static final String PAGE_LOGIN = "page.login";
    public static final String BUTTON_VALUE = "REGISTRATION";
    public static final String REP_PASS = "repPass";
    public static final String ACCOUNT = "account";
    public static final String MSG = "registration.success";
    private Logger logger = Logger.getLogger(RegistrationCommand.class.getName());

    @Override
    public String execute(HttpServletRequest request) {
        MessageHandler handler = MessageHandler.getInstance();
        AccountService service = AccountService.getInstance();
        RequestHelper helper = RequestHelper.getInstance();
        String page = ConfigManager.getProperty(REGISTRATION);
        String button = request.getParameter(BUTTON_VALUE);
        if (button != null) {
            Account account;
            account = helper.getAccountFromRequest(request);
            String confirmPassword = request.getParameter(REP_PASS);
            try {
                AccountValidator.validateNewAccount(account, confirmPassword);
                service.addAccount(account);
                page = ConfigManager.getProperty(PAGE_LOGIN);
                handler.writeSuccess(request, MSG);
            } catch (ValidationException | ServiceException e) {
                logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
                handler.writeError(request, e);
                request.setAttribute(ACCOUNT, account);
            }
        }
        return page;
    }
}
