package edu.epam.task6.command.common.profile.impl;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.command.UnderCommand;
import edu.epam.task6.command.common.profile.SubCommand;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.exception.ValidationException;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.service.AccountService;
import edu.epam.task6.validation.AccountValidator;

import javax.servlet.http.HttpSession;

/**
 * <p>This class is used for changing password of the account</p>
 *
 * @author Vladislav Gryadovkin
 * @see UnderCommand ,edu.epam.task6.command.common.profile.SubCommand
 */
public class EditPasswordCommand implements SubCommand, UnderCommand {
    public static final String PASSWORD = "password";
    public static final String NEW_PASSWORD = "newPassword";
    public static final String CONFIRM_PASSWORD = "confirmPassword";
    public static final String CHANGE_PASSWORD = "page.changepass";
    public static final String PAGE_LOGIN = "page.login";
    private static final EditPasswordCommand passwordCommand = new EditPasswordCommand();
    public static final String CHANGE = "change";;
    public static final String ACCOUNT = "account";
    public static final String MSG = "change.password";

    private EditPasswordCommand() {

    }

    public static EditPasswordCommand getPasswordCommand() {
        return passwordCommand;
    }

    public String execute(AdaptableHttpRequest adaptableHttpRequest, Account account) throws ValidationException, ServiceException {
        String page = getDefaultPage();
        String button = adaptableHttpRequest.getParameter(CHANGE);
        HttpSession session = adaptableHttpRequest.getSession(true);
        MessageHandler handler = MessageHandler.getInstance();
        if (button != null) {
            AccountService service = AccountService.getInstance();
            String prevPassword = adaptableHttpRequest.getParameter(PASSWORD);
            String newPassword = adaptableHttpRequest.getParameter(NEW_PASSWORD);
            String confirmPassword = adaptableHttpRequest.getParameter(CONFIRM_PASSWORD);
            AccountValidator.validatePassword(account, prevPassword, newPassword, confirmPassword);
            session.setAttribute(ACCOUNT, service.updatePassword(account,newPassword));
            handler.writeSuccess(adaptableHttpRequest, MSG);
            page = ConfigManager.getProperty(PAGE_LOGIN);

        }
        return page;
    }

    @Override
    public String getDefaultPage() {
        return ConfigManager.getProperty(CHANGE_PASSWORD);
    }


}
