package edu.epam.task6.command.common.profile.factory;

import edu.epam.task6.command.common.profile.SubCommand;
import edu.epam.task6.command.common.profile.impl.EditInfoCommand;
import edu.epam.task6.command.common.profile.impl.EditPasswordCommand;

/**
 * <p>This class is acts as factory to define appropriate
 * change information SubCommand</p>
 *
 * @author Vladislav Gryadovkin
 */
public class ProfileFactory {

    public static final String PROFILE = "profile";
    public static final String PASSWORD = "password";

    public SubCommand getUnderCommand(String type) {
        if (type == null) {
            return null;
        }
        if (type.equalsIgnoreCase(PROFILE)) {
            return EditInfoCommand.getEditInfoCommand();
        } else if (type.equalsIgnoreCase(PASSWORD)) {
            return EditPasswordCommand.getPasswordCommand();
        }
        return null;
    }
}

