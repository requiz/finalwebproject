package edu.epam.task6.command.common;

import edu.epam.task6.command.Command;
import edu.epam.task6.manager.ConfigManager;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>This class is used to forwarding to them menu page of application</p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class MainCommand implements Command {
    public static final String PAGE_LOGIN = "page.login";

    @Override
    public String execute(HttpServletRequest request) {
        return ConfigManager.getProperty(PAGE_LOGIN);
    }
}
