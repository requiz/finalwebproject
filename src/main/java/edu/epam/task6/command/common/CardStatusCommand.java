package edu.epam.task6.command.common;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.Command;
import edu.epam.task6.controller.CommandEnum;
import edu.epam.task6.controller.CommandFactory;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.validation.exception.ValidationException;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import edu.epam.task6.service.CardService;
import edu.epam.task6.validation.CardValidator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>This class is used to change status of card that is exists in the system</p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class CardStatusCommand implements Command {
    public static final String NUMBER = "number";
    public static final String CARD_SUCCESS = "card.success";
    public static final String ACCOUNT = "account";
    public static final String COMMAND_TAG = "commandTag";
    public static final String STATUS = "status";
    private Logger logger = Logger.getLogger(CardStatusCommand.class.getName());

    @Override
    public String execute(HttpServletRequest request) {
        MessageHandler handler = MessageHandler.getInstance();
        HttpSession session = request.getSession(true);
        CardService service = CardService.getInstance();
        String page = null;
        Account account = (Account) session.getAttribute(ACCOUNT);
        String pageFrom = request.getParameter(COMMAND_TAG);
        String status = request.getParameter(STATUS);
        String number = request.getParameter(NUMBER);
        Long parsedNumber;
        try {
            parsedNumber = CardValidator.validateNumber(number);
            service.changeStatus(parsedNumber, account, status);
            handler.writeSuccess(request, CARD_SUCCESS);
            page = CommandFactory.getCommand(CommandEnum.valueOf(pageFrom.toUpperCase())).execute(request);
        } catch (ValidationException e) {
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
            handler.writeError(request, e);
        }
        return page;
    }
}
