package edu.epam.task6.command.common.profile;

import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.command.UnderCommand;
import edu.epam.task6.command.common.profile.factory.ProfileFactory;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.exception.ValidationException;

/**
 * <p>This interface is used to process classes
 * of {@link ProfileFactory}
 * </p>
 *
 * @author Vladislav Gryadovkin
 */
public interface SubCommand extends UnderCommand {
    /**
     * <p>This method is used to process wrapped http requests</p>
     * @param request is wrapped http request
     * @param account the information from session about user or admin
     * @return
     * @throws ValidationException if the error occurred during validating fields information
     * @throws ServiceException if the error ocurred in service layer
     */
    String execute(AdaptableHttpRequest request, Account account) throws ValidationException, ServiceException;

}
