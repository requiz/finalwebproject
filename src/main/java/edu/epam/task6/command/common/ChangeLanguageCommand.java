package edu.epam.task6.command.common;

import edu.epam.task6.command.helper.RequestHelper;
import edu.epam.task6.command.Command;
import edu.epam.task6.controller.CommandEnum;
import edu.epam.task6.controller.CommandFactory;
import edu.epam.task6.entity.impl.Account;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>This class is used for changing language of corresponding
 * page</p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class ChangeLanguageCommand implements Command {

    public static final String LANGUAGE = "language";
    public static final String COMMAND_TAG = "commandTag";
    public static final String CHANGABLE = "changable";
    public static final String TRUE = "t";
    public static final String ACCOUNT = "account";

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession(true);
        String language = request.getParameter(LANGUAGE);
        session.setAttribute(LANGUAGE, language);
        Account account = (Account) session.getAttribute(ACCOUNT);
        RequestHelper helper = RequestHelper.getInstance();
        String pageFrom = request.getParameter(COMMAND_TAG);
        String prevCommand = helper.validateUri(request);
        request.setAttribute(CHANGABLE, TRUE);
        if (account == null) {
            page = pageFrom;
        } else {
            page = CommandFactory.getCommand(CommandEnum.valueOf(prevCommand.toUpperCase()))
                    .execute(request);
        }
        return page;
    }
}
