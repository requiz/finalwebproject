package edu.epam.task6.command.admin.insert.impl.currency;

import edu.epam.task6.command.admin.insert.SubCommand;
import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.command.UnderCommand;
import edu.epam.task6.entity.impl.Currency;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.exception.ValidationException;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.service.CurrencyService;

import java.util.List;

/**
 * <p>This class is used for viewing all currencies that exists in the system
 * </p>
 *
 * @author Vladislav Gryadovkin
 * @see UnderCommand ,SubCommand
 */
public class ViewCurrencyCommand implements SubCommand {
    public static final String PAGE_CURRENCIES = "page.currencies";
    public static final String CURRENCIES = "currencies";
    private static ViewCurrencyCommand currencyCommand = new ViewCurrencyCommand();

    public static ViewCurrencyCommand getInstance() {
        return currencyCommand;
    }

    @Override
    public String execute(AdaptableHttpRequest adaptableHttpRequest) throws ValidationException, ServiceException {
        String page = getDefaultPage();
        CurrencyService service = CurrencyService.getInstance();
        List<Currency> currencies = service.getAllCurrencies();
        adaptableHttpRequest.setAttribute(CURRENCIES, currencies);
        return page;
    }

    @Override
    public String getDefaultPage() {
        return ConfigManager.getProperty(PAGE_CURRENCIES);
    }
}
