package edu.epam.task6.command.admin.insert.factory;

import edu.epam.task6.command.admin.insert.SubCommand;
import edu.epam.task6.command.admin.insert.impl.operation.AddOperationCommand;
import edu.epam.task6.command.admin.insert.impl.operation.ChangeStatusCommand;
import edu.epam.task6.command.admin.insert.impl.operation.UpdateCommand;
import edu.epam.task6.command.admin.insert.impl.operation.ViewOperationCommand;

/**
 * <p>This class is acts as factory to define appropriate
 * operation SubCommand</p>
 *
 * @author Vladislav Gryadovkin
 */
public class OperationFactory {

    public static final String NEW = "new";
    public static final String UPDATE = "update";
    public static final String CHANGE = "change";
    public static final String VIEW = "view";

    public SubCommand getUnderCommand(String type) {
        if (type == null) {
            return null;
        }
        if (type.equalsIgnoreCase(NEW)) {
            return AddOperationCommand.getAddOperationCommand();
        } else if (type.equalsIgnoreCase(
                UPDATE)) {
            return UpdateCommand.getUpdate();
        } else if (type.equalsIgnoreCase(
                CHANGE)) {
            return ChangeStatusCommand.getInstance();
        } else if (type.equalsIgnoreCase(VIEW)) {
            return ViewOperationCommand.getInstance();
        }
        return null;
    }
}
