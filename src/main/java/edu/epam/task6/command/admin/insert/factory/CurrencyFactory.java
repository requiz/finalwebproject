package edu.epam.task6.command.admin.insert.factory;


import edu.epam.task6.command.admin.insert.impl.currency.InsertCurrencyCommand;
import edu.epam.task6.command.admin.insert.impl.currency.UpdateCurrencyCommand;
import edu.epam.task6.command.admin.insert.impl.currency.ViewCurrencyCommand;
import edu.epam.task6.command.admin.insert.SubCommand;
/**
 * <p>This class is acts as factory to define appropriate
 * currency SubCommand</p>
 *
 * @author Vladislav Gryadovkin
 */
public class CurrencyFactory {

    public static final String INSERT = "insert";
    public static final String VIEW = "view";
    public static final String UPDATE = "update";

    public SubCommand getUnderCommand(String type) {
        if (type == null) {
            return null;
        } else if (type.equalsIgnoreCase(INSERT)) {
            return InsertCurrencyCommand.getInstance();
        } else if (type.equalsIgnoreCase(
                VIEW)) {
            return ViewCurrencyCommand.getInstance();
        } else if (type.equalsIgnoreCase(UPDATE)){
            return UpdateCurrencyCommand.getInstance();
        }
            return null;
    }
}
