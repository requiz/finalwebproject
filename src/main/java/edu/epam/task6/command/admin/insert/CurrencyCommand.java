package edu.epam.task6.command.admin.insert;

import edu.epam.task6.command.admin.insert.factory.CurrencyFactory;
import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.command.Command;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.exception.ValidationException;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
/**
 * <p>This class is used to determine corresponding subCommand by parameter
 * and send processing to that command
 * </p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class CurrencyCommand implements Command {

    public static final String NAME = "name";
    public static final String ACCOUNT = "account";
    private Logger logger = Logger.getLogger(CurrencyCommand.class.getName());
    private CurrencyFactory factory;

    public CurrencyCommand() {
        factory = new CurrencyFactory();
    }


    @Override
    public String execute(HttpServletRequest request) {
        String page;
        AdaptableHttpRequest adaptableHttpRequest = new AdaptableHttpRequest(request);
        String command = adaptableHttpRequest.getParameterFromOneRequest(NAME);
        SubCommand underCommand = null;
        MessageHandler handler = MessageHandler.getInstance();
        try {
            adaptableHttpRequest.removeParameter(NAME);
            underCommand= factory.getUnderCommand(command);
            page = underCommand.execute(adaptableHttpRequest);
        } catch (ValidationException | ServiceException e) {
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
            handler.writeError(request, e);
            page = underCommand.getDefaultPage();
        }
        return page;
    }
}
