package edu.epam.task6.command.admin.transaction;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.command.Command;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.entity.impl.Operation;
import edu.epam.task6.entity.impl.Transaction;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import edu.epam.task6.service.AccountService;
import edu.epam.task6.service.TransactionService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
/**
 * <p>This class is used for getting report about specific user transactions</p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class ViewToAdminCommand implements Command {
    public static final String TRANSACTIONS = "transactions";
    public static final String LOGIN = "login";
    public static final String PAGE_VIEW_TRANSACTIONS = "page.viewUserTransactions";
    public static final String ACCOUNT = "account";
    private Logger logger = Logger.getLogger(ViewToAdminCommand.class.getName());


    public String execute(HttpServletRequest request) {
        AdaptableHttpRequest adaptableHttpRequest = new AdaptableHttpRequest(request);
        MessageHandler handler = MessageHandler.getInstance();
        String page = ConfigManager.getProperty(PAGE_VIEW_TRANSACTIONS);
        String login = adaptableHttpRequest.getParameter(LOGIN);
        try {
            AccountService service = AccountService.getInstance();
            TransactionService transactionService = TransactionService.getInstance();
            Account selectAccount = service.getAccountByLogin(login);
            Map<Transaction, Operation> map = transactionService.getByAccount(selectAccount);
            request.setAttribute(TRANSACTIONS, map);
        } catch (ServiceException e) {
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
            handler.writeError(request, e);
        }
        return page;
    }


}
