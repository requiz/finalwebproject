package edu.epam.task6.command.admin.insert.impl.operation;


import edu.epam.task6.command.UnderCommand;
import edu.epam.task6.command.admin.insert.SubCommand;
import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.helper.RequestHelper;
import edu.epam.task6.controller.CommandEnum;
import edu.epam.task6.controller.CommandFactory;
import edu.epam.task6.entity.impl.Operation;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.service.OperationService;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.OperationValidator;
import edu.epam.task6.validation.exception.ValidationException;

/**
 * <p>This class is used for inserting new operation to the system
 * </p>
 *
 * @author Vladislav Gryadovkin
 * @see UnderCommand,SubCommand
 */
public class AddOperationCommand implements SubCommand {
    public static final String UPDATE = "update";
    public static final String NAME = "name";
    public static final String VIEW = "view";
    public static final String MSG = "operation.add";
    private static final String PAGE = "page.newoper";
    public static final String ENTER = "oper";
    private static final AddOperationCommand addOperationCommand = new AddOperationCommand();
    public static final String OPERATION = "operation";

    private AddOperationCommand() {

    }

    public static AddOperationCommand getAddOperationCommand() {
        return addOperationCommand;
    }

    @Override
    public String execute(AdaptableHttpRequest request) throws ServiceException, ValidationException {
        String page = getDefaultPage();
        MessageHandler handler = MessageHandler.getInstance();
        String button = request.getParameter(ENTER);
        if (button != null) {
            OperationService service = OperationService.getInstance();
            String parameter = request.getParameter(UPDATE);
            RequestHelper helper = RequestHelper.getInstance();
            Operation operation = helper.getOperationFromRequest(request);
            try {
                OperationValidator.validateNewOperation(operation);
                if (parameter == null) {
                    service.insert(operation);
                } else {
                    service.update(operation);
                }
                request.addParameter(NAME, VIEW);
                handler.writeSuccess(request, MSG);
                page = CommandFactory.getCommand(CommandEnum.OPER).execute(request);
            } catch (ValidationException | ServiceException e) {
                request.setAttribute(OPERATION, operation);
                throw e;
            }
        }
        return page;
    }

    @Override
    public String getDefaultPage() {
        return ConfigManager.getProperty(PAGE);
    }
}
