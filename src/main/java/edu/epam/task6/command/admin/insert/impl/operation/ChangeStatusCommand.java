package edu.epam.task6.command.admin.insert.impl.operation;

import edu.epam.task6.command.UnderCommand;
import edu.epam.task6.command.admin.insert.SubCommand;
import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.controller.CommandEnum;
import edu.epam.task6.controller.CommandFactory;
import edu.epam.task6.entity.impl.Operation;
import edu.epam.task6.service.OperationService;
import edu.epam.task6.validation.FieldsValidator;
import edu.epam.task6.validation.exception.ValidationException;

/**
 * <p>This class is used for adding operation to the archive
 * </p>
 *
 * @author Vladislav Gryadovkin
 * @see UnderCommand,SubCommand
 */
public class ChangeStatusCommand implements SubCommand {

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String VIEW = "view";
    public static final String MSG = "operation.archive";

    private ChangeStatusCommand() {

    }

    private static final ChangeStatusCommand changeStatus = new ChangeStatusCommand();

    public static ChangeStatusCommand getInstance() {
        return changeStatus;
    }

    @Override
    public String execute(AdaptableHttpRequest request) throws ValidationException {
        MessageHandler handler = MessageHandler.getInstance();
        String id = request.getParameter(ID);
        Integer validId = FieldsValidator.validateId(id);
        OperationService service = OperationService.getInstance();
        Operation operation = service.getOperById(validId);
        service.updateStatus(operation);
        request.addParameter(NAME, VIEW);
        String page = CommandFactory.getCommand(CommandEnum.OPER).execute(request);
        handler.writeSuccess(request, MSG);
        return page;
    }

    @Override
    public String getDefaultPage() {
        throw new UnsupportedOperationException();
    }
}
