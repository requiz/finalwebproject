package edu.epam.task6.command.admin.transaction;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.command.Command;
import edu.epam.task6.entity.impl.Operation;
import edu.epam.task6.entity.impl.Transaction;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import edu.epam.task6.service.TransactionService;
import edu.epam.task6.helper.ViewHelper;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
/**
 * <p>This class is used for getting report about executed transactions</p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class ViewAllCommand implements Command {
    public static final String PAGE_VIEW_TRANSACTIONS = "page.viewAllTransactions";
    public static final String PAGE_VIEWALL = "page.viewall";
    public static final String REPORT = "report";
    public static final String TRANSACTIONS = "transactions";
    public static final String ACCOUNT = "account";
    public static final String SORT = "sort";
    private Logger logger = Logger.getLogger(ViewAllCommand.class.getName());

    @Override
    public String execute(HttpServletRequest request) {
        MessageHandler handler = MessageHandler.getInstance();
        String page = ConfigManager.getProperty(PAGE_VIEWALL);
        AdaptableHttpRequest adaptableHttpRequest = new AdaptableHttpRequest(request);
        String button = adaptableHttpRequest.getParameter(REPORT);
        try {
            if (button != null) {
                TransactionService service = TransactionService.getInstance();
                adaptableHttpRequest.removeParameter(REPORT);
                Map<Transaction, Operation> transactions = service.
                        getAllTransactions(button);
                String parameterSort = request.getParameter(SORT);
                if (parameterSort != null) {
                    transactions = ViewHelper.sortTransaction(parameterSort, transactions);
                    request.setAttribute(SORT, parameterSort);
                }
                request.setAttribute(TRANSACTIONS, transactions);
                page = ConfigManager.getProperty(PAGE_VIEW_TRANSACTIONS);
                request.setAttribute(REPORT, button);
            }
        } catch (ServiceException e) {
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
            handler.writeError(request, e);
        }
        return page;
    }
}



