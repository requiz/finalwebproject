package edu.epam.task6.command.admin.insert;

import edu.epam.task6.command.admin.insert.factory.OperationFactory;
import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.command.Command;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.exception.ValidationException;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
/**
 * <p>This class is used to determine corresponding subCommand by parameter
 * and send processing to that command
 * </p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class OperationCommand implements Command {
    private OperationFactory operationFactory;
    private Logger logger = Logger.getLogger(OperationCommand.class.getName());

    public OperationCommand() {
        operationFactory = new OperationFactory();
    }

    private final String NAME = "name";

    @Override
    public String execute(HttpServletRequest request) {
        AdaptableHttpRequest adaptableHttpRequest = new AdaptableHttpRequest(request);
        MessageHandler handler = MessageHandler.getInstance();
        String page;
        SubCommand underCommand = null;
        String command = adaptableHttpRequest.getParameterFromOneRequest(NAME);
        try {
            adaptableHttpRequest.removeParameter(NAME);
            underCommand = operationFactory.getUnderCommand(command);
            page = underCommand.execute(adaptableHttpRequest);
        } catch (ValidationException | ServiceException e) {
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
            handler.writeError(request, e);
            page = underCommand.getDefaultPage();
        }
        return page;
    }
}
