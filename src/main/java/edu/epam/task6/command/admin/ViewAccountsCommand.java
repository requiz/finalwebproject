package edu.epam.task6.command.admin;

import edu.epam.task6.command.Command;
import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import edu.epam.task6.service.AccountService;
import edu.epam.task6.service.exception.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>This class is used for viewing all user accounts existing in the system</p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class ViewAccountsCommand implements Command {
    private static final String USER_VIEW = "page.users";
    public static final String ACCOUNTS = "accounts";
    public static final String NO_OF_PAGES = "noOfPages";
    public static final String CURRENT_PAGE = "currentPage";
    public static final String PAGE = "page";
    private Logger logger = Logger.getLogger(ViewAccountsCommand.class.getName());

    @Override
    public String execute(HttpServletRequest request) {
        AccountService service = AccountService.getInstance();
        MessageHandler handler = MessageHandler.getInstance();
        String page = ConfigManager.getProperty(USER_VIEW);
        List<Account> accounts;
        try {
            int number = 1;
            int recordsPerPage = 5;
            String paramterPage = request.getParameter(PAGE);
            if (paramterPage != null) {
                number = Integer.parseInt(paramterPage);
            }
            accounts = service.getUserAccounts((number - 1) * recordsPerPage, recordsPerPage);
            int noOfRecords = service.numberOfRecords();
            int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
            request.setAttribute(ACCOUNTS, accounts);
            request.setAttribute(NO_OF_PAGES, noOfPages);
            request.setAttribute(CURRENT_PAGE, number);
        } catch (ServiceException e) {
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
            handler.writeError(request, e);
        }
        return page;
    }
}
