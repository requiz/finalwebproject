package edu.epam.task6.command.admin.insert.impl.currency;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.admin.insert.SubCommand;
import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.command.UnderCommand;
import edu.epam.task6.controller.CommandEnum;
import edu.epam.task6.controller.CommandFactory;
import edu.epam.task6.entity.impl.Currency;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.exception.ValidationException;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.service.CurrencyService;
import edu.epam.task6.validation.CurrencyValidator;
/**
 * <p>This class is used for inserting new currency to the system
 * </p>
 *
 * @author Vladislav Gryadovkin
 * @see UnderCommand ,SubCommand
 */
public class InsertCurrencyCommand implements SubCommand {
    public static final String PAGE_CARD = "page.currency";
    public static final String ENTER = "currency";
    public static final String CURRENCY = "currency";
    public static final String COURSE = "course";
    public static final String MAX_SUM = "max_sum";
    public static final String UPDATE = "update";
    public static final String VIEW = "view";
    public static final String NAME = "name";
    public static final String CURRENCY_ADD = "currency.add";
    private static InsertCurrencyCommand currencyCommand = new InsertCurrencyCommand();

    private InsertCurrencyCommand() {

    }

    public static InsertCurrencyCommand getInstance() {
        return currencyCommand;
    }

    @Override
    public String execute(AdaptableHttpRequest request) throws ValidationException, ServiceException {
        String page = getDefaultPage();
        MessageHandler handler = MessageHandler.getInstance();
        String button = request.getParameter(ENTER);
        String parameter = request.getParameter(UPDATE);
        if (button != null) {
            CurrencyService service = CurrencyService.getInstance();
            String currency = request.getParameter(CURRENCY);
            String course = request.getParameter(COURSE);
            String max_sum = request.getParameter(MAX_SUM);
            request.addParameter(NAME, VIEW);
            Currency validCurrency = CurrencyValidator.validateCurrency(currency, course, max_sum);
            if (parameter == null) {
                service.insert(validCurrency);
            } else service.update(validCurrency);
            handler.writeSuccess(request, CURRENCY_ADD);
            page = CommandFactory.getCommand(CommandEnum.CURRENCY).execute(request);
        }
        return page;
    }

    @Override
    public String getDefaultPage() {
        return ConfigManager.getProperty(PAGE_CARD);
    }
}

