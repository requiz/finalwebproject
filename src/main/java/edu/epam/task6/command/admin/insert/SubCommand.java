package edu.epam.task6.command.admin.insert;

import edu.epam.task6.command.admin.insert.factory.OperationFactory;
import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.command.UnderCommand;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.exception.ValidationException;

/**
 * <p>This interface is used to process classes
 * of {@link OperationFactory ,
 * edu.epam.task6.command.admin.currencies.CurrencyFactory}
 * </p>
 *
 * @author Vladislav Gryadovkin
 */
public interface SubCommand extends UnderCommand {
    /**
     * <p>This method is used to process wrapped http requests</p>
     * @param adaptableHttpRequest is wrapped http request
     * @return
     * @throws ValidationException if the error occurred during validating fields information
     * @throws ServiceException if the error ocurred in service layer
     */
    String execute(AdaptableHttpRequest adaptableHttpRequest) throws ValidationException, ServiceException;
}
