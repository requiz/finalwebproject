package edu.epam.task6.command.admin.insert.impl.currency;

import edu.epam.task6.command.UnderCommand;
import edu.epam.task6.command.admin.insert.SubCommand;
import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.entity.impl.Currency;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.service.CurrencyService;
import edu.epam.task6.service.exception.ServiceException;

/**
 * <p>This class is used for updating currency that exists in the system
 * </p>
 *
 * @author Vladislav Gryadovkin
 * @see UnderCommand ,SubCommand
 */
public class UpdateCurrencyCommand implements SubCommand {
    public static final String RES = "res";
    public static final String CURRENCY = "currency";
    public static final String PAGE_CURRENCY = "page.currency";
    private static UpdateCurrencyCommand currencyCommand = new UpdateCurrencyCommand();

    public static UpdateCurrencyCommand getInstance() {
        return currencyCommand;
    }

    @Override
    public String execute(AdaptableHttpRequest adaptableHttpRequest) throws ServiceException {
        String page = getDefaultPage();
        CurrencyService service = CurrencyService.getInstance();
        String name = adaptableHttpRequest.getParameter(RES);
        Currency currency = service.getCurrencyByName(name);
        adaptableHttpRequest.setAttribute(CURRENCY, currency);
        return page;
    }

    @Override
    public String getDefaultPage() {
        return ConfigManager.getProperty(PAGE_CURRENCY);
    }
}
