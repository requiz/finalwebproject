package edu.epam.task6.command.admin;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.command.Command;
import edu.epam.task6.controller.CommandEnum;
import edu.epam.task6.controller.CommandFactory;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import edu.epam.task6.service.AccountService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
/**
 * <p>This class is used for displaying information about user activity in cards or transactions</p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class SelectUserCommand implements Command {
    public static final String LOGINS = "logins";
    public static final String PAGE_VIEWUSER = "page.viewuser";
    public static final String PAR = "par";
    public static final String TRANSACTION = "trans";
    public static final String ENTER = "selectuser";
    private Logger logger = Logger.getLogger(SelectUserCommand.class.getName());

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigManager.getProperty(PAGE_VIEWUSER);
        MessageHandler handler = MessageHandler.getInstance();
        AdaptableHttpRequest adaptableHttpRequest = new AdaptableHttpRequest(request);
        String type = adaptableHttpRequest.getParameter(PAR);
        String button = adaptableHttpRequest.getParameter(ENTER);
        try {
            if (button == null) {
                AccountService service = AccountService.getInstance();
                request.setAttribute(LOGINS, service.getToAdmin());
                request.setAttribute(PAR, type);
            } else {
                adaptableHttpRequest.removeParameter(ENTER);
                if (type.equals(TRANSACTION)) {
                    page = CommandFactory.getCommand(CommandEnum.VIEWTOADMIN).execute(request);
                } else page = CommandFactory.getCommand(CommandEnum.VIEWUSERCARDS).execute(request);
            }
        } catch (ServiceException e) {
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
            handler.writeError(request, e);
        }
        return page;
    }
}
