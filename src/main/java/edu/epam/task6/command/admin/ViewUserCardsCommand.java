package edu.epam.task6.command.admin;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.command.Command;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.entity.impl.Card;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import edu.epam.task6.service.AccountService;
import edu.epam.task6.service.CardService;
import edu.epam.task6.helper.ViewHelper;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * <p>This class is used for viewing all user cards existing in the system</p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class ViewUserCardsCommand implements Command {
    public static final String PAGE_VIEW = "page.viewusercards";
    public static final String CARDS = "cards";
    public static final String LOGIN = "login";
    public static final String BLOCK = "block";
    public static final String UNBLOCK = "unblock";
    private Logger logger = Logger.getLogger(ViewUserCardsCommand.class.getName());

    @Override
    public String execute(HttpServletRequest request) {
        AdaptableHttpRequest adaptableHttpRequest = new AdaptableHttpRequest(request);
        MessageHandler handler = MessageHandler.getInstance();
        String page = ConfigManager.getProperty(PAGE_VIEW);
        String login = adaptableHttpRequest.getParameter(LOGIN);
        AccountService service = AccountService.getInstance();
        Account account;
        try {
            account = service.getAccountByLogin(login);
            CardService cardService = CardService.getInstance();
            List<Card> cards = cardService.getAll(account);
            request.setAttribute(CARDS, ViewHelper.getUserCards(cards, login));
            request.setAttribute(BLOCK, ViewHelper.setStatusForBlock());
            request.setAttribute(UNBLOCK, ViewHelper.setStatusForUnblock());
        } catch (ServiceException e) {
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
            handler.writeError(request, e);
        }
        return page;
    }
}
