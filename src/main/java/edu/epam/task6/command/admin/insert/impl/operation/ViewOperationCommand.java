package edu.epam.task6.command.admin.insert.impl.operation;

import edu.epam.task6.command.admin.insert.SubCommand;
import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.command.UnderCommand;
import edu.epam.task6.entity.impl.Operation;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.service.OperationService;

import java.util.List;

/**
 * <p>This class is used for operations that exists in the system
 * </p>
 *
 * @author Vladislav Gryadovkin
 * @see UnderCommand ,SubCommand
 */
public class ViewOperationCommand implements SubCommand {
    public static final String PAGE_VIEWOPER = "page.viewoper";
    public static final String OPERATIONS = "operations";
    private static final ViewOperationCommand operationCommand = new ViewOperationCommand();

    private ViewOperationCommand() {

    }

    public static ViewOperationCommand getInstance() {
        return operationCommand;
    }

    @Override
    public String execute(AdaptableHttpRequest request) throws ServiceException {
        String page = getDefaultPage();
        OperationService service = OperationService.getInstance();
        List<Operation> operations = service.getOperations();
        request.setAttribute(OPERATIONS, operations);
        return page;
    }


    @Override
    public String getDefaultPage() {
        return ConfigManager.getProperty(PAGE_VIEWOPER);
    }
}
