package edu.epam.task6.command.admin.insert.impl.operation;

import edu.epam.task6.command.UnderCommand;
import edu.epam.task6.command.admin.insert.SubCommand;
import edu.epam.task6.command.helper.AdaptableHttpRequest;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.service.OperationService;
import edu.epam.task6.validation.FieldsValidator;
import edu.epam.task6.validation.exception.ValidationException;

/**
 * <p>This class is used for updating operation
 * </p>
 *
 * @author Vladislav Gryadovkin
 * @see UnderCommand ,SubCommand
 */
public class UpdateCommand implements SubCommand {

    private static final UpdateCommand update = new UpdateCommand();
    public static final String OPERATION = "operation";
    private static final String PAGE = "page.newoper";
    public static final String ID = "id";
    public static final String PARAMETER = "parameter";
    public static final String TRUE = "true";

    private UpdateCommand() {
    }

    public static UpdateCommand getUpdate() {
        return update;
    }

    @Override
    public String execute(AdaptableHttpRequest request) throws ValidationException {
        String page = getDefaultPage();
        String id = request.getParameter(ID);
        Integer validId = FieldsValidator.validateId(id);
        OperationService service = OperationService.getInstance();
        request.setAttribute(PARAMETER, TRUE);
        request.setAttribute(OPERATION, service.getOperById(validId));
        return page;
    }

    @Override
    public String getDefaultPage() {
        return ConfigManager.getProperty(PAGE);
    }
}
