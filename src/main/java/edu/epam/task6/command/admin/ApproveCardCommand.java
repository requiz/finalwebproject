package edu.epam.task6.command.admin;

import edu.epam.task6.command.helper.MessageHandler;
import edu.epam.task6.command.Command;
import edu.epam.task6.entity.impl.Card;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.manager.ConfigManager;
import edu.epam.task6.manager.LanguageManager;
import edu.epam.task6.manager.MessageManager;
import edu.epam.task6.service.CardService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>This class is used for confirming card creation in the system</p>
 *
 * @author Vladislav Gryadovkin
 * @see Command
 */
public class ApproveCardCommand implements Command {
    private static final String VIEW_NEW_CARDS = "page.approve";
    private static final String CARDS = "cards";
    private Logger logger = Logger.getLogger(ApproveCardCommand.class.getName());

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        MessageHandler handler = MessageHandler.getInstance();
        CardService service = CardService.getInstance();
        page = ConfigManager.getProperty(VIEW_NEW_CARDS);
        try {
            Map<Card, String> cards = service.getCardsForApprove();
            request.setAttribute(CARDS, cards);
        } catch (ServiceException e) {
            logger.error(MessageManager.getProperty(e.getMessage(), LanguageManager.EN.toString()));
            handler.writeError(request, e);
        }
        return page;
    }
}
