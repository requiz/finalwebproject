package edu.epam.task6.service;

import edu.epam.task6.dao.impl.CardTypeDao;
import edu.epam.task6.service.exception.ServiceException;

import java.util.List;

/**
 * <p>This class is used for obtaining parameters from {@link edu.epam.task6.dao.impl.CardTypeDao}
 * and then passing them to the command </p>
 *
 * @author Vladislav Gryadovkin
 */
public class CardTypeService {
    private static final CardTypeService service = new CardTypeService();
    public static final String ADDCARD_TYPE = "addcard.type";

    private CardTypeService() {

    }

    public static CardTypeService getInstance() {
        return service;
    }

    public List<String> getAllTypes() throws ServiceException {
        CardTypeDao dao = CardTypeDao.getInstance();
        List<String> types = dao.getCardTypes();
        if (types.isEmpty()) {
            throw new ServiceException(ADDCARD_TYPE);
        }
        return dao.getCardTypes();
    }
}
