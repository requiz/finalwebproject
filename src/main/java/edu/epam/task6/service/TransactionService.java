package edu.epam.task6.service;

import edu.epam.task6.dao.impl.TransactionDao;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.entity.impl.Operation;
import edu.epam.task6.entity.impl.Transaction;
import edu.epam.task6.entity.impl.status.TransactionStatus;
import edu.epam.task6.service.exception.ServiceException;

import java.util.Map;

/**
 * <p>This class is used for obtaining parameters from {@link edu.epam.task6.dao.impl.TransactionDao}
 * and then passing them to the command </p>
 *
 * @author Vladislav Gryadovkin
 */
public class TransactionService {
    private static final TransactionService service = new TransactionService();
    public static final String TRANSACTION_VIEW = "transaction.view";
    public static final String TRANSACTION_VIEW_ALL = "transaction.viewAll";
    public static final String USER_TRANSACTION = "user.transaction";

    private TransactionService() {

    }

    public static TransactionService getInstance() {
        return service;
    }

    public Map<Transaction, Operation> getAllTransactions(String parameter) throws ServiceException {
        TransactionDao dao = TransactionDao.getInstance();
        Map<Transaction, Operation> transactionMap = dao.getReport(parameter);
        if (transactionMap.isEmpty()) {
            throw new ServiceException(TRANSACTION_VIEW_ALL);
        }
        return transactionMap;
    }

    public Map<Transaction, Operation> getByAccount(Account account) throws ServiceException {
        Map<Transaction, Operation> map;
        TransactionDao dao = TransactionDao.getInstance();
        map = dao.getTransactionToAdmin(account);
        if (map.isEmpty()) {
            throw new ServiceException(USER_TRANSACTION);
        }
        return map;
    }

    public Map<Transaction, Operation> getToClient(Account account) throws ServiceException {
        Map<Transaction, Operation> map;
        TransactionDao dao = TransactionDao.getInstance();
        map = dao.getSelectedTransactions(account);
        if (map.isEmpty()) {
            throw new ServiceException(TRANSACTION_VIEW);
        }
        return map;
    }

    public Transaction getSelectedTransaction(int id) {
        TransactionDao dao = TransactionDao.getInstance();
        return dao.getByParam(id);
    }

    public void cancelTransaction(Transaction transaction, double cardFrom, double cardTo) {
        TransactionDao dao = TransactionDao.getInstance();
        transaction.setStatus(TransactionStatus.CANCEL);
        if (cardTo == 0) {
            dao.cancelPaymentTransaction(transaction, cardFrom);
        } else {
            dao.cancelTransferTransaction(transaction, cardFrom, cardTo);
        }
    }

    public void insertTransferPayment(Transaction transaction, double sum) {
        TransactionDao dao = TransactionDao.getInstance();
        dao.insertServiceTransaction(transaction, sum);
    }


    public void insertTransferToCard(Transaction transaction, double sumFrom, double sumTo) {
        TransactionDao dao = TransactionDao.getInstance();
        dao.insertTransferTransaction(transaction, sumFrom, sumTo);
    }
}
