package edu.epam.task6.service;

import edu.epam.task6.dao.impl.CurrencyDao;
import edu.epam.task6.entity.impl.Card;
import edu.epam.task6.entity.impl.Currency;
import edu.epam.task6.service.exception.ServiceException;

import java.util.List;
/**
 * <p>This class is used for obtaining parameters from {@link edu.epam.task6.dao.impl.CurrencyDao}
 * and then passing them to the command </p>
 *
 * @author Vladislav Gryadovkin
 */
public class CurrencyService {
    public static final String CURRENCY_EXIST_ERROR = "currency.existError";
    public static final String CURRENCY_FIELDS_ERROR = "currency.fieldsError";
    public static final String CARD_ADD = "addcard.currency";
    public final String BALANCE_ERROR = "message.balanceError";

    private CurrencyService() {

    }

    private static final CurrencyService service = new CurrencyService();

    public static CurrencyService getInstance() {
        return service;
    }

    public void insert(Currency currency) throws ServiceException {
        CurrencyDao dao = CurrencyDao.getInstance();
        Currency dbCurrency = dao.getByParam(currency.getName());
        if (dbCurrency == null) {
            dao.insert(currency);
        } else throw new ServiceException(CURRENCY_EXIST_ERROR);
    }

    public Currency getCurrencyByName(String name) throws ServiceException {
        CurrencyDao dao = CurrencyDao.getInstance();
        Currency currency = dao.getByParam(name);
        if (currency == null) {
            throw new ServiceException(CARD_ADD);
        }
        return currency;
    }

    public void update(Currency validCurrency) throws ServiceException {
        CurrencyDao dao = CurrencyDao.getInstance();
        Currency dbCurrency = dao.getByParam(validCurrency.getName());
        if (dbCurrency.equals(validCurrency)) {
            throw new ServiceException(CURRENCY_FIELDS_ERROR);
        } else dao.update(validCurrency);
    }

    public List<Currency> getAllCurrencies() throws ServiceException {
        CurrencyDao dao = CurrencyDao.getInstance();
        List<Currency> currencies = dao.getAll();
        if (currencies.isEmpty()) {
            throw new ServiceException(CARD_ADD);
        }
        return currencies;
    }

    public void validateCurrency(Card card) throws ServiceException {
        CurrencyDao currencyDao = CurrencyDao.getInstance();
        Currency currency = currencyDao.getByParam(card.getCurrency());
        if (currency.getMax_sum() < card.getBalance()) {
            throw new ServiceException(BALANCE_ERROR);
        }
    }
}
