package edu.epam.task6.service;

import edu.epam.task6.dao.impl.OperationDao;
import edu.epam.task6.entity.impl.Operation;
import edu.epam.task6.service.exception.ServiceException;

import java.util.List;

/**
 * <p>This class is used for obtaining parameters from  {@link edu.epam.task6.dao.impl.OperationDao}
 *
 * @author Vladislav Gryadovkin
 */
public class OperationService {
    private static final OperationService service = new OperationService();
    public static final String OPERATION_ERROR_EXIST = "operation..error.exist";
    public static final String OPERATIONS_ERROR_UPDATE = "operations.error.update";
    public static final String PAY_SERVICEOPER = "pay.serviceoper";
    public static final String VIEWOPER_EXIST = "viewoper.exist";
    public static final String OPERATION_NOTEXIST = "operation.notexist";

    public static OperationService getInstance() {
        return service;
    }

    public void insert(Operation operation) throws ServiceException {
        OperationDao dao = OperationDao.getInstance();
        Operation existOperation = dao.getByName(operation.getName());
        if (existOperation == null) {
            dao.insert(operation);
        } else throw new ServiceException(OPERATION_ERROR_EXIST);
    }

    public List<Operation> getOperations() throws ServiceException {
        OperationDao dao = OperationDao.getInstance();
        List<Operation> operations = dao.getAll();
        if (operations.isEmpty()) {
            throw new ServiceException(VIEWOPER_EXIST);
        }
        return operations;
    }

    public Operation getOperByName(String name) throws ServiceException {
        OperationDao dao = OperationDao.getInstance();
        Operation operation = dao.getByName(name);
        if (operation == null) {
            throw new ServiceException(OPERATION_NOTEXIST);
        }
        return operation;
    }

    public List<Operation> getActiveOperations() throws ServiceException {
        OperationDao dao = OperationDao.getInstance();
        List<Operation> operations = dao.getActive();
        if (operations.isEmpty()) {
            throw new ServiceException(PAY_SERVICEOPER);
        }
        return operations;
    }

    public void update(Operation validOperation) throws ServiceException {
        OperationDao operationDao = OperationDao.getInstance();
        Operation dbOperation = operationDao.getByParam(validOperation.getId());
        if (dbOperation.equals(validOperation)) {
            throw new ServiceException(OPERATIONS_ERROR_UPDATE);
        } else {
            operationDao.update(validOperation);
        }
    }

    public Operation getOperById(int id) {
        OperationDao dao = OperationDao.getInstance();
        return dao.getByParam(id);
    }

    public void updateStatus(Operation operation) {
        OperationDao operationDao = OperationDao.getInstance();
        operationDao.changeStatus(operation);
    }
}
