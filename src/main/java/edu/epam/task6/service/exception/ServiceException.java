package edu.epam.task6.service.exception;

/**
 * <p>This class is used for description of errors which occurred when something wrong in services </p>
 *
 * @author Vladislav Gryadovkin
 */
public class ServiceException extends Exception {
    public ServiceException() {
        super();
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(Throwable throwable) {
        super(throwable);
    }

    public ServiceException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
