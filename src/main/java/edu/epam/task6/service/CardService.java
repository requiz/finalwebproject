package edu.epam.task6.service;

import edu.epam.task6.dao.impl.CardDao;
import edu.epam.task6.dao.impl.CurrencyDao;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.entity.impl.Card;
import edu.epam.task6.entity.impl.Currency;
import edu.epam.task6.entity.impl.Transaction;
import edu.epam.task6.entity.impl.status.AccountStatus;
import edu.epam.task6.entity.impl.status.CardStatus;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.helper.ViewHelper;

import java.util.List;
import java.util.Map;

/**
 * <p>This class is used for obtaining parameters from {@link edu.epam.task6.dao.impl.CardDao}
 * and then passing them to the command </p>
 *
 * @author Vladislav Gryadovkin
 */
public class CardService {
    private static final CardService service = new CardService();
    public static final String EMPTY_LIST = "empty.list";
    public static final String CARD_VIEW = "card.view";
    public static final String PAY_SERVICECARD = "pay.servicecard";
    public static final String MESSAGE = "pay.carddestination";
    public static final String APPROVE_CARD = "approve.card";
    public static final String CARD_CREATE = "card.create";
    private final String CARD_DROP_ERROR = "card.drop.error";
    private final String MESSAGE_CANCEL = "message.cancel";
    private final String MESSAGE_CURRENCY = "message.currency";
    private final String MESSAGE_DECREASE = "message.decrease";

    private CardService() {

    }

    public static CardService getInstance() {
        return service;
    }

    public List<Card> getAll(Account account) throws ServiceException {
        CardDao dao = CardDao.getInstance();
        List<Card> cards = dao.getUserCards(account);
        if (cards.isEmpty()) {
            throw new ServiceException(CARD_VIEW);
        }
        return cards;
    }

    public List<Card> getWorkingCards(Account account) throws ServiceException {
        CardDao cardDao = CardDao.getInstance();
        List<Card> cards = cardDao.getCardWithStatus(account, CardStatus.WORKING);
        if (cards.isEmpty()) {
            throw new ServiceException(PAY_SERVICECARD);
        }
        return cards;
    }

    public void changeStatus(long number, Account account, String status) {
        CardDao dao = CardDao.getInstance();
        Card card = dao.getByParam(number);
        if (card.getStatus() == CardStatus.WORKING && account.getStatus().equals(AccountStatus.USER)) {
            card.setStatus(CardStatus.BLOCKED);
        } else if (card.getStatus() == CardStatus.BLOCKED && account.getStatus().equals(AccountStatus.USER)) {
            card.setStatus(CardStatus.WORKING);
        } else if (card.getStatus() == CardStatus.NEW && account.getStatus().equals(AccountStatus.ADMIN)) {
            card.setStatus(CardStatus.valueOf(status.toUpperCase()));
        } else if (card.getStatus() == CardStatus.ADMIN_BLOCKED && account.getStatus().equals(AccountStatus.ADMIN)) {
            card.setStatus(CardStatus.WORKING);
        } else if (account.getStatus().equals(AccountStatus.ADMIN) && validateBlockStatus(card)) {
            card.setStatus(CardStatus.ADMIN_BLOCKED);
        }
        dao.update(card);
    }

    public Boolean validateBlockStatus(Card card) {
        Boolean result = false;
        List<CardStatus> list = ViewHelper.setStatusForBlock();
        for (CardStatus aList : list) {
            if (aList == card.getStatus()) {
                result = true;
                break;
            }
        }
        return result;
    }

    public void validateBeforeDel(long number) throws ServiceException {
        CardDao dao = CardDao.getInstance();
        Card card = dao.getByParam(number);
        if (card == null) {
            throw new ServiceException(EMPTY_LIST);
        } else if (card.getStatus() == CardStatus.BLOCKED) {
            throw new ServiceException(CARD_DROP_ERROR);
        } else dao.delete(card);
    }

    public List<Card> getAllenCards(int id) throws ServiceException {
        CardDao cardDao = CardDao.getInstance();
        List<Card> list = cardDao.getAllenCards(id);
        if (list.isEmpty()) {
            throw new ServiceException(MESSAGE);
        }
        return list;
    }

    public void insert(Card card) throws ServiceException {
        CardDao dao = CardDao.getInstance();
        Card dbCard = dao.getByParam(card.getNumber());
        if (dbCard == null) {
            dao.insert(card);
        } else throw new ServiceException(CARD_CREATE);
    }

    public double increaseAmmount(Card card, double course, double payment) {
        double converterCourse = payment / course;
        double money = card.getBalance() + converterCourse;
        return money;
    }

    public double validateCurrency(Card card) throws ServiceException {
        CurrencyDao dao = CurrencyDao.getInstance();
        if (card.getStatus() == CardStatus.BLOCKED || card.getStatus() == CardStatus.ADMIN_BLOCKED) {
            throw new ServiceException(MESSAGE_CURRENCY);
        }
        Currency currency = dao.getByParam(card.getCurrency());
        return currency.getCourse();
    }

    public double decreaseAmmount(Card card, double course, double payment) throws ServiceException {
        double converterCourse = payment / course;
        double money = card.getBalance() - converterCourse;
        if (card.getBalance() < converterCourse) {
            throw new ServiceException(MESSAGE_DECREASE);
        }
        return money;
    }

    public Card getByNumber(long number) {
        CardDao cardDao = CardDao.getInstance();
        return cardDao.getByParam(number);
    }

    public double withdrawMoney(Transaction transaction) throws ServiceException {
        CardDao cardDao = CardDao.getInstance();
        Card card = cardDao.getByParam(transaction.getNumber());
        double sumOfMoney = decreaseAmmount(card, validateCurrency(card), transaction.getAmmount());
        return sumOfMoney;
    }

    public double putMoney(Transaction transaction) throws ServiceException {
        CardDao dao = CardDao.getInstance();
        Card allen = dao.getByParam(transaction.getNumberTo());
        double sumPlus = increaseAmmount(allen, validateCurrency(allen), transaction.getAmmount());
        return sumPlus;
    }

    public double getMoneyBack(Transaction transaction) throws ServiceException {
        CardDao dao = CardDao.getInstance();
        Card card = dao.getByParam(transaction.getNumber());
        double sumOfMoney = increaseAmmount(card, validateCurrency(card), transaction.getAmmount());
        return sumOfMoney;
    }

    public double validateCardTo(Transaction transaction) throws ServiceException {
        CardDao dao = CardDao.getInstance();
        if (transaction.getNumberTo() != 0) {
            Card cardTo = dao.getByParam(transaction.getNumberTo());
            if (cardTo == null) {
                throw new ServiceException(MESSAGE_CANCEL);
            }
            double sumMinus = decreaseAmmount(cardTo, validateCurrency(cardTo), transaction.getAmmount());
            return sumMinus;
        }
        return 0;
    }

    public Map<Card, String> getCardsForApprove() throws ServiceException {
        CardDao dao = CardDao.getInstance();
        Map<Card, String> cardStringMap = dao.selectNewStatus();
        if (cardStringMap.isEmpty()) {
            throw new ServiceException(APPROVE_CARD);
        }
        return cardStringMap;

    }
}
