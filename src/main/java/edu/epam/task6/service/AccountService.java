package edu.epam.task6.service;

import edu.epam.task6.dao.impl.AccountDao;
import edu.epam.task6.entity.impl.Account;
import edu.epam.task6.service.exception.ServiceException;
import edu.epam.task6.validation.Cipher;

import java.util.List;

/**
 * <p>This class is used for obtaining parameters from {@link edu.epam.task6.dao.impl.AccountDao}
 * and then passing them to the command </p>
 *
 * @author Vladislav Gryadovkin
 */
public class AccountService {
    private static final AccountService service = new AccountService();
    public static final String MESSAGE_FIELDS = "message.notvalidUser";
    public static final String MESSAGE_PASSWORD = "message.passwordError";
    public static final String MESSAGE_REPEAT_LOGIN = "message.repeatLogin";
    public static final String MESSAGE_UPDATE = "message.update";
    public static final String SELECT_USER = "select.user";
    public static final String MESSAGE_NEW_PASSWORD = "message.newPassword";
    public static final String MESSAGE_LOGINERROR = "message.loginerror";
    public static final String MESSAGE_EMAILERROR = "message.emailerror";

    private AccountService() {

    }

    public static AccountService getInstance() {
        return service;
    }

    public Account retrieveAccount(Account account) throws ServiceException {
        AccountDao dao = AccountDao.getAccountDao();
        Account dbAccount;
        dbAccount = dao.getByParam(account.getLogin());
        if (dbAccount != null) {
            String encryptedPassword = Cipher.getHex(account.getPassword());
            if (!encryptedPassword.equals(dbAccount.getPassword())) {
                throw new ServiceException(MESSAGE_PASSWORD);
            }
        } else throw new ServiceException(MESSAGE_FIELDS);
        return dbAccount;
    }

    public void addAccount(Account account) throws ServiceException {
        AccountDao dao = AccountDao.getAccountDao();
        Account dbAccount = dao.getByParam(account.getLogin());
        if (dbAccount == null) {
            String encryptedPassword = Cipher.getHex(account.getPassword());
            account.setPassword(encryptedPassword);
            dao.insert(account);
        } else throw new ServiceException(MESSAGE_REPEAT_LOGIN);
    }

    public List<Account> getUserAccounts(int page, int recordsPerPage) throws ServiceException {
        AccountDao accountDao = AccountDao.getAccountDao();
        List<Account> accountList = accountDao.getAll(page, recordsPerPage);
        if (accountList.isEmpty()) {
            throw new ServiceException(SELECT_USER);
        }
        return accountList;
    }

    public boolean updateInfoAccount(Account accountConfirm) throws ServiceException {
        boolean result;
        AccountDao dao = AccountDao.getAccountDao();
        Account account = dao.getById(accountConfirm.getId());
        if (!account.equals(accountConfirm)) {
            dao.update(accountConfirm);
            result = true;
        } else throw new ServiceException(MESSAGE_UPDATE);
        return result;
    }

    public Account updatePassword(Account account, String password) throws ServiceException {
        AccountDao dao = AccountDao.getAccountDao();
        String encryptedPassword = Cipher.getHex(password);
        Account dbAccount = getAccountByLogin(account.getLogin());
        if (dbAccount.getPassword().equals(encryptedPassword)) {
            throw new ServiceException(MESSAGE_NEW_PASSWORD);
        }
        dbAccount.setPassword(encryptedPassword);
        dao.changePassword(dbAccount);
        return dbAccount;
    }

    public Account getAccountByLogin(String login) throws ServiceException {
        Account account;
        AccountDao dao = AccountDao.getAccountDao();
        account = dao.getByParam(login);
        if (account == null) {
            throw new ServiceException(MESSAGE_LOGINERROR);
        }
        return account;
    }

    public boolean validateDataForRecovery(Account account) throws ServiceException {
        Account dbAccount = getAccountByLogin(account.getLogin());
        if (!dbAccount.getEmail().equals(account.getEmail())) {
            throw new ServiceException(MESSAGE_EMAILERROR);
        }
        return true;
    }

    public List<String> getToAdmin() throws ServiceException {
        AccountDao accountDao = AccountDao.getAccountDao();
        List<String> logins = accountDao.getUserLogins();
        if (logins.isEmpty()) {
            throw new ServiceException(SELECT_USER);
        }
        return logins;
    }

    public int numberOfRecords() {
        AccountDao dao = AccountDao.getAccountDao();
        return dao.getNoOfRecords();
    }
}
