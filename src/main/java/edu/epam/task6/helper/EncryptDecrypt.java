package edu.epam.task6.helper;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class EncryptDecrypt {
    public static final String TRUE = "true";
    public static final String JASYPT = "jasypt";
    public static final String workingDirectory = "properties";
    public static final String separator = "/";
    public static final String FALSE = "false";
    private String propertyFileName;
    private final String propertyKey = "password";
    private final String isPropertyKeyEncrypted = "is.database.user.password.encrypted";
    private Lock lock = new ReentrantLock();


    public EncryptDecrypt(String pPropertyFileName) {
        this.propertyFileName = workingDirectory + separator + pPropertyFileName;

    }

    public void encryptPropertyValue() throws ConfigurationException {

        PropertiesConfiguration config = new PropertiesConfiguration(propertyFileName);
        String isEncrypted = config.getString(isPropertyKeyEncrypted);
        if (isEncrypted.equals(FALSE)) {
            lock.lock();
            try {
                String tmpPwd = config.getString(propertyKey);
                StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
                encryptor.setPassword(JASYPT);
                String encryptedPassword = encryptor.encrypt(tmpPwd);
                config.setProperty(propertyKey, encryptedPassword);
                config.setProperty(isPropertyKeyEncrypted, TRUE);
                config.save();
            } finally {
                lock.unlock();
            }
        }
    }

    public String decryptPropertyValue() throws ConfigurationException {
        PropertiesConfiguration config = new PropertiesConfiguration(propertyFileName);
        String encryptedPropertyValue = config.getString(propertyKey);
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(JASYPT);
        String decryptedPropertyValue = encryptor.decrypt(encryptedPropertyValue);
        return decryptedPropertyValue;
    }
}