package edu.epam.task6.helper;

import edu.epam.task6.entity.impl.Card;
import edu.epam.task6.entity.impl.Operation;
import edu.epam.task6.entity.impl.Transaction;
import edu.epam.task6.entity.impl.status.CardStatus;

import java.util.*;

/**
 * <p>This class is acts as helper class which main role is to display entity in needed format</p>
 *
 * @author Vladislav Gryadovkin
 */
public class ViewHelper {

    public static final String BY_SUM = "bysum";
    public static final String BY_DATE = "bydate";

    public static List<CardStatus> setNotChangedStatus() {
        List<CardStatus> list = new ArrayList<>();
        list.add(CardStatus.NEW);
        list.add(CardStatus.ADMIN_BLOCKED);
        list.add(CardStatus.DISAPPROVED);
        return list;
    }

    public static Map<String, List<Card>> getUserCards(List<Card> cards, String login) {
        Map<String, List<Card>> listMap = new HashMap<>();
        if (cards.size() != 0) {
            listMap.put(login, cards);
            return listMap;
        }
        return null;
    }

    public static List<CardStatus> setStatusForBlock() {
        List<CardStatus> list = new ArrayList<>();
        list.add(CardStatus.WORKING);
        list.add(CardStatus.BLOCKED);
        return list;
    }

    public static List<CardStatus> setStatusForUnblock() {
        List<CardStatus> list = new ArrayList<>();
        list.add(CardStatus.ADMIN_BLOCKED);
        return list;
    }

    public static List<Card> sortCards(List<Card> cards) {
        Collections.sort(cards, (o1, o2) -> {
            if (o1.getStatus() == o2.getStatus()) {
                return Long.compare(o1.getNumber(), o2.getNumber());
            } else return o1.getStatus().compareTo(o2.getStatus());
        });
        return cards;
    }

    public static Map<Transaction, Operation> sortTransaction(String parameter, Map<Transaction, Operation> transactions) {
        List<Map.Entry<Transaction, Operation>> entryList = new ArrayList<>(transactions.entrySet());
        if (parameter.equals(BY_DATE)) {
            sortByDate(entryList);
        } else if (parameter.equals(BY_SUM)) {
            sortByBalance(entryList);
        }
        Map<Transaction, Operation> transactionStringMap = getLinkedMap(entryList);
        return transactionStringMap;
    }

    public static Map<Transaction, Operation> getLinkedMap(List<Map.Entry<Transaction, Operation>> entries) {
        Map<Transaction, Operation> transactionStringMap = new LinkedHashMap<>();
        for (Map.Entry<Transaction, Operation> sortedList : entries) {
            transactionStringMap.put(sortedList.getKey(), sortedList.getValue());
        }

        return transactionStringMap;
    }

    private static void sortByDate(List<Map.Entry<Transaction, Operation>> map) {
        Collections.sort(
                map, (transactionStringEntry, transactionStringEntry2) ->
                        transactionStringEntry2.getKey().getDatetime()
                                .compareTo(transactionStringEntry.getKey().getDatetime())
        );
    }

    private static void sortByBalance(List<Map.Entry<Transaction, Operation>> map) {
        Collections.sort(
                map, (transactionStringEntry, transactionStringEntry2) ->
                        transactionStringEntry.getKey().getAmmount()
                                .compareTo(transactionStringEntry2.getKey().getAmmount())
        );
    }
}

