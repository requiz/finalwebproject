package edu.epam.task6.pool;

import edu.epam.task6.helper.EncryptDecrypt;
import org.apache.commons.configuration.ConfigurationException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * <p>This class is used for reading properties from MYSQL database</p>
 *
 * @author Vladislav Gryadovkin
 */
public class DBConnection {

    public static final String URL = "url";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String USER = "user";
    public static final String USE_UNICODE = "useUnicode";
    public static final String TRUE = "true";
    public static final String CHARACTER_ENCODING = "characterEncoding";
    public static final String UTF_8 = "UTF-8";
    public static final String DB_PROPERTIES = "db.properties";

    public static Connection readProperties(ResourceBundle bundle) throws SQLException, ConfigurationException {
        EncryptDecrypt encryptDecrypt = new EncryptDecrypt(DB_PROPERTIES);
        encryptDecrypt.encryptPropertyValue();
        String password = encryptDecrypt.decryptPropertyValue();
        String url = bundle.getString(URL);
        String user = bundle.getString(LOGIN);
        Properties properties = new Properties();
        properties.setProperty(USER, user);
        properties.setProperty(PASSWORD, password);
        properties.setProperty(USE_UNICODE, TRUE);
        properties.setProperty(CHARACTER_ENCODING,
                UTF_8);
        return DriverManager.getConnection(url, properties);
    }
}
