package edu.epam.task6.pool;

import com.mysql.jdbc.Driver;
import edu.epam.task6.pool.exception.PoolException;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
/**
 * <p>This class is used for allocation  given number of connections from database.
 * during validating fields</p>
 *
 * @author Vladislav Gryadovkin
 */
public class ConnectionPool {
    private static ResourceBundle bundle = ResourceBundle.getBundle("properties.db");
    private static Logger logger = Logger.getLogger(ConnectionPool.class.getName());
    private static BlockingQueue<PooledConnection> connections;
    private static AtomicBoolean isCreated = new AtomicBoolean(false);
    private static AtomicBoolean isOpened = new AtomicBoolean(false);
    private static Lock lock = new ReentrantLock();
    private static ConnectionPool instance;
    private static final int MAX_CONNECTIONS = 20;

    private ConnectionPool() {
        connections = new ArrayBlockingQueue<>(MAX_CONNECTIONS);
    }

    public static ConnectionPool getInstance() {
        if (!isCreated.get()) {
            lock.lock();
            if (instance == null) {
                instance = new ConnectionPool();
                isCreated.set(true);
            }
            lock.unlock();
        }
        return instance;
    }

    public void openConnection() {
        PooledConnection pool;
        try {
            if (!isOpened.get()) {
                lock.lock();
                try {
                    DriverManager.registerDriver(new Driver());
                    Connection connection = DBConnection.readProperties(bundle);
                    for (int i = 0; i < MAX_CONNECTIONS; i++) {
                        pool = new PooledConnection(connection);
                        connections.add(pool);
                    }
                    isOpened.set(true);
                } finally {
                    lock.unlock();
                }
            }
        } catch (SQLException | ConfigurationException e) {
            logger.error(e.getMessage());
            throw new PoolException(e);
        }
    }

    public void retrieveConnection(PooledConnection pooled) {
        try {
            connections.put(pooled);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
            throw new PoolException(e);
        }
    }

    public void closePool() {
        for (PooledConnection pool : connections) {
            try {
                if (pool != null) {
                    pool.closeConnection();
                }
            } catch (SQLException e) {
                logger.error(e.getMessage());
                throw new PoolException(e);
            }
        }
    }

    public PooledConnection takeConnection() {
        PooledConnection connection = null;
        try {
            connection = connections.take();
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }
        return connection;
    }
}
