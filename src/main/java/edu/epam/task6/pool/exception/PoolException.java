package edu.epam.task6.pool.exception;

/**
 * <p>This class is used for description of errors which occurred when something wrong
 * during establishing connection to database</p>
 *
 * @author Vladislav Gryadovkin
 */
public class PoolException extends RuntimeException {
    public PoolException() {
        super();
    }

    public PoolException(String message) {
        super(message);
    }

    public PoolException(Throwable throwable) {
        super(throwable);
    }

    public PoolException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
