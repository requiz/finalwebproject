package edu.epam.task6.pool;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.File;
import java.util.TimeZone;
/**
 * <p>This class is used for initializing pool and log4j properties</p>
 *
 * @author Vladislav Gryadovkin
 */
public class PoolListener implements ServletContextListener {
    public static final String LOG_4_J_CONFIGURATION = "log4jConfiguration";
    public static final String EUROPE_MINSK = "Europe/Minsk";
    private ConnectionPool pool = ConnectionPool.getInstance();

    @Override
    public void contextInitialized(ServletContextEvent event) {
        TimeZone timeZone = TimeZone.getTimeZone(EUROPE_MINSK);
        TimeZone.setDefault(timeZone);
        ServletContext sc = event.getServletContext();
        String log4jFile = sc.getInitParameter(LOG_4_J_CONFIGURATION);

        if (log4jFile == null) {
            BasicConfigurator.configure();
        } else {
            String webAppPath = sc.getRealPath("/");
            String log4jProp = webAppPath + log4jFile;
            File output = new File(log4jProp);
            if (output.exists()) {
                PropertyConfigurator.configure(log4jProp);
            } else {
                BasicConfigurator.configure();
            }
        }
        pool.openConnection();
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        pool.closePool();

    }
}
