<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title></title>
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js"/>"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<jsp:include page="menu.jsp"/>
<form action="${pageContext.request.contextPath}/controller" class="form-horizontal">

    <input type="hidden" name="command" id="clicked_button" value="">
    <input type="hidden" name="param" value="admin">
    <input type="hidden" name="par" value="${requestScope.par}">

    <div class="container bootstrap snippet">
        <c:set var="cur" value="${currency}"/>
        <legend><fmt:message key="user.header"/></legend>
        <div class="row">
            <c:out value="${errorMessage}"/>
            <div class="form-group">
                <label class="col-md-4 control-label" for="login"><fmt:message key="user.select"/> </label>

                <div class="col-md-4">
                    <select id="login" name="login" class="form-control input-group">
                        <c:forEach items="${logins}" var="login">
                            <option>${login}</option>
                        </c:forEach>
                    </select>

                </div>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <button type="submit" class="btn btn-success btn-md" name="selectuser"
                                onclick="return buttonClick(this)"
                                value="ok"><fmt:message key="user.approve"/>
                        </button>
                        <button type="submit" class="btn btn-default btn-md" name="main" formnovalidate
                                onclick="return buttonClick(this)" formmethod="post"><fmt:message key="newCard.cancel"/>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>
