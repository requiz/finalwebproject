<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title></title>
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js" />"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<jsp:include page="menu.jsp"/>
<form action="${pageContext.request.contextPath}/controller">
    <input type="hidden" name="command" value="approve">
    <table class="table table-striped table-bordered table-responsive">
        <c:if test="${not empty accounts}">
            <thead>
            <tr>
                <th><fmt:message key="login.login"/></th>
                <th><fmt:message key="registration.email"/></th>
                <th><fmt:message key="registration.contactNmber"/></th>
            </tr>
            </thead>
        </c:if>
        <tbody>
        <c:forEach items="${accounts}" var="account">
            <tr>
                <td><c:out value="${account.login}"/></td>
                <td><c:out value="${account.email}"/></td>
                <td><c:out value="${account.contactNumber}"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <c:if test="${currentPage != 1}">
        <a href="${pageContext.request.contextPath}/controller?command=viewusers&page=${currentPage - 1}">Previous</a>
    </c:if>
    <table border="1" cellpadding="5" cellspacing="5">
        <tr>
            <c:forEach begin="1" end="${noOfPages}" var="i">
                <c:choose>
                    <c:when test="${currentPage eq i}">
                        <td>${i}</td>
                    </c:when>
                    <c:otherwise>
                        <td><a href="${pageContext.request.contextPath}/controller?command=viewusers&page=${i}">${i}</a></td>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </tr>
    </table>
    <c:if test="${currentPage lt noOfPages}">
        <a href="${pageContext.request.contextPath}/controller?command=viewusers&page=${currentPage + 1}">Next</a>
    </c:if>
</form>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>
