<%@ taglib prefix="ctg" uri="customtags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js" />"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<jsp:include page="/jsp/common/defineMenu.jsp"/>
<c:set var="message" value="${success}"/>
<c:set var="error" value="${errorMessage}"/>
<form action="${pageContext.request.contextPath}/controller" id="form">
    <input type="hidden" name="commandTag" value=<ctg:CommandTag/>>
    <input type="hidden" name="command" value="cardstatus">

    <div class="panel-body">
        <div class="row">
            <c:if test="${message!=null}">
                <div class="alert alert-info">
                    <strong> <c:out value="${message}"/></strong>
                </div>
            </c:if>
            <c:if test="${error!=null}">
                <div class="alert alert-danger">
                    <strong> <c:out value="${error}"/></strong>
                </div>
            </c:if>
        </div>
    </div>
    <div class="table-responsive">
        <table align="center" style="width: auto;" class="table table-bordered table-striped  table-condensed">
            <c:if test="${not empty cards}">
                <thead>
                <tr>
                    <th><fmt:message key="viewCards.number"/></th>
                    <th><fmt:message key="viewCards.balance"/></th>
                    <th><fmt:message key="viewCards.currency"/></th>
                    <th><fmt:message key="viewCards.dateopen"/></th>
                    <th class="cent"><fmt:message key="viewCards.status"/></th>
                    <th class="cent" colspan=2><fmt:message key="viewCards.action"/></th>
                </tr>
                </thead>
            </c:if>
            <tbody>
            <c:forEach items="${cards}" var="mapCard" varStatus="loop">
                <c:set var="card" value="${mapCard.value}"/>
                <input type="hidden" name="login" value="${mapCard.key}">
                <c:forEach items="${card}" var="owncard">
                    <tr style="text-align: center">
                        <td><c:out value="${owncard.number}"/></td>
                        <td><fmt:formatNumber value="${owncard.balance}"
                                              type="number"/></td>
                        <td><c:out value="${owncard.currency}"/></td>
                        <td><fmt:formatDate pattern="dd.MM.yy"
                                            value="${owncard.date}"/></td>
                        <td><c:out value="${owncard.status}"/></td>

                        <c:set var="paramter_block" value="false"/>
                        <c:forEach var="item" items="${block}">
                            <c:if test="${item eq owncard.status}">
                                <c:set var="paramter_block" value="true"/>
                            </c:if>
                        </c:forEach>
                        <c:set var="paramter_unblock" value="false"/>
                        <c:forEach var="parameter" items="${unblock}">
                            <c:if test="${parameter eq owncard.status}">
                                <c:set var="paramter_unblock" value="true"/>
                            </c:if>
                        </c:forEach>
                        <c:choose>
                            <c:when test="${paramter_block == true}">
                                <td class="danger t4">
                                    <button type="submit" formmethod="post"
                                            class="btn btn-link"
                                            value="${owncard.number}"
                                            name="number">
                                        <fmt:message key="usercards.block"/>
                                    </button>
                                </td>
                            </c:when>
                            <c:when test="${paramter_unblock == true}">
                                <td class="success t4">
                                    <button type="submit" formmethod="post"
                                            class="btn btn-link"
                                            value="${owncard.number}"
                                            name="number">
                                        <fmt:message key="usercards.unblock"/>
                                    </button>
                                </td>
                            </c:when>
                            <c:otherwise>
                                <td></td>
                            </c:otherwise>
                        </c:choose>
                    </tr>
                </c:forEach>
            </c:forEach>
            </tbody>
        </table>
    </div>
</form>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>
