<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title></title>
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js"/>"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<jsp:include page="menu.jsp"/>
<form action="${pageContext.request.contextPath}/controller" class="form-horizontal" method="post">

    <input type="hidden" name="command" id="clicked_button" value="">
    <input type="hidden" name="name" value="new">
    <input type="hidden" name="id" value="${operation.id}">
    <c:set var="error" value="${errorMessage}"/>
    <div class="container bootstrap snippet">
        <c:set var="oper" value="${operation}"/>
        <c:if test="${not empty parameter}">
            <input type="hidden" name="update" value="true">
        </c:if>
        <legend><fmt:message key="operation.header"/></legend>
        <div class="row">
            <c:if test="${not empty error}">
                <div class="alert alert-danger">
                    <c:out value="${error}"/>
                </div>
            </c:if>
            <div class="form-group">
                <label class="col-md-4 control-label" for="operation"><fmt:message key="operation.name"/> </label>

                <div class="col-md-4">
                    <textarea rows="4" id="operation" cols="25" class="form-control input-md"
                              name="title">${oper.name}</textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="description"><fmt:message
                        key="operation.description"/> </label>

                <div class="col-md-4">
                    <textarea rows="4" id="description" cols="25" class="form-control input-md"
                              name="description">${oper.description}</textarea>

                </div>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <button type="submit" class="btn btn-success btn-md" name="oper"
                                value="Ok" onclick="return buttonClick(this)"><fmt:message key="profile.save"/>
                        </button>
                        <button type="submit" class="btn btn-default btn-md" name="main" formnovalidate
                                onclick="return buttonClick(this)"><fmt:message key="newCard.cancel"/>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>
