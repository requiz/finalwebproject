<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title></title>
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js" />"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<jsp:include page="menu.jsp"/>
<c:set var="message" value="${errorMessage}"/>
<form action="${pageContext.request.contextPath}/controller" role="form">
    <input type="hidden" name="command" value="viewalltransactions">

    <div class="container bootstrap snippet">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <fmt:message key="transaction.report"/>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <c:if test="${message!=null}">
                                <div class="alert alert-danger">
                                    <strong> <c:out value="${message}"/></strong>
                                </div>
                            </c:if>
                            <div style="margin-top:30px;" class="col-xs-12 col-sm-12 col-md-12 login-box">
                                <div class="form-group text-center">
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-primary btn-md" name="report"
                                                value="day"><fmt:message key="transaction.day"/>
                                        </button>
                                        <button type="submit" class="btn btn-default btn-md" name="report"
                                                value="week"><fmt:message key="transaction.week"/>
                                        </button>
                                        <button type="submit" class="btn btn-info btn-md" name="report"
                                                value="month"><fmt:message key="transaction.month"/>
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>
