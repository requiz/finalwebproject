<%@ taglib prefix="ctg" uri="customtags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
    <script src="<c:url value="/resources/javascript/jquery.min.js"/> "></script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<jsp:include page="menu.jsp"/>
<c:set var="message" value="${success}"/>
<div class="panel-body">
    <div class="row">
        <c:if test="${message!=null}">
            <div class="alert alert-info">
                <strong> <c:out value="${message}"/></strong>
            </div>
        </c:if>
    </div>
</div>
<div class="table-responsive">
    <table align="center" class="table table-bordered table-striped table-condensed">
        <c:if test="${not empty currencies}">
            <thead>
            <tr>
                <th><fmt:message key="viewoper.name"/></th>
                <th><fmt:message key="viewcur.course"/></th>
                <th><fmt:message key="viewcur.maxsum"/></th>
                <th><fmt:message key="viewCards.status"/></th>
            </tr>
            </thead>
        </c:if>
        <tbody>

        <c:forEach items="${currencies}" var="cur" varStatus="loop">
            <tr>
                <td><c:out value="${cur.name}"/></td>
                <td><fmt:formatNumber value="${cur.course}"
                                      type="number"/></td>
                <td><fmt:formatNumber value="${cur.max_sum}"
                                      type="number"/></td>
                <td><a
                        href="<%=request.getContextPath()%>/controller?command=currency&name=update&res=<c:out
                value="${cur.name}"/>"><fmt:message key="viewcur.update"/> </a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>
