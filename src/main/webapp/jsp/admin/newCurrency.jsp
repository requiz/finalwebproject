<%@ taglib prefix="ctg" uri="customtags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title></title>
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js"/>"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<jsp:include page="menu.jsp"/>
<form action="${pageContext.request.contextPath}/controller" class="form-horizontal" method="post">

    <input type="hidden" name="command" id="clicked_button" value="">
    <input type="hidden" name="name" value="insert">
    <input type="hidden" name="param" value="update">
    <c:set var="error" value="${errorMessage}"/>
    <div class="container bootstrap snippet">
        <c:set var="cur" value="${currency}"/>
        <c:if test="${not empty cur}">
            <input type="hidden" name="update" value="true">
        </c:if>
        <legend><fmt:message key="currency.header"/></legend>
        <div class="row">
            <c:if test="${not empty error}">
                <div class="alert alert-danger">
                    <c:out value="${error}"/>
                </div>
            </c:if>
            <div class="form-group">
                <label class="col-md-4 control-label" for="currency"><fmt:message key="currency.name"/>
                </label>

                <div class="col-md-4">
                    <input id="currency" name="currency" type="text"
                           class="form-control input-md" value="${cur.name}"
                    <c:if test="${not empty cur}">
                           readonly="readonly"
                    </c:if>
                           pattern="[a-zA-Z]+" required>

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="course"><fmt:message key="currency.course"/> </label>

                <div class="col-md-4">
                    <input id="course" name="course" type="text" class="form-control input-md"
                    <c:if test="${cur.name == 'by'}">
                           readonly="readonly"
                    </c:if>
                           pattern="\d{1,12}" required
                           value="<fmt:formatNumber value="${cur.course}" pattern="#"
                           type="number"/>">

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label" for="max_sum"><fmt:message key="currency.max_sum"/> </label>

                <div class="col-md-4">

                    <input id="max_sum" name="max_sum" type="text" class="form-control input-md"
                           pattern="\d{1,12}" required
                           value="<fmt:formatNumber value="${cur.max_sum}" pattern="#"
                           type="number"/>">

                </div>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <button type="submit" class="btn btn-success btn-md" name="currency"
                                value="Ok" onclick="return buttonClick(this)"><fmt:message key="profile.save"/>
                        </button>
                        <button type="submit" class="btn btn-default btn-md" name="main"
                                onclick="return buttonClick(this)" formnovalidate><fmt:message key="newCard.cancel"/>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>
[
