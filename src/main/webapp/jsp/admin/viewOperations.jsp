<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title></title>
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js" />"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<jsp:include page="menu.jsp"/>
<c:set var="message" value="${success}"/>
<div class="panel-body">
    <div class="row">
        <c:if test="${message!=null}">
            <div class="alert alert-info">
                <strong> <c:out value="${message}"/></strong>
            </div>
        </c:if>
    </div>
</div>
<table class="table table-bordered table-striped table-condensed">
    <c:if test="${not empty operations}">
        <thead>
        <tr>
            <th><fmt:message key="viewoper.name"/></th>
            <th><fmt:message key="viewoper.description"/></th>
            <th><fmt:message key="viewCards.status"/></th>
            <th class="cent" colspan=2><fmt:message key="viewCards.action"/></th>
        </tr>
        </thead>
    </c:if>
    <tbody>
    <c:forEach items="${operations}" var="operations">
        <tr>
            <td><c:out value="${operations.name}"/></td>
            <td><c:out value="${operations.description}"/></td>
            <td><c:out value="${operations.status}"/></td>
            <c:choose>
                <c:when test="${operations.status == 'ACTIVE'}">
                    <td><a href="<%=request.getContextPath()%>/controller?command=oper&name=update&id=<c:out
                value="${operations.id}"/>"><fmt:message key="viewoper.update"/> </a></td>
                    <td><a href="<%=request.getContextPath()%>/controller?command=oper&name=change&id=<c:out
                value="${operations.id}"/>"><fmt:message key="viewoper.archive"/> </a></td>
                </c:when>
                <c:otherwise>
                    <td></td>
                    <td></td>
                </c:otherwise>
            </c:choose>
        </tr>
    </c:forEach>
    </tbody>
</table>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>
