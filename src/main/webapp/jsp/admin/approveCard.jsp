<%@ taglib prefix="ctg" uri="customtags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title></title>
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js" />"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>

    <script type="text/javascript">
        function showDetails(button) {
            document.getElementById('clicked_button').value = button.getAttribute("data-button-type");
        }
    </script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<jsp:include page="menu.jsp"/>
<c:set var="message" value="${success}"/>
<c:set var="error" value="${errorMessage}"/>
<form action="${pageContext.request.contextPath}/controller" method="post">
    <div class="panel-body">
        <div class="row">
            <c:if test="${message!=null}">
                <div class="alert alert-info">
                    <strong> <c:out value="${message}"/></strong>
                </div>
            </c:if>
            <c:if test="${error!=null}">
                <div class="alert alert-danger">
                    <strong> <c:out value="${error}"/></strong>
                </div>
            </c:if>
        </div>
    </div>
    <input type="hidden" name="commandTag" value=<ctg:CommandTag/>>
    <input type="hidden" name="command" value="cardstatus">
    <input type="hidden" name="status" id="clicked_button" value="">
    <table align="center" style="width: auto;" class="table table-striped table-bordered table-responsive">
        <c:if test="${not empty cards}">
            <thead class="thead-inverse">
            <tr>
                <th><fmt:message key="viewCards.number"/></th>
                <th><fmt:message key="viewCards.currency"/></th>
                <th><fmt:message key="viewCards.balance"/></th>
                <th><fmt:message key="login.login"/></th>
                <th class="cent" colspan=2><fmt:message key="viewCards.action"/></th>
            </tr>
            </thead>
        </c:if>
        <c:forEach var="card" items="${cards}">
            <tbody>
            <tr style="text-align: center">
                <td>
                    <c:out value="${card.key.number}"/>
                </td>
                <td>
                    <c:out value="${card.key.currency}"/>
                </td>
                <td>
                    <fmt:formatNumber value="${card.key.balance}"
                                      type="number"/>
                </td>
                <td><c:out value="${card.value}"/></td>
                <td>
                    <button type="submit" class="btn btn-link" name="number" value="${card.key.number}"
                            onclick="showDetails(this)" data-button-type="working">
                        <fmt:message key="card.approve"/>
                    </button>
                </td>
                <td>
                    <button type="submit" class="btn btn-link" name="number" value="${card.key.number}"
                            onclick="showDetails(this)" data-button-type="disapproved">
                        <fmt:message key="card.disapprove"/>
                    </button>
                </td>
            </tr>
            </tbody>
        </c:forEach>
    </table>
</form>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>