<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js" />"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<jsp:include page="menu.jsp"/>
<c:set var="error" value="${errorMessage}"/>
<c:set var="message" value="${success}"/>
<form action="${pageContext.request.contextPath}/controller" method="post">
    <input type="hidden" name="command" value="pay">
    <input type="hidden" name="param" value="cl">
    <c:if test="${error!=null}">
        <div class="panel-body">
            <div class="row">
                <div class="alert alert-danger">
                    <strong> <c:out value="${error}"/></strong>
                </div>
            </div>
        </div>
    </c:if>
    <c:if test="${not empty cards && not empty operations}">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-4 text-center">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><img class="pull-right"
                                                         src="http://i76.imgup.net/accepted_c22e0.png">
                                <fmt:message key="payment.service"/></h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" id="payment-form">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12">
                                        <div class="form-group">
                                            <label for="cards"><fmt:message key="payment.select"/> </label>
                                            <select id="cards" name="cards" class="form-control">
                                                <c:forEach items="${cards}" var="card">
                                                    <option>${card.number}(${card.currency})</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12">
                                        <div class="form-group">
                                            <label for="operation"><fmt:message key="payment.operation"/> </label>
                                            <select id="operation" name="operation" class="form-control">
                                                <c:forEach items="${operations}" var="operation">
                                                    <option>${operation.name}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="sum"><fmt:message key="payment.sum"/> </label>
                                            <input type="text" class="form-control" id="sum" name="sum"
                                                   pattern="\d{1,12}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button class="btn btn-success btn-lg btn-block" type="submit" value="Ok"
                                                name="enter"><fmt:message key="payment.submit"/>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </c:if>
</form>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>
