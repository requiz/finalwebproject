<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js" />"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<jsp:include page="/common/defineLanguage.jsp"/>
<div class="container">
    <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><fmt:message key="registration.head"/>
                        <small><fmt:message key="registration.subtitle"/></small>
                    </h3>
                </div>
                <div class="panel-body">
                    <form role="form" action="${pageContext.request.contextPath}/controller" method="post">
                        <input type="hidden" name="command" id="clicked_button" value="">

                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="firstName" id="firstName" class="form-control input-sm"
                                           pattern="([a-zA-Zа-яА-я]{3,30})"
                                           required
                                           placeholder="<fmt:message key="registration.firstName"/>"
                                           value="${account.firstName}">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="lastName" id="lastName" class="form-control input-sm"
                                           pattern="([a-zA-Zа-яА-я]{3,30})"
                                           required
                                           placeholder="<fmt:message key="registration.lastName"/>"
                                           value="${account.lastName}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="email" name="email" id="email" class="form-control input-sm"
                                   placeholder="<fmt:message key="registration.email"/>"
                                   pattern="^(.+)@(.+)$"
                                   required
                                   onkeyup="checkCapsWarning(event)" onfocus="checkCapsWarning(event)"
                                   onblur="removeCapsWarning()"
                                   value="${account.email}">
                        </div>

                        <div class="form-group">
                            <input type="text" name="login" id="login" class="form-control input-sm"
                                   pattern="^[a-zA-Z0-9]{3,16}$"
                                   required
                                   placeholder="<fmt:message key="login.login"/>"
                                   value="${account.login}">
                        </div>

                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="password" name="password" id="password" class="form-control input-sm"
                                           placeholder="<fmt:message key="login.password"/>"
                                           pattern="^[a-z0-9]{3,16}$"
                                           required
                                           onkeyup="checkCapsWarning(event)" onfocus="checkCapsWarning(event)"
                                           onblur="removeCapsWarning()">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="password" name="repPass" id="rePassword" class="form-control input-sm"
                                           placeholder="<fmt:message key="registration.confirm"/>"
                                           pattern="^[a-z0-9]{3,16}$"
                                           required
                                           onkeyup="checkCapsWarning(event)" onfocus="checkCapsWarning(event)"
                                           onblur="removeCapsWarning()">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-8 col-sm-8 col-md-8">
                                <div class="form-group">
                                    <input type="tel" name="number" id="contactNumber" class="form-control input-sm"
                                           pattern="\d{2}[\-]\d{3}[\-]\d{4}"
                                           required
                                           placeholder="<fmt:message key="registration.contactNmber"/>"
                                           value="${account.contactNumber}">
                                </div>
                            </div>

                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <select id="sex" name="sex">
                                        <option><fmt:message key="registration.male"/></option>
                                        <option><fmt:message key="registration.female"/></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="capsIndicator">
                            <fmt:message key="login.capslock"/></div>
                        <div class="error">
                            <c:out value="${errorMessage}"/>
                        </div>
                        <input type="submit" name="REGISTRATION"
                               value="<fmt:message key="registration.submit"/>"
                               class="btn btn-info btn-block"
                               onclick="return buttonClick(this)">
                        <input type="submit" formnovalidate name="main"
                               value="<fmt:message key="registration.cancel"/>"
                               class="btn btn-danger btn btn-block"
                               onclick="return buttonClick(this)">

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>