<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js" />"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<jsp:include page="/jsp/common/defineMenu.jsp"/>
<form>
    <jsp:include page="/jsp/common/tableTransactions.jsp"/>
    <input type="hidden" name="command" value="viewtouser">
    <div class="sort">
        <select name="sort" onchange="this.form.submit()">
            <option selected disabled><fmt:message key="sort.sort"/></option>
            <option value="bydate" <c:if test="${requestScope.sort eq 'bydate'}">selected</c:if>><fmt:message
                    key="sort.date"/></option>
            <option value="bysum" <c:if test="${requestScope.sort eq 'bysum'}">selected</c:if>><fmt:message
                    key="sort.balance"/></option>
        </select>
    </div>
</form>
</body>
</html>
