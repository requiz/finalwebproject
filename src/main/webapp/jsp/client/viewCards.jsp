<%@ taglib prefix="ctg" uri="customtags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js" />"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
    <script type="text/javascript">
        function showDetails(button) {
            document.getElementById('clicked_button').value = button.getAttribute("data-button-type");
        }
    </script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<jsp:include page="/jsp/common/defineMenu.jsp"/>
<c:set var="message" value="${success}"/>
<c:set var="error" value="${errorMessage}"/>
<form action="${pageContext.request.contextPath}/controller" id="form">
    <input type="hidden" name="name" value="user">
    <input type="hidden" name="prm" value="v">
    <input type="hidden" name="commandTag" value=<ctg:CommandTag/>>
    <input type="hidden" name="command" id="clicked_button" value="">

    <div class="panel-body">
        <div class="row">
            <c:if test="${message!=null}">
                <div class="alert alert-info">
                    <strong> <c:out value="${message}"/></strong>
                </div>
            </c:if>
            <c:if test="${error!=null}">
                <div class="alert alert-danger">
                    <strong> <c:out value="${error}"/></strong>
                </div>
            </c:if>
        </div>
    </div>
    <div class="table-responsive">
        <table align="center" style="width: auto;" class="table table-bordered table-striped  table-condensed">
            <c:if test="${not empty cards}">
                <thead>
                <tr>
                    <th><fmt:message key="viewCards.number"/></th>
                    <th><fmt:message key="viewCards.balance"/></th>
                    <th><fmt:message key="viewCards.currency"/></th>
                    <th><fmt:message key="viewCards.dateopen"/></th>
                    <th class="cent"><fmt:message key="viewCards.status"/></th>
                    <th class="cent" colspan=2><fmt:message key="viewCards.action"/></th>
                </tr>
                </thead>
            </c:if>
            <tbody>
            <c:forEach items="${cards}" var="card" varStatus="loop">
                <tr style="text-align: center">
                    <td><c:out value="${card.number}"/></td>
                    <td class="success"><fmt:formatNumber value="${card.balance}"
                                                          type="number"/></td>
                    <td><c:out value="${card.currency}"/></td>
                    <td><fmt:formatDate pattern="dd.MM.yy"
                                        value="${card.date}"/></td>
                    <td class="info"><c:out value="${card.status}"/></td>
                    <c:set var="contains" value="false"/>
                    <c:forEach var="item" items="${notchanged}">
                        <c:if test="${item eq card.status}">
                            <c:set var="contains" value="true"/>
                        </c:if>
                    </c:forEach>
                    <c:choose>
                        <c:when test="${contains == false}">
                            <td class="danger t4">
                                <button type="submit" formmethod="post"
                                        class="btn btn-link"
                                        value="${card.number}"
                                        name="number"
                                        onclick="showDetails(this)" data-button-type="cardstatus">
                                    <fmt:message key="viewCards.changeStatus"/>
                                </button>
                            </td>
                        </c:when>
                        <c:otherwise>
                            <td class="danger"></td>
                        </c:otherwise>
                    </c:choose>
                    <td class="danger t4">
                        <button type="submit" id="drop" name="number" class="btn btn-link" value="${card.number}"
                                onclick="showDetails(this)" data-button-type="drop" formmethod="post">
                            <fmt:message key="viewCards.drop"/>
                        </button>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</form>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>
