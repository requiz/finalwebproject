<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<c:url value="/resources/styles/bootstrap.min.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/jquery.min.js" />"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand"
               href="${pageContext.request.contextPath}/controller?command=viewcards"><fmt:message key="home"/> </a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="${pageContext.request.contextPath}/controller?command=addcard"> <fmt:message
                    key="clientmenu.addcard"/> </a></li>
            <li><a href="${pageContext.request.contextPath}/controller?command=viewtouser"><fmt:message
                    key="clientmenu.alltransactions"/></a>
            </li>
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><fmt:message
                    key="clientmenu.transfer"/> <span
                    class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="${pageContext.request.contextPath}/controller?command=pay"><fmt:message
                            key="clientmenu.pay"/></a></li>
                    <li><a href="${pageContext.request.contextPath}/controller?command=transfer"><fmt:message
                            key="clientmenu.transfermoney"/> </a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><fmt:message
                    key="clientmenu.changedetailes"/> <span
                    class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="${pageContext.request.contextPath}/controller?command=change&name=profile"><fmt:message
                            key="clientmenu.changeinformation"/></a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/controller?command=change&name=password"><fmt:message
                                key="clientmenu.changepassword"/></a>
                    </li>
                </ul>
            </li>
            <li><a href="${pageContext.request.contextPath}/controller?command=logout"><fmt:message
                    key="clientmenu.logout"/></a></li>
        </ul>
    </div>
</nav>
<jsp:include page="/common/defineLanguage.jsp"/>
</body>
</html>
