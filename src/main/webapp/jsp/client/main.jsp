<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js" />"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
</head>
<body onload="document.getElementById('main-page').submit()">
<form id="main-page" action="${pageContext.request.contextPath}/controller">
    <input type="hidden" name="command" value="viewcards">
</form>
</body>
</html>
