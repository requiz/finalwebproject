<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js" />"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<jsp:include page="menu.jsp"/>
<c:set var="message" value="${errorMessage}"/>
<form action="${pageContext.request.contextPath}/controller" method="post">
    <input type="hidden" name="command" id="clicked_button" value="">

    <div class="container bootstrap snippet">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <fmt:message key="newCard.add"/>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <c:if test="${message!=null}">
                                <div class="alert alert-danger">
                                    <strong> <c:out value="${message}"/></strong>
                                </div>
                            </c:if>
                            <div class="col-xs-6 col-sm-6 col-md-6 login-box">
                                <div class="form-group">
                                    <label for="type"><fmt:message key="newCard.type"/> </label>
                                    <select id="type" class="form-control" name="type">
                                        <c:forEach items="${types}" var="type">
                                            <option>${type}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 login-box">
                                <div class="form-group">
                                    <label for="currency"><fmt:message key="newCard.currency"/> </label>
                                    <select id="currency" class="form-control" name="currency">
                                        <c:forEach items="${currency}" var="cur">
                                            <option data-placeholder="${cur.max_sum}">${cur.name}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 login-box">

                                <div class="form-group">
                                    <label for="balance"><fmt:message key="newCard.balance"/> </label>
                                    <input type="text" id="balance" class="form-control" name="balance"
                                           pattern="\d{1,12}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <button type="submit" class="btn btn-success btn-md" name="addcard"
                                    onclick="return buttonClick(this)">
                                <fmt:message key="newCard.submit"/>
                            </button>
                            <button type="submit" class="btn btn-default btn-md" name="main" formnovalidate
                                    onclick="return buttonClick(this)"><fmt:message key="newCard.cancel"/>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>
