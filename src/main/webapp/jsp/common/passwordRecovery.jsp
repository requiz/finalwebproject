<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js" />"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<jsp:include page="/common/defineLanguage.jsp"/>
<c:set var="error" value="${errorMessage}"/>
<c:if test="${error!=null}">
    <div class="panel-body">
        <div class="row">
            <div class="alert alert-danger">
                <strong> <c:out value="${error}"/></strong>
            </div>
        </div>
    </div>
</c:if>
<div class="container">
    <div class="row">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center">
                            <h3><i class="fa fa-lock fa-4x"></i></h3>

                            <h2 class="text-center"><fmt:message key="login.recovery"/></h2>

                            <p><fmt:message key="login.pagerecovery"/></p>

                            <div class="panel-body">

                                <form class="form" action="${pageContext.request.contextPath}/controller" method="post">
                                    <input type="hidden" name="command" value="recovery">
                                    <fieldset>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i
                                                        class="glyphicon glyphicon-chevron-right color-blue"></i></span>

                                                <input type="text" id="login" name="login"
                                                       class="form-control input-sm"
                                                       placeholder="<fmt:message key="login.login"/>"
                                                       pattern="^[a-zA-Z0-9]{3,16}$"
                                                       required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i
                                                        class="glyphicon glyphicon-envelope color-blue"></i></span>

                                                <input type="email" name="email" id="email"
                                                       class="form-control input-sm"
                                                       placeholder="<fmt:message key="registration.email"/>"
                                                       pattern="^(.+)@(.+)$"
                                                       required
                                                       onkeyup="checkCapsWarning(event)"
                                                       onfocus="checkCapsWarning(event)"
                                                       onblur="removeCapsWarning()">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input class="btn btn-lg btn-primary btn-block" name="button"
                                                   value="<fmt:message key="lable.buttonrecovery"/> "
                                                   type="submit">
                                        </div>
                                    </fieldset>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>
