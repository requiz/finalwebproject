<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title></title>
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js" />"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<jsp:include page="/jsp/common/defineMenu.jsp"/>
<c:set var="message" value="${errorMessage}"/>
<form action="${pageContext.request.contextPath}/controller" method="post">
    <input type="hidden" name="command" id="clicked_button" value="">
    <input type="hidden" name="name" value="password">

    <div class="container bootstrap snippet">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <fmt:message key="profile.password"/>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <c:if test="${message!=null}">
                                <div class="alert alert-danger">
                                    <strong> <c:out value="${message}"/></strong>
                                </div>
                            </c:if>
                            <div class="col-xs-6 col-sm-6 col-md-6 separator social-login-box"><br>
                                <img alt="" class="img-thumbnail"
                                     src="http://bootdey.com/img/Content/avatar/avatar1.png">
                            </div>
                            <div style="margin-top:80px;" class="col-xs-6 col-sm-6 col-md-6 login-box">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control" type="password"
                                               placeholder="<fmt:message key="password.current"/> "
                                               name="password"
                                               pattern="^[a-z0-9]{3,16}$"
                                               required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">

                                        <input class="form-control" type="password"
                                               placeholder="<fmt:message key="password.new"/> "
                                               name="newPassword"
                                               pattern="^[a-z0-9]{3,16}$"
                                               required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">

                                        <input class="form-control" type="password"
                                               placeholder="<fmt:message key="password.confirm"/> "
                                               name="confirmPassword"
                                               pattern="^[a-z0-9]{3,16}$"
                                               required>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <button type="submit" class="btn btn-success btn-md" name="change" value="ok"
                                        onclick="return buttonClick(this)"><fmt:message key="profile.save"/>
                                </button>
                                <button type="submit" class="btn btn-default btn-md" name="main" formnovalidate
                                        onclick="return buttonClick(this)"><fmt:message key="newCard.cancel"/>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
</form>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>