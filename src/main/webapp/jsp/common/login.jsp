<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title></title>
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/jquery.min.js" />"></script>
    <script src="<c:url value="/resources/javascript/capslock.js"/>"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<jsp:include page="/common/defineLanguage.jsp"/>
<c:if test="${success!=null}">
    <div class="panel-body">
        <div class="row">
            <div class="alert alert-info">
                <strong> <c:out value="${success}"/></strong>
            </div>
        </div>
    </div>
</c:if>
<form action="${pageContext.request.contextPath}/controller" method="post" class="form-1">
    <input type="hidden" name="command" id="clicked_button" value="">

    <p class="field">
        <label for="userID"><fmt:message key="login.login"/> </label>
        <input class="input" type="text" id="userID" placeholder="<fmt:message key="login.login"/>"
               name="login" value="nikolay"
               pattern="^[a-zA-Z0-9]{3,16}$"
               title="<fmt:message key="login.message"/>"
               required=""/>
    </p>

    <p class="field">
        <label for="password"><fmt:message key="login.password"/> </label>
        <input class="input" type="password" id="password"
               placeholder="<fmt:message key="login.password"/>"
               name="password" value="1111"
               onkeyup="checkCapsWarning(event)" onfocus="checkCapsWarning(event)" onblur="removeCapsWarning()"
               pattern="^[a-z0-9]{3,16}$"
               title="<fmt:message key="password.message"/>"
               required
                />
    </p>
    <br>

    <div class="error">
        <c:out value="${errorMessage}"/>
    </div>
    <p>

    <div id="capsIndicator">
        <fmt:message key="login.capslock"/></div>
    <input type="submit" class="submit" value="<fmt:message key="login.submit"/>"
           name="login" onclick="return buttonClick(this)"/>
    <input type="submit" class="btn-success" formnovalidate
           value="<fmt:message key="login.registration"/>" name="registration"
           onclick="return buttonClick(this)">
    <input type="reset" class="btn-default" value="<fmt:message key="login.clear"/>">

</form>
<div class="etc-login-form">
    <p><fmt:message key="login.recovery"/> <a
            href="${pageContext.request.contextPath}/controller?command=recovery"><fmt:message
            key="login.recoverymsg"/> </a></p>
</div>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>