<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<c:set var="status" value="${account.status}"/>
<c:if test="${status=='USER'}">
    <jsp:include page="/jsp/client/menu.jsp"/>
</c:if>
<c:if test="${status=='ADMIN'}">
    <jsp:include page="/jsp/admin/menu.jsp"/>
</c:if>
</body>
</html>
