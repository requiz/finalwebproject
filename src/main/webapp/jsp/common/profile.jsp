<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js" />"></script>
    <script src="<c:url value="/resources/javascript/bootstrap.min.js" />"></script>
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<c:set var="account" value="${sessionScope.account}"/>
<jsp:include page="/jsp/common/defineMenu.jsp"/>
<c:set var="message" value="${errorMessage}"/>
<c:set var="suc" value="${success}"/>
<form method="POST" id="mydiv" action="${pageContext.request.contextPath}/controller">
    <input type="hidden" name="command" id="clicked_button" value="">
    <input type="hidden" name="password" value="${profile.password}">
    <input type="hidden" name="name" value="profile">
    <c:set var="upd" value="${update}"/>
    <div class="container bootstrap snippet">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <fmt:message key="profile.header"/>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <c:if test="${message!=null}">
                                <div class="alert alert-danger">
                                    <strong> <c:out value="${message}"/></strong>
                                </div>
                            </c:if>
                            <c:if test="${suc!=null}">
                                <div class="alert alert-danger">
                                    <strong> <c:out value="${suc}"/></strong>
                                </div>
                            </c:if>
                            <div class="col-xs-6 col-sm-6 col-md-6 separator social-login-box"><br>
                                <img alt="" class="img-thumbnail"
                                     src="http://bootdey.com/img/Content/avatar/avatar1.png">
                            </div>
                            <div style="margin-top:30px;" class="col-xs-6 col-sm-6 col-md-6 login-box">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label for="login"><fmt:message key="login.login"/> </label>
                                        <input type="text" class="form-control" name="login" id="login"
                                               pattern="^[a-zA-Z0-9]{3,16}$"
                                               required
                                               value="<c:out value="${account.login}" />"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <label for="firstName"><fmt:message key="registration.firstName"/>
                                        </label>
                                        <input type="text" name="firstName" class="form-control" id="firstName"
                                               pattern="([a-zA-Zа-яА-я]{3,30})"
                                               required
                                               value="<c:out value="${account.firstName}" />"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <label for="lastName"><fmt:message key="registration.lastName"/>
                                        </label>
                                        <input type="text" name="lastName" class="form-control" id="lastName"
                                               pattern="([a-zA-Zа-яА-я]{3,30})"
                                               required
                                               value="<c:out value="${account.lastName}" />"/>

                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 login-box">

                                <div class="form-group">
                                    <div class="input-group">
                                        <label for="email"><fmt:message key="registration.email"/> </label>
                                        <input type="email" class="form-control" name="email" id="email"
                                               pattern="^(.+)@(.+)$"
                                               required
                                               value="<c:out value="${account.email}" />"/>

                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 login-box">
                                <div class="form-group">
                                    <div class="input-group">
                                        <label for="contactNumber"><fmt:message
                                                key="registration.contactNmber"/> </label>
                                        <input type="tel" name="number" class="form-control" id="contactNumber"
                                               pattern="\d{2}[\-]\d{3}[\-]\d{4}"
                                               required
                                               value="<c:out value="${account.contactNumber}" />"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <button type="submit" class="btn btn-success btn-md" name="change" value="ok"
                                    onclick="return buttonClick(this)"><fmt:message key="profile.save"/>
                            </button>
                            <button type="submit" class="btn btn-default btn-md" name="main" formnovalidate
                                    onclick="return buttonClick(this)"><fmt:message key="newCard.cancel"/>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>
