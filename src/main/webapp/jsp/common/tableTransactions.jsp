<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.language}" scope="session"/>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
</head>
<body>
<c:set var="account" value="${sessionScope.account}"/>
<c:set var="message" value="${errorMessage}"/>
<c:set var="suc" value="${success}"/>
<c:if test="${message!=null || suc!= null}">
    <div class="panel-body">
        <div class="row">
            <c:if test="${message != null}">
                <div class="alert alert-danger">
                    <strong> <c:out value="${message}"/></strong>
                </div>
            </c:if>
            <c:if test="${suc != null}">
                <div class="alert alert-info">
                    <strong> <c:out value="${suc}"/></strong>
                </div>
            </c:if>
        </div>
    </div>
</c:if>
<table class="table table-bordered  table-striped  table-condensed">
    <c:if test="${not empty transactions}">
        <thead>
        <tr>
            <th><fmt:message key="transaction.oper"/></th>
            <th><fmt:message key="transaction.ammount"/></th>
            <th><fmt:message key="transaction.number"/></th>
            <th><fmt:message key="transaction.date"/></th>
            <c:if test="${not empty position}">
                <th><fmt:message key="transaction.cancel"/></th>
            </c:if>
            <c:if test="${account.status == 'ADMIN'}">
                <th><fmt:message key="viewCards.status"/></th>
            </c:if>
        </tr>
        </thead>
    </c:if>
    <tbody>
    <c:forEach items="${transactions}" var="transaction">
        <c:set var="key" value="${transaction.key}"/>
        <c:set var="value" value="${transaction.value}"/>
        <tr>
            <td><c:out value="${value.name}"/>
                <c:if test="${archived[value.id]!=null}">
                    <em> (<fmt:message key="operation.inarchive"/>)</em>
                </c:if>
            </td>
            <td><fmt:formatNumber value="${key.ammount}"
                                  type="number"/></td>
            <td><c:out value="${key.number}"/></td>
            <td><fmt:formatDate type="both"
                                dateStyle="short" timeStyle="short"
                                value="${key.datetime}"/></td>
            <c:if test="${account.status == 'ADMIN'}">
                <td><c:out value="${key.status}"/></td>
            </c:if>
            <c:if test="${position[key]!= null}">
                <td>
                    <a href="${pageContext.request.contextPath}/controller?command=cancel&id=${key.id}"><fmt:message
                            key="transaction.cancelSelected"/> </a>
                </td>
            </c:if>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
