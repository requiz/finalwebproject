<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:setBundle basename="properties.lable"/>
<html>
<head>
    <title></title>
    <link href="<c:url value="/resources/styles/login.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js"/>"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
</head>
<body>
<jsp:include page="/common/header.jsp"/>
<div class="error-page-wrap">
    <article class="error-page gradient">
        <hgroup>
            <h1>404</h1>

            <h2>oops! page not found</h2>
        </hgroup>
        <c:set var="status" value="${account.status}"/>
        <c:choose>
            <c:when test="${status=='USER'}">
                <a href="<c:url value="/jsp/client/main.jsp"/>" title="Back to site" class="error-back">back</a>
            </c:when>
            <c:when test="${status=='ADMIN'}">
                <a href="<c:url value="/jsp/admin/main.jsp"/>" title="Back to site" class="error-back">back</a>
            </c:when>
            <c:otherwise>
                <a href="<c:url value="/jsp/common/login.jsp"/>" title="Back to site" class="error-back">back</a>
            </c:otherwise>
        </c:choose>
    </article>
</div>
<jsp:include page="/common/footer.jsp"/>
</body>
</html>
