<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
    <link href="<c:url value="/resources/styles/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/styles/menu.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/javascript/capslock.js" />"></script>
    <script src="<c:url value="/resources/javascript/jquery.min.js" />"></script>
</head>
<body onload="block()">
<c:set var="lang" value="${pageContext.response.locale}"/>
<form action="${pageContext.request.contextPath}/controller" method="post">
    <input type="hidden" name="command" value="changelanguage">
    <input type="hidden" name="commandTag" value=<ctg:PageTag/>>
    <input type="hidden" name="lastcommand" id="param" value="${changable}"/>

    <div class="language">
        <select name="language" id="change" onchange="this.form.submit()">
            <option value="ru_RU" <c:if test="${sessionScope.language eq 'ru_RU'}">selected</c:if>>Русский</option>
            <option value="en_US" <c:if test="${sessionScope.language eq 'en_US'}">selected</c:if>>English</option>
        </select>
    </div>
</form>
</body>
</html>
