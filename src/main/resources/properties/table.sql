CREATE TABLE `account` (
   `acc_id` int(11) NOT NULL AUTO_INCREMENT,
   `login` varchar(45) NOT NULL,
   `password` varchar(45) NOT NULL,
   `first_name` varchar(45) NOT NULL,
   `last_name` varchar(45) NOT NULL,
   `sex` varchar(15) DEFAULT NULL,
   `email` varchar(200) NOT NULL,
   `contact_number` varchar(45) DEFAULT NULL,
   `role` varchar(20) DEFAULT NULL,
   PRIMARY KEY (`acc_id`),
   UNIQUE KEY `login_UNIQUE` (`login`),
   UNIQUE KEY `email_UNIQUE` (`email`)
 ) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8

CREATE TABLE `currency` (
   `name` varchar(45) NOT NULL,
   `course` decimal(19,2) DEFAULT NULL,
   `max_sum` decimal(19,2) DEFAULT NULL,
   PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `operation` (
   `oper_id` int(11) NOT NULL AUTO_INCREMENT,
   `name` varchar(45) DEFAULT NULL,
   `description` varchar(300) DEFAULT NULL,
   `status` varchar(45) DEFAULT 'ACTIVE',
   PRIMARY KEY (`oper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4433 DEFAULT CHARSET=utf8

CREATE TABLE `type` (
   `card_type` varchar(45) NOT NULL,
   `description` varchar(200) DEFAULT NULL,
   `rate` int(11) DEFAULT NULL,
   PRIMARY KEY (`card_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


CREATE TABLE `card` (
   `number` int(11) NOT NULL AUTO_INCREMENT,
   `balance` decimal(10,4) DEFAULT NULL,
   `currency` varchar(15) DEFAULT NULL,
   `date_open` date NOT NULL,
   `type_card_type` varchar(45) NOT NULL,
   `account_acc_id` int(11) NOT NULL,
   `status` varchar(20) NOT NULL DEFAULT '{WORKING}',
   PRIMARY KEY (`number`),
   KEY `fk_card_type1_idx` (`type_card_type`),
   KEY `fk_card_account1_idx` (`account_acc_id`),
   CONSTRAINT `fk_card_account1` FOREIGN KEY (`account_acc_id`) REFERENCES `account` (`acc_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
   CONSTRAINT `fk_card_type1` FOREIGN KEY (`type_card_type`) REFERENCES `type` (`card_type`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=88392955 DEFAULT CHARSET=utf8

CREATE TABLE `transaction` (
   `tr_id` int(11) NOT NULL AUTO_INCREMENT,
   `ammount` decimal(10,2) DEFAULT NULL,
   `datetime` datetime DEFAULT NULL,
   `card_number` int(11) DEFAULT NULL,
   `card_number1` int(11) DEFAULT NULL,
   `operation_oper_id` int(11) NOT NULL DEFAULT '3',
   `status` varchar(20) DEFAULT 'SAVE',
   PRIMARY KEY (`tr_id`),
   KEY `fk_transaction_card1_idx` (`card_number`),
   KEY `fk_transaction_card2_idx` (`card_number1`),
   KEY `fk_transaction_operation1_idx` (`operation_oper_id`),
   CONSTRAINT `fk_transaction_card1` FOREIGN KEY (`card_number`) REFERENCES `card` (`number`) ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT `fk_transaction_card2` FOREIGN KEY (`card_number1`) REFERENCES `card` (`number`) ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT `fk_transaction_operation1` FOREIGN KEY (`operation_oper_id`) REFERENCES `operation` (`oper_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8
